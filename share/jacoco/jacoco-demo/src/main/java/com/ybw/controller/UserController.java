package com.ybw.controller;

import com.ybw.service.UserService;
import com.ybw.vo.UserVO;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户
 *
 * @author ybw
 * @version V1.0
 * @className TestController
 * @date 2023/8/13
 **/
@Slf4j
@RequestMapping("/user")
@RestController
public class UserController {

    @Resource
    private UserService userService;

    /**
     * 获取用户
     *
     * @param id
     * @methodName: getUser
     * @return: com.ybw.vo.UserVO
     * @author: ybw
     * @date: 2023/8/13
     **/
    @GetMapping("/getUser")
    public UserVO getUser(Long id) {
        return userService.getUser(id);
    }
}
