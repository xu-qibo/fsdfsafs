package com.ybw.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author ybw
 * @version V1.0
 * @className UserVO
 * @date 2023/8/13
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserVO {
    private Long id;
    private String name;
}
