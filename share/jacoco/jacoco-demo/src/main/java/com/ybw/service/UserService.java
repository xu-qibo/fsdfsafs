package com.ybw.service;

import com.ybw.vo.UserVO;

/**
 * @author ybw
 * @version V1.0
 * @className UserService
 * @date 2023/8/13
 **/
public interface UserService {
    /**
     * @param id
     * @methodName: getUser
     * @return: com.ybw.vo.UserVO
     * @author: ybw
     * @date: 2023/8/13
     **/
    UserVO getUser(Long id);
}
