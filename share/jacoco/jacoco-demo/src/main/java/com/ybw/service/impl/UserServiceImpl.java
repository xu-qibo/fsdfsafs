package com.ybw.service.impl;

import com.ybw.service.UserService;
import com.ybw.vo.UserVO;
import org.springframework.stereotype.Service;

/**
 * @author ybw
 * @version V1.0
 * @className UserServiceImpl
 * @date 2023/8/13
 **/
@Service
public class UserServiceImpl implements UserService {

    @Override
    public UserVO getUser(Long id) {
        if (id.equals(1L)) {
            return new UserVO(1L, "张三1");
        }
        if (id.equals(2L)) {
            return new UserVO(2L, "张三2");
        }
        if (id.equals(3L)) {
            return new UserVO(3L, "张三3");
        }
        return new UserVO(10L, "张三10");
    }
}
