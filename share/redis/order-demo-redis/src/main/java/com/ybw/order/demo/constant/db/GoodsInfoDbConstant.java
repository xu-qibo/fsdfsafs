package com.ybw.order.demo.constant.db;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @program: order-demo-redis
 * @description:
 * @author: geoffrey
 * @create: 2022-09-01 22:24
 */
public interface GoodsInfoDbConstant {

    /**
     * 上下架
     *
     * @author ybw
     * @version V1.0
     * @className GoodsInfoDbConstant
     * @date 2022/9/2
     **/
    @Getter
    @AllArgsConstructor
    enum AllowSale {
        PUT_SHELF(1, "上架"),
        OFF_SHELF(2, "下架");
        private Integer code;
        private String msg;

        /**
         * 根据code获取msg
         *
         * @param code
         * @return java.lang.String
         * @throws
         * @methodName: getMsgByCode
         * @author ybwei
         * @date 2022/2/16 11:29
         */
        public static String getMsgByCode(Integer code) {
            for (AllowSale allowSale : values()) {
                if (allowSale.getCode().equals(code)) {
                    return allowSale.getMsg();
                }
            }
            return null;
        }
    }

    /**
     * 状态 1：发售中；2：等待发售；3：已售罄；4：发售结束；
     *
     * @author ybwei
     * @version V1.0
     * @className GoodsInfoDBConstant
     * @date 2022/9/1
     **/
    @Getter
    @AllArgsConstructor
    enum Status {
        ON_SALE(1, "发售中"),
        WAIT_SALE(2, "等待发售"),
        END_SALE(3, "已售罄"),
        SOLD_OUT(4, "发售结束");

        private Integer code;
        private String desc;

        /**
         * 根据code获取msg
         *
         * @param code
         * @return java.lang.String
         * @throws
         * @methodName: getMsgByCode
         * @author ybwei
         * @date 2022/2/16 11:29
         */
        public static String getMsgByCode(Integer code) {
            for (GoodsInfoDbConstant.Status status : values()) {
                if (status.getCode().equals(code)) {
                    return status.getDesc();
                }
            }
            return null;
        }
    }
}
