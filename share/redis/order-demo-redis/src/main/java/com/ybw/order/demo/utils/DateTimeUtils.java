package com.ybw.order.demo.utils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * 时间工具类
 *
 * @author ybwei
 * @date 2022/1/25 17:17
 **/
public class DateTimeUtils {

    public final static DateTimeFormatter yyyyMMdd = DateTimeFormatter.ofPattern("yyyyMMdd");

    /**
     * 获取时间戳（秒）
     *
     * @return java.lang.Long
     * @throws
     * @methodName: getTimeStampSecond
     * @author ybwei
     * @date 2022/1/25 17:18
     */
    public static Long getTimeStampSecond() {
        return System.currentTimeMillis() / 1000;
    }

    /**
     * 获取昨日日期
     *
     * @methodName: getYesterdayDate
     * @return: java.lang.String
     * @author: ybw
     * @date: 2022/4/7
     **/
    public static String getYesterdayDate() {
        LocalDate yesterday = LocalDate.now().minusDays(1);
        return yyyyMMdd.format(yesterday);
    }

    /**
     * 格式化
     *
     * @param localDate
     * @param dateTimeFormatter
     * @methodName: format
     * @return: java.lang.String
     * @author: ybw
     * @date: 2022/4/11
     **/
    public static String format(LocalDate localDate, DateTimeFormatter dateTimeFormatter) {
        return dateTimeFormatter.format(localDate);
    }
}
