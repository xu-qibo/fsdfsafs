package com.ybw.order.demo.utils;

import org.springframework.cglib.beans.BeanMap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Map转对象 对象转Map 工具类
 *
 * @author ybwei
 * @date 2022/3/10 15:27
 **/
public class MapUtils {


    /**765
     * 将对象装换为map
     *
     * @param bean
     * @return java.util.Map<java.lang.String, java.lang.Object>
     * @throws
     * @methodName: beanToMap
     * @author ybwei
     * @date 2022/3/14 18:03
     */
    public static <T> Map<Object, Object> beanToMap(T bean) {
        Map<Object, Object> map = new HashMap<>();
        if (bean != null) {
            BeanMap beanMap = BeanMap.create(bean);
            for (Object key : beanMap.keySet()) {
                map.put(key + "", beanMap.get(key));
            }
        }
        return map;
    }

    /**
     * 将map装换为javabean对象
     *
     * @param map
     * @param bean
     * @return T
     * @throws
     * @methodName: mapToBean
     * @author ybwei
     * @date 2022/3/14 18:04
     */
    public static <T> T mapToBean(Map<Object, Object> map, T bean) {
        BeanMap beanMap = BeanMap.create(bean);
        beanMap.putAll(map);
        return bean;
    }


    /**
     * @methodName: mapToBean
     * @param map
     * @param clazz
     * @return: T
     * @author: geoffrey
     * @date: 2022/3/15
     **/
    public static <T> T mapToBean(Map<Object, Object> map,Class<T> clazz) {
        try {
            T bean = clazz.newInstance();
            BeanMap beanMap = BeanMap.create(bean);
            beanMap.putAll(map);
            return bean;
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    /**
     * 将List<T>转换为List<Map<String, Object>>
     *
     * @param objList
     * @return java.util.List<java.util.Map < java.lang.String, java.lang.Object>>
     * @throws
     * @methodName: objectsToMaps
     * @author ybwei
     * @date 2022/3/14 18:04
     */
    public static <T> List<Map<Object, Object>> objectsToMaps(List<T> objList) {
        List<Map<Object, Object>> list = new ArrayList<>();
        if (objList != null && objList.size() > 0) {
            Map<Object, Object> map = null;
            T bean = null;
            for (int i = 0, size = objList.size(); i < size; i++) {
                bean = objList.get(i);
                map = beanToMap(bean);
                list.add(map);
            }
        }
        return list;
    }

    /**
     * 将List<Map<String,Object>>转换为List<T>
     *
     * @param maps
     * @param clazz
     * @return java.util.List<T>
     * @throws
     * @methodName: mapsToObjects
     * @author ybwei
     * @date 2022/3/14 18:05
     */
    public static <T> List<T> mapsToObjects(List<Map<Object, Object>> maps, Class<T> clazz) throws InstantiationException, IllegalAccessException {
        List<T> list = new ArrayList<>();
        if (maps != null && maps.size() > 0) {
            Map<Object, Object> map = null;
            T bean = null;
            for (int i = 0, size = maps.size(); i < size; i++) {
                map = maps.get(i);
                bean = clazz.newInstance();
                mapToBean(map, bean);
                list.add(bean);
            }
        }
        return list;
    }
}
