package com.ybw.order.demo.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ybw.order.demo.entity.GoodsInfo;
import com.ybw.order.demo.service.GoodsInfoService;
import com.ybw.order.demo.vo.base.req.PageBaseReqVO;
import com.ybw.order.demo.vo.base.res.ApiResult;
import com.ybw.order.demo.vo.base.res.ApiResultGenerator;
import com.ybw.order.demo.vo.base.res.PageBaseResVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * <p>
 * 商品 前端控制器
 * </p>
 *
 * @author ybwei
 * @since 2022-09-01
 */
@Slf4j
@RestController
@RequestMapping("/goods")
public class GoodsInfoController {

    @Resource
    private GoodsInfoService goodsInfoService;

    /**
     * 商品列表-C端
     *
     * @param pageBaseReqVO
     * @methodName: list
     * @return: com.ybw.order.demo.vo.base.res.ApiResult
     * @author: ybw
     * @date: 2022/9/1
     **/
    @PostMapping("/list")
    public ApiResult list(@Valid @RequestBody PageBaseReqVO pageBaseReqVO) throws Exception {
        PageBaseResVO pageBaseResVO = goodsInfoService.list(pageBaseReqVO);
        return ApiResultGenerator.successResult(pageBaseResVO);
    }

    /**
     * 商品-详情
     *
     * @param goodsInfoId
     * @methodName: detail
     * @return: com.ybw.order.demo.vo.base.res.ApiResult
     * @author: ybwei
     * @date: 2022/9/1
     **/
    @PostMapping("/detail")
    public ApiResult detail(@RequestParam("goodsInfoId") Long goodsInfoId) throws Exception {
        GoodsInfo goodsInfo = goodsInfoService.detail(goodsInfoId);
        return ApiResultGenerator.successResult(goodsInfo);
    }
}

