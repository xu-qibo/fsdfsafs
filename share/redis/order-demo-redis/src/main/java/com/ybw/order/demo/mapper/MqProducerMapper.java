package com.ybw.order.demo.mapper;

import com.ybw.order.demo.entity.MqProducer;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ybwei
 * @since 2022-09-04
 */
public interface MqProducerMapper extends BaseMapper<MqProducer> {

}
