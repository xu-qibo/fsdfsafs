package com.ybw.order.demo.constant;

/**
 * redis key前缀
 *
 * @program: order-demo-redis
 * @description:
 * @author: geoffrey
 * @create: 2022-09-01 22:05
 */
public interface RedisPreConstant {

    /**
     * 今天售卖的商品
     *
     * @author: ybwei
     * @date: 2022/9/1
     **/
    String TODAY_SELL_GOODS_INFO_LIST = "TODAY:SELL:GOODS:INFO:LIST:";

    /**
     * 商品
     *
     * @author: ybwei
     * @date: 2022/9/1
     **/
    String GOODS_INFO = "GOODS:INFO:";
}
