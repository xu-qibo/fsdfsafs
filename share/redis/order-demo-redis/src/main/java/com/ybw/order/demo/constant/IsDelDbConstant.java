package com.ybw.order.demo.constant;

/**
 * @author ybwei
 * @Description 是否有效
 * @date 2021/7/8 13:55
 **/
public interface IsDelDbConstant {

    /**
     * @Description 有效
     * @author: ybwei
     * @Date: 2021/7/8 13:58
     */
    int Y = 1;
    /**
     * @Description 无效
     * @author: ybwei
     * @Date: 2021/7/8 13:58
     */
    int N = 0;
}
