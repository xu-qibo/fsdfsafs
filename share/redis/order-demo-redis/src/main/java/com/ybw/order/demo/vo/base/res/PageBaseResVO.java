package com.ybw.order.demo.vo.base.res;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 分页返回参数基类
 *
 * @author ybwei
 * @date 2022/2/16 15:42
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PageBaseResVO<T> {

    /**
     * 总数量
     *
     * @author: ybwei
     * @date: 2022/2/16 15:42
     */
    private Long total;
    /**
     * 内容
     *
     * @author: ybwei
     * @date: 2022/2/16 15:43
     */
    private List<T> dataList;
}
