package com.ybw.order.demo.service;

import com.ybw.order.demo.entity.OrderReconciliationMistake;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 订单-微信对账-差错表 服务类
 * </p>
 *
 * @author ybwei
 * @since 2022-09-05
 */
public interface OrderReconciliationMistakeService extends IService<OrderReconciliationMistake> {

}
