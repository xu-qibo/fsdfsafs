package com.ybw.order.demo.service;

import com.ybw.order.demo.dto.HandleStockStatusDTO;
import com.ybw.order.demo.entity.GoodsInfo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ybw.order.demo.vo.base.req.PageBaseReqVO;
import com.ybw.order.demo.vo.base.res.PageBaseResVO;

/**
 * <p>
 * 商品 服务类
 * </p>
 *
 * @author ybwei
 * @since 2022-02-20
 */
public interface GoodsInfoService extends IService<GoodsInfo> {

    /**
     * 今天售卖的商品-一1天一次
     *
     * @className GoodsInfoService
     * @author ybwei
     * @date 2022/9/1
     * @version V1.0
     **/
    void todaySellGoodsInfoListTask();

    /**
     * @param pageBaseReqVO
     * @methodName: list
     * @return: com.ybw.order.demo.vo.base.res.PageBaseResVO
     * @author: ybwei
     * @date: 2022/9/1
     **/
    PageBaseResVO list(PageBaseReqVO pageBaseReqVO);

    /**
     * 商品-详情
     *
     * @param goodsInfoId
     * @methodName: detail
     * @return: com.ybw.order.demo.entity.GoodsInfo
     * @author: ybwei
     * @date: 2022/9/1
     **/
    GoodsInfo detail(Long goodsInfoId);

    /**
     * 获取商品信息
     *
     * @param goodsInfoId
     * @methodName: getGoodsInfo
     * @return: com.ybw.order.demo.entity.GoodsInfo
     * @author: ybwei
     * @date: 2022/9/1
     **/
    GoodsInfo getGoodsInfo(Long goodsInfoId);

    /**
     * redis库存处理-加或减
     *
     * @param goodsId
     * @param handle
     * @methodName: handleStockStatusInRedis
     * @return: void
     * @author: ybwei
     * @date: 2022/9/1
     **/
    void handleStockStatusInRedis(Long goodsId, HandleStockStatusDTO.Handle handle);

    /**
     * 订单-加或减库存(mysql)
     *
     * @param handleStockStatusDTO
     * @methodName: handleStockStatusInMysql
     * @return: void
     * @author: ybwei
     * @date: 2022/9/1
     **/
    void handleStockStatusInMysql(HandleStockStatusDTO handleStockStatusDTO);
}
