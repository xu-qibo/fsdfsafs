package com.ybw.order.demo.service.impl;

import com.ybw.order.demo.entity.OrderReconciliationMistake;
import com.ybw.order.demo.mapper.OrderReconciliationMistakeMapper;
import com.ybw.order.demo.service.OrderReconciliationMistakeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单-微信对账-差错表 服务实现类
 * </p>
 *
 * @author ybwei
 * @since 2022-09-05
 */
@Service
public class OrderReconciliationMistakeServiceImpl extends ServiceImpl<OrderReconciliationMistakeMapper, OrderReconciliationMistake> implements OrderReconciliationMistakeService {

}
