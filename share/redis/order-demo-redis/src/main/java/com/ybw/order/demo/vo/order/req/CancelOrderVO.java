package com.ybw.order.demo.vo.order.req;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * 取消订单
 *
 * @program: order-demo
 * @description:
 * @author: geoffrey
 * @create: 2022-02-20 09:25
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CancelOrderVO {
    /**
     * 订单号
     */
    @NotNull
    private String orderNo;
}
