package com.ybw.order.demo.service;


import com.ybw.order.demo.dto.HandleStockStatusDTO;

/**
 * @author ybwei
 * @Description 生产者发送
 * @date 2021/10/18 14:06
 **/
public interface SenderService {
    /**
     * 订单-过期失效
     *
     * @param ordNo 订单号
     * @return void
     * @throws
     * @methodName: sendOrderExpireTime
     * @author ybwei
     * @date 2022/2/21 18:08
     */
    void sendOrderExpireTime(String ordNo);

    /**
     * 订单-加或减库存(mysql)（同步-顺序执行）
     *
     * @param handleStockStatusDTO
     * @return void
     * @throws
     * @methodName: sendOrderHandleStockStatus
     * @author ybwei
     * @date 2022/3/11 17:11
     */
    void sendOrderHandleStockStatus(HandleStockStatusDTO handleStockStatusDTO);

}
