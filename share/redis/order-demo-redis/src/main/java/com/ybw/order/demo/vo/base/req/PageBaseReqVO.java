package com.ybw.order.demo.vo.base.req;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 分页请求参数基类
 *
 * @author ybwei
 * @date 2022/2/15 18:17
 **/
@Data
public class PageBaseReqVO {
    /**
     * 页码
     *
     * @author: ybwei
     * @date: 2022/2/15 15:22
     */
    @NotNull(message = "当前页不能为空")
    protected Long pageNum;
    /**
     * 每页条数
     *
     * @author: ybwei
     * @date: 2022/2/15 15:22
     */
    @NotNull(message = "每页条数不能为空")
    protected Long pageSize;
}
