package com.ybw.order.demo.config.wx;

import com.ybw.order.demo.utils.GsonUtils;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

/**
 * @Description 公众号配置
 * @author ybwei
 * @date 2021/3/19 17:12
 **/
@Data
@ConfigurationProperties(prefix = "wx.mp")
public class WxMpProperties {

    /**
     * 是否使用redis存储access token
     */
    private boolean useRedis;

    /**
     * 多个公众号配置信息
     */
    private List<MpConfig> configs;

    @Data
    public static class MpConfig {
        /**
         * 设置微信公众号的appid
         */
        private String appId;

        /**
         * 设置微信公众号的app secret
         */
        private String secret;

        /**
         * 设置微信公众号的token
         */
        private String token;

        /**
         * 设置微信公众号的EncodingAESKey
         */
        private String aesKey;
        /**
         * 微信支付商户号
         */
        private String mchId;

        /**
         * 微信支付商户密钥
         */
        private String mchKey;

        /**
         * 服务商模式下的子商户公众账号ID，普通模式请不要配置，请在配置文件中将对应项删除
         */
        private String subAppId;

        /**
         * 服务商模式下的子商户号，普通模式请不要配置，最好是请在配置文件中将对应项删除
         */
        private String subMchId;

        /**
         * apiclient_cert.p12文件的绝对路径，或者如果放在项目中，请以classpath:开头指定
         */
        private String keyPath;
        /**
         *  @Description 异步接收微信支付结果通知的回调地址，通知url必须为外网可访问的url，不能携带参数。
         *  @author: ybwei
         *  @Date: 2021/3/22 14:55
         */ 
        private String notifyUrl;
        /**
         *  @Description 退款回调地址
         *  @author: ybwei
         *  @Date: 2021/3/22 14:56
         */ 
        private String refundUrl;
    }

    @Override
    public String toString() {
        return GsonUtils.toJsonString(this);
    }
}
