package com.ybw.order.demo.utils;

import lombok.extern.slf4j.Slf4j;

import java.util.function.Supplier;

/**
 * @author ybw
 * @version V1.0
 * @className RetryUtils
 * @date 2022/8/17
 **/
@Slf4j
public class RetryUtils {

    /**
     * 重试
     *
     * @param maxRetryCount 最大重试次数
     * @param supplier
     * @methodName: retry
     * @return: void
     * @author: ybw
     * @date: 2022/8/17
     **/
    public static Object retry(int maxRetryCount, Supplier<Object> supplier) {
        //1、重试次数=0
        int retryCount = 0;
        //2、执行
        return retry(retryCount, maxRetryCount, supplier);
    }

    /**
     * @param retryCount    重试次数
     * @param maxRetryCount 最大重试次数
     * @param supplier
     * @methodName: retry
     * @return: void
     * @author: ybw
     * @date: 2022/8/17
     **/
    private static Object retry(int retryCount, int maxRetryCount, Supplier<Object> supplier) {
        try {
            //1、累计重试次数
            retryCount++;
            //2、执行逻辑
            return supplier.get();
        } catch (Exception e) {
            log.info("重试次数:{}", retryCount);
            if (retryCount < maxRetryCount) {
                //3、重试次数小于最大重试次数
                return retry(retryCount, maxRetryCount, supplier);
            }
            //4、打印日志，抛出异常
            log.error("RetryUtils retry 最大重试次数:{},重试次数:{}, error:", maxRetryCount, retryCount, e);
            throw e;
        }
    }
}
