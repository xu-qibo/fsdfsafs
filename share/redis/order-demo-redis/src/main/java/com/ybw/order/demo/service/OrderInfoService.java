package com.ybw.order.demo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ybw.order.demo.entity.OrderInfo;
import com.ybw.order.demo.vo.order.req.CancelOrderVO;
import com.ybw.order.demo.vo.order.req.CreateOrderReqVO;

/**
 * <p>
 * 订单 服务类
 * </p>
 *
 * @author ybwei
 * @since 2022-02-20
 */
public interface OrderInfoService extends IService<OrderInfo> {

    /**
     * @className OrderService
     * @author geoffrey
     * @date 2022/2/20
     * @version V1.0
     **/
    void createOrder2(CreateOrderReqVO createOrderReqVO);

    /**
     * @param cancelOrderVO
     * @methodName: cancelOrder
     * @return: void
     * @author: geoffrey
     * @date: 2022/2/20
     **/
    void cancelOrder(CancelOrderVO cancelOrderVO);

    /**
     * 支付通知-成功
     *
     * @param orderNo
     * @methodName: notifyOrder
     * @return: void
     * @author: geoffrey
     * @date: 2022/2/20
     **/
    void notifyOrder(String orderNo);


    /**
     * 生成订单号
     *
     * @methodName: generateOrderNo
     * @return: java.lang.String
     * @author: ybw
     * @date: 2022/9/2
     **/
    String generateOrderNo();

    /**
     * 下单
     *
     * @param createOrderReqVO
     * @methodName: createOrder
     * @return: void
     * @author: ybw
     * @date: 2022/9/2
     **/
    void createOrder(CreateOrderReqVO createOrderReqVO);

    /**
     * 订单-微信对账
     *
     * @methodName: reconciliation
     * @return: void
     * @author: ybwei
     * @date: 2022/9/5
     **/
    void reconciliation() throws Exception;
}
