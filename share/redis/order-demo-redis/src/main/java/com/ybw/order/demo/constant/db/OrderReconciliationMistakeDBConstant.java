package com.ybw.order.demo.constant.db;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 订单-微信对账-差错表
 *
 * @author ybw
 * @version V1.0
 * @className OrderReconciliationMistakeDBConstant
 * @date 2022/4/8
 **/
public interface OrderReconciliationMistakeDBConstant {

    /**
     * 类型 1：漏单；2：金额不一致；
     *
     * @author ybw
     * @version V1.0
     * @className OrderReconciliationMistakeDBConstant
     * @date 2022/4/8
     **/
    @AllArgsConstructor
    @Getter
    enum Type {
        /**
         * 漏单
         *
         * @author: ybw
         * @date: 2022/4/8
         **/
        LOST_LIST(1,"漏单"),
        /**
         * 金额不一致
         *
         * @author: ybw
         * @date: 2022/4/8
         **/
        MONEY_DIFFER(2,"金额不一致");
        private Integer code;
        private String msg;
    }

    /**
     * 状态  0：未处理；1：处理中；2：处理完成；
     *
     * @author ybw
     * @version V1.0
     * @className OrderReconciliationMistakeDBConstant
     * @date 2022/4/8
     **/
    interface Status {
        /**
         * 未处理
         *
         * @author: ybw
         * @date: 2022/4/8
         **/
        Integer NOT_HANDLE = 0;
        /**
         * 处理中
         *
         * @author: ybw
         * @date: 2022/4/8
         **/
        Integer HANDLE = 1;
        /**
         * 处理完成
         *
         * @author: ybw
         * @date: 2022/4/8
         **/
        Integer COMPLETE = 2;
    }

}
