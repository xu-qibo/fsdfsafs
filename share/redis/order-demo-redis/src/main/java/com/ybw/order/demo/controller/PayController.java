package com.ybw.order.demo.controller;

import com.ybw.order.demo.exception.BizException;
import com.ybw.order.demo.service.OrderInfoService;
import com.ybw.order.demo.service.RedisService;
import com.ybw.order.demo.vo.order.req.CancelOrderVO;
import com.ybw.order.demo.vo.order.req.CreateOrderReqVO;
import com.ybw.order.demo.vo.base.res.ApiResult;
import com.ybw.order.demo.vo.base.res.ApiResultGenerator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * @program: order-demo
 * @description:
 * @author: geoffrey
 * @create: 2022-02-19 21:27
 */
@Slf4j
@RestController
@RequestMapping("/order")
public class PayController {

    @Resource
    private OrderInfoService orderService;
    @Resource
    private RedisService redisService;

    /**
     * 生成订单号
     *
     * @methodName: generateOrderNo
     * @return: java.lang.String
     * @author: geoffrey
     * @date: 2022/2/19
     **/
    @PostMapping("/generateOrderNo")
    public ApiResult generateOrderNo() throws Exception {
        return ApiResultGenerator.successResult(orderService.generateOrderNo());
    }

    /**
     * 下单-分布式锁实现
     *
     * @param createOrderReqVO
     * @methodName: createOrder2
     * @return: java.lang.Integer
     * @author: geoffrey
     * @date: 2022/2/19
     **/
    @PostMapping("/createOrder2")
    public String createOrder2(@Valid @RequestBody CreateOrderReqVO createOrderReqVO) throws Exception {
        //1、获取分布式锁，等待时间为10秒
        boolean lockFlag = redisService.tryLock(createOrderReqVO.getGoodsInfoId().toString(), 10);
        if (lockFlag) {
            try {
                orderService.createOrder2(createOrderReqVO);
            } catch (BizException e) {
                log.error("createOrder2 error:", e);
                return e.getMessage();
            } finally {
                redisService.unlock(createOrderReqVO.getGoodsInfoId().toString());
            }
            return "success";
        }
        log.info("fail");
        return "fail";
    }


    /**
     * 下单-非分布式锁实现（推荐）
     *
     * @param createOrderReqVO
     * @methodName: createOrder2
     * @return: java.lang.Integer
     * @author: geoffrey
     * @date: 2022/2/19
     **/
    @PostMapping("/createOrder")
    public ApiResult createOrder(@Valid @RequestBody CreateOrderReqVO createOrderReqVO) throws Exception {
        orderService.createOrder(createOrderReqVO);
        return ApiResultGenerator.successResult(null);
    }


    /**
     * 取消订单
     *
     * @param cancelOrderVO
     * @methodName: cancelOrder
     * @return: java.lang.String
     * @author: geoffrey
     * @date: 2022/2/20
     **/
    @PostMapping("/cancelOrder")
    public String cancelOrder(@Valid @RequestBody CancelOrderVO cancelOrderVO) throws Exception {
        orderService.cancelOrder(cancelOrderVO);
        return "success";
    }

    /**
     * 支付通知-成功（假的，真实的微信或支付宝以官方文档为准）
     *
     * @param orderNo
     * @methodName: notifyOrder
     * @return: java.lang.String
     * @author: geoffrey
     * @date: 2022/2/20
     **/
    @PostMapping("/notifyOrder")
    public String notifyOrder(String orderNo) {
        orderService.notifyOrder(orderNo);
        return "success";
    }
}