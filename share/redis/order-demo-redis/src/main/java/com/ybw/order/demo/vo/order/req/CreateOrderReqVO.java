package com.ybw.order.demo.vo.order.req;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @program: order-demo
 * @description:
 * @author: geoffrey
 * @create: 2022-02-19 21:37
 */
@Data
public class CreateOrderReqVO {
    /**
     * 订单号
     */
    @NotNull
    private String orderNo;
    /**
     * goods的id
     */
    @NotNull
    private Long goodsInfoId;
    /**
     * 购买数量
     *
     * @author: geoffrey
     * @date: 2022/2/19
     **/
    @NotNull
    private Integer buyCount;
}
