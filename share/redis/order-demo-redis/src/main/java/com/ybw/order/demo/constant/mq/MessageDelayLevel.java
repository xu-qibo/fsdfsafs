package com.ybw.order.demo.constant.mq;

/**
 * @author ybwei
 * @description 1s 5s 10s 30s 1m 2m 3m 4m 5m 6m 7m 8m 9m 10m 20m 30m 1h 2h
 * @date 2021/12/14 15:14
 **/
public interface MessageDelayLevel {
    /**
     * @description 1s
     * @author: ybwei
     * @date: 2021/12/14 15:15
     */
    int ONE = 1;
    /**
     * @description 5s
     * @author: ybwei
     * @date: 2021/12/14 15:17
     */
    int TWO = 2;
    /**
     * @description 10s
     * @author: ybwei
     * @date: 2021/12/14 15:18
     */
    int THREE = 3;
    /**
     * @description 30s
     * @author: ybwei
     * @date: 2021/12/14 15:18
     */
    int FOUR = 4;
    /**
     * @description 1m
     * @author: ybwei
     * @date: 2021/12/14 15:18
     */
    int FIVE = 5;
    /**
     * @description 2m
     * @author: ybwei
     * @date: 2021/12/14 15:18
     */
    int SIX = 6;
    /**
     * @description 3m
     * @author: ybwei
     * @date: 2021/12/14 15:19
     */
    int SEVEN = 7;
    /**
     * @description 4m
     * @author: ybwei
     * @date: 2021/12/14 15:20
     */
    int EIGHT = 8;
    /**
     * @description 5m
     * @author: ybwei
     * @date: 2021/12/14 15:20
     */
    int NINE = 9;
    /**
     * @description 6m
     * @author: ybwei
     * @date: 2021/12/14 15:20
     */
    int TEN = 10;
    /**
     * @description 7m
     * @author: ybwei
     * @date: 2021/12/14 15:20
     */
    int ELEVEN = 11;
    /**
     * @description 8m
     * @author: ybwei
     * @date: 2021/12/14 15:20
     */
    int TWELVE = 12;
    /**
     * @description 9m
     * @author: ybwei
     * @date: 2021/12/14 15:22
     */
    int THIRTEEN = 13;
    /**
     * @description 10m
     * @author: ybwei
     * @date: 2021/12/14 15:22
     */
    int FOURTEEN = 14;
    /**
     * @description 20m
     * @author: ybwei
     * @date: 2021/12/14 15:22
     */
    int FIFTEEN = 15;
    /**
     * @description 30m
     * @author: ybwei
     * @date: 2021/12/14 15:23
     */
    int SIXTEEN = 16;
    /**
     * @description 1h
     * @author: ybwei
     * @date: 2021/12/14 15:23
     */
    int SEVENTEEN = 17;
    /**
     * @description 2h
     * @author: ybwei
     * @date: 2021/12/14 15:23
     */
    int EIGHTEEN = 18;
}
