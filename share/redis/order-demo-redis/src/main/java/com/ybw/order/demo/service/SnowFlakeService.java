package com.ybw.order.demo.service;

/**
 * 雪花算法
 *
 * @author ybw
 * @version V1.0
 * @className SnowFlakeService
 * @date 2022/6/9
 **/
public interface SnowFlakeService {

    /**
     * 获得下一个ID
     *
     * @methodName: nextId
     * @return: long
     * @author: ybw
     * @date: 2022/6/9
     **/
    long nextId();
}
