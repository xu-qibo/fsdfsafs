package com.ybw.order.demo.constant;

/**
 * 订单常量
 *
 * @author ybwei
 * @date 2022/2/24 16:31
 **/
public interface OrderConstant {

    /**
     * 锁
     *
     * @author ybwei
     * @date 2022/2/24 16:32
     **/
    interface Lock {
        /**
         * 等待锁的最长时间（单位：秒）
         *
         * @author: ybwei
         * @date: 2022/2/24 16:33
         */
        Long TIMEOUT = 6L;
    }

    /**
     * 创建订单
     *
     * @author: ybwei
     * @date: 2022/2/24 16:35
     */
    interface Create {
        /**
         * 用户一种商品只能买一个
         *
         * @author: ybwei
         * @date: 2022/2/24 16:35
         */
        Integer BUY_COUNT = 1;
        /**
         * 超时时间（单位：秒）
         *
         * @author: ybwei
         * @date: 2022/3/9 17:45
         */
        Long GOODS_INFO_DTO_TIMEOUT = 86400L;
        /**
         * redis-商品库存字段
         *
         * @author: ybwei
         * @date: 2022/3/10 18:37
         */
        String GOODS_INFO_DTO_STOCK_COUNT = "stockCount";

    }


    /**
     * mq前缀
     *
     * @author ybwei
     * @date 2022/3/11 17:02
     **/
    interface MqPre {
        /**
         * nft转移
         *
         * @author: ybwei
         * @date: 2022/3/11 17:04
         */
        String ORDER_NFT_TRANSFER = "ORDER_NFT_TRANSFER_";
        /**
         * 加或减库存
         *
         * @author: ybwei
         * @date: 2022/3/11 17:12
         */
        String HANDLE_STOCK_STATUS = "HANDLE_STOCK_STATUS_";
    }

    /**
     * 订单号前缀
     *
     * @author ybw
     * @version V1.0
     * @className OrderConstant
     * @date 2022/7/27
     **/
    interface OrderNoPre {
        /**
         * 预留第一、二位前缀
         *
         * @author: ybw
         * @date: 2022/7/27
         **/
        String RESERVE = "10";
        /**
         * 项目名-超市
         *
         * @author: ybw
         * @date: 2022/7/27
         **/
        String SUPERMARKET = "01";

    }
}
