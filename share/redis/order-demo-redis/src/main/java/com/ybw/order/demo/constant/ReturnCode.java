package com.ybw.order.demo.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author ybwei
 * @Description
 * @date 2021/4/8 18:06
 **/
@AllArgsConstructor
@Getter
public enum ReturnCode {
    /**
     * @Description 成功
     * @author: ybwei
     * @Date: 2021/4/8 18:06
     */
    SUCCESS(200, "成功"),
    /**
     * @Description 错误
     * @author: ybwei
     * @Date: 2021/4/8 18:12
     */
    ERROR(1001, "错误"),

    /**
     * @Description 参数失效
     * @author: ybwei
     * @Date: 2021/4/8 18:08
     */
    PARAM_INVALID(2001, "参数失效"),
    /**
     * 商品库存不足
     *
     * @author: ybwei
     * @date: 2022/2/21 10:53
     */
    GOODS_INFO_NOT_ENOUGH(2002, "商品库存不足"),
    GOODS_INFO_EXCEED(2003, "商品库存不足"),
    /**
     * 未到发售时间
     *
     * @author: ybwei
     * @date: 2022/2/21 17:13
     */
    GOODS_INFO_SALE_NOT_TIME_ERROR(2004, "未到发售时间"),
    ;
    private int code;
    private String msg;
}
