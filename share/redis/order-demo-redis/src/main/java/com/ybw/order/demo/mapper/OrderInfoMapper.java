package com.ybw.order.demo.mapper;

import com.ybw.order.demo.entity.OrderInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单 Mapper 接口
 * </p>
 *
 * @author ybwei
 * @since 2022-02-20
 */
public interface OrderInfoMapper extends BaseMapper<OrderInfo> {

}
