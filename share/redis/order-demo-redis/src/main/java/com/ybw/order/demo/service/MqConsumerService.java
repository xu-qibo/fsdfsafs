package com.ybw.order.demo.service;

import com.ybw.order.demo.entity.MqConsumer;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.rocketmq.common.message.MessageExt;

import java.util.function.Consumer;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author ybwei
 * @since 2022-09-04
 */
public interface MqConsumerService extends IService<MqConsumer> {

    /**
     * 保存消费信息
     *
     * @param msg
     * @param consumer
     * @return void
     * @throws
     * @methodName: saveMqConsumer
     * @author ybwei
     * @date 2022/3/9 11:15
     */
    void saveMqConsumer(MessageExt msg, Consumer<String> consumer);
}
