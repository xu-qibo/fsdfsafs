package com.ybw.order.demo.constant.mq;

/**
 * @program: order-demo
 * @description:
 * @author: geoffrey
 * @create: 2022-02-20 10:08
 */
public interface MqConstant {
    /**
     * @author: geoffrey
     * @date: 2022/2/20
     **/
    String TOPIC = "test_delay_topic";
    /**
     * @Description 生产者-分组
     * @author: ybwei
     * @Date: 2021/10/9 13:54
     */
    String PRODUCER_GROUP = "test_producer_group";
    /**
     * @Description:
     * @Author: geoffrey
     * @Date: 2021/10/8
     **/
    String CONSUMER_GROUP="test_consumer_group";
    /**
     * @description 3000毫秒，不建议修改该值，该值应该与broker配置中的sendTimeout一致，发送超时，可临时修改该值，建议解决超时问题，提高broker集群的Tps。
     * @author: ybwei
     * @date: 2021/12/14 15:34
     */
    int TIMEOUT=3000;
}
