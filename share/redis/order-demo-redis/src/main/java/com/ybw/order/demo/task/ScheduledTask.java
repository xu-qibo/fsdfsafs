package com.ybw.order.demo.task;

import com.ybw.order.demo.service.GoodsInfoService;
import com.ybw.order.demo.service.OrderInfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;

import javax.annotation.Resource;

/**
 * 定时任务
 *
 * @author ybwei
 * @version V1.0
 * @className ScheduledTask
 * @date 2022/9/1
 **/
@Component
@Slf4j
public class ScheduledTask {
    @Resource
    private GoodsInfoService goodsInfoService;
    @Resource
    private OrderInfoService orderInfoService;

    /**
     * 今天售卖的商品-一1天一次
     *
     * @methodName: todaySellGoodsInfoListTask
     * @return: void
     * @author: ybwei
     * @date: 2022/9/1
     **/
    @Scheduled(cron = "0 0 1 * * ? *")
    public void todaySellGoodsInfoListTask() {
        log.info("ScheduledTask todaySellGoodsInfoListTask start");
        goodsInfoService.todaySellGoodsInfoListTask();
    }

    /**
     * 订单-微信对账
     * 微信在次日9点启动生成前一天的对账单，建议商户10点后再获取；
     *
     * @methodName: reconciliationOrder
     * @return: ResponseWrapper<WxPayMpOrderResult>
     * @author: ybwei
     * @date: 2022/9/5
     **/
    @Scheduled(cron = "0 0 10 * * ? ")
    public void reconciliationOrder() throws Exception {
        try {
            orderInfoService.reconciliation();
        } catch (Exception e) {
            log.error("ScheduledTask reconciliationOrder error:", e);
        }
    }
}
