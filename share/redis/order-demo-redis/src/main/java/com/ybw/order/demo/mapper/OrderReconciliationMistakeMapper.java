package com.ybw.order.demo.mapper;

import com.ybw.order.demo.entity.OrderReconciliationMistake;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单-微信对账-差错表 Mapper 接口
 * </p>
 *
 * @author ybwei
 * @since 2022-09-05
 */
public interface OrderReconciliationMistakeMapper extends BaseMapper<OrderReconciliationMistake> {

}
