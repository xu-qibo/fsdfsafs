package com.ybw.order.demo.service.impl;

import com.ybw.order.demo.constant.SnowFlakeConstant;
import com.ybw.order.demo.service.SnowFlakeService;
import com.ybw.order.demo.utils.SnowFlake;
import org.springframework.stereotype.Service;

/**
 * @author ybw
 * @version V1.0
 * @className SnowFlakeServiceImpl
 * @date 2022/6/9
 **/
@Service
public class SnowFlakeServiceImpl implements SnowFlakeService {

    private final SnowFlake idWorker = new SnowFlake(SnowFlakeConstant.WORKER_ID, SnowFlakeConstant.DATACENTER_ID);

    @Override
    public long nextId() {
        return idWorker.nextId();
    }
}
