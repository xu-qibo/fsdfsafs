package com.ybw.order.demo.service.impl;

import com.ybw.order.demo.entity.MqProducer;
import com.ybw.order.demo.mapper.MqProducerMapper;
import com.ybw.order.demo.service.MqProducerService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ybwei
 * @since 2022-09-04
 */
@Service
public class MqProducerServiceImpl extends ServiceImpl<MqProducerMapper, MqProducer> implements MqProducerService {

}
