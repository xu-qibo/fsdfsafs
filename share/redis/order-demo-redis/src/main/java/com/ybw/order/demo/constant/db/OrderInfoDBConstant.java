package com.ybw.order.demo.constant.db;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author ybwei
 * @date 2022/2/16 11:06
 **/
public interface OrderInfoDBConstant {

    /**
     * 订单状态 状态 0：待付；1：支付成功；2：已取消；
     *
     * @author ybwei
     * @date 2022/2/16 11:07
     **/
    @Getter
    @AllArgsConstructor
    enum Status {
        /**
         * 等待付款
         *
         * @author: ybwei
         * @date: 2022/2/16 11:09
         */
        WAIT_PAY(0, "等待付款"),

        /**
         * 成功
         *
         * @author: ybwei
         * @date: 2022/2/16 11:11
         */
        SUCCESS(1, "成功"),

        /**
         * 已取消
         *
         * @author: ybwei
         * @date: 2022/2/16 11:11
         */
        CANCEL(2, "已取消"),
        ;

        private Integer code;
        private String msg;

        /**
         * 根据code获取msg
         *
         * @param code
         * @return java.lang.String
         * @throws
         * @methodName: getMsgByCode
         * @author ybwei
         * @date 2022/2/16 11:28
         */
        public static String getMsgByCode(Integer code) {
            for (Status status : values()) {
                if (status.getCode().equals(code)) {
                    return status.getMsg();
                }
            }
            return null;
        }
    }

}
