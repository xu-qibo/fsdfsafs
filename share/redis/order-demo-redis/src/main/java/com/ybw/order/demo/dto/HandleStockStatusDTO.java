package com.ybw.order.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author ybwei
 * @date 2022/3/11 17:56
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class HandleStockStatusDTO {
    /**
     * 商品id
     *
     * @author: ybwei
     * @date: 2022/3/11 17:54
     */
    private Long goodsId;
    /**
     * 操作：加或减
     *
     * @author: ybwei
     * @date: 2022/3/11 17:56
     */
    private Handle handle;


    /**
     * 操作
     *
     * @author ybwei
     * @date 2022/3/11 17:56
     **/
    public enum Handle {
        /**
         * 减
         *
         * @author: ybwei
         * @date: 2022/3/11 17:55
         */
        MINUS,
        /**
         * 加
         *
         * @author: ybwei
         * @date: 2022/3/11 17:56
         */
        PLUS;
    }
}
