package com.ybw.order.demo.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 订单-微信对账-差错表
 * </p>
 *
 * @author ybwei
 * @since 2022-09-05
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class OrderReconciliationMistake implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     *  类型 1：漏单；2：金额不一致；
     */
    private Integer type;

    /**
     *  order_info的id 
     */
    private Long orderId;

    /**
     *  订单号 
     */
    private String ordNo;

    /**
     *  订单状态 1：等待付款；2：付款成功；3：已取消；
     */
    private Integer ordStatus;

    /**
     *  实付总金额（交易凭证）。
     */
    private BigDecimal ordTotalMoneyPaid;

    /**
     *  微信-订单号 
     */
    private String wxOrdNo;

    /**
     *  微信-订单状态 
     */
    private Integer wxOrdStatus;

    /**
     *  微信-实付总金额 
     */
    private BigDecimal wxOrdTotalMoneyPaid;

    /**
     *  状态 0：未处理；1：处理中；2：处理完成；
     */
    private Integer status;

    /**
     *  创建时间 
     */
    private LocalDateTime createTime;

    /**
     *  修改时间 
     */
    private LocalDateTime updateTime;

    /**
     *  是否有效 0：无效；1：有效；
     */
    private Integer isDel;


}
