package com.ybw.order.demo.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 * 订单-微信对账-差错表 前端控制器
 * </p>
 *
 * @author ybwei
 * @since 2022-09-05
 */
@Controller
@RequestMapping("/orderReconciliationMistake")
public class OrderReconciliationMistakeController {

}

