package com.ybw.order.demo.mapper;

import com.ybw.order.demo.entity.GoodsInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 商品 Mapper 接口
 * </p>
 *
 * @author ybwei
 * @since 2022-02-20
 */
public interface GoodsInfoMapper extends BaseMapper<GoodsInfo> {

    /**
     * 修改库存
     *
     * @param goodsId
     * @param buyCount
     * @methodName: updateStockCount
     * @return: int
     * @author: geoffrey
     * @date: 2022/2/20
     **/
    int updateStockCount(@Param("goodsId") Long goodsId, @Param("buyCount") Integer buyCount);

    /**
     * 减库存并更新状态
     *
     * @param id
     * @param buyCount
     * @methodName: minusStockStatus
     * @return: int
     * @author: ybwei
     * @date: 2022/9/1
     **/
    int minusStockStatus(@Param("id") Long id, @Param("buyCount") int buyCount);

    /**
     * 减库存
     *
     * @param id
     * @param buyCount
     * @methodName: minusStock
     * @return: int
     * @author: ybwei
     * @date: 2022/9/1
     **/
    int minusStock(@Param("id") Long id, @Param("buyCount") int buyCount);

    /**
     * 加库存并更新状态为已结束
     *
     * @param id
     * @param buyCount
     * @methodName: plusStockStatusEnd
     * @return: int
     * @author: ybwei
     * @date: 2022/9/1
     **/
    int plusStockStatusEnd(@Param("id") Long id, @Param("buyCount") int buyCount);

    /**
     * 加库存并更新状态为
     *
     * @param id
     * @param buyCount
     * @methodName: plusStockStatus
     * @return: int
     * @author: ybwei
     * @date: 2022/9/1
     **/
    int plusStockStatus(@Param("id") Long id, @Param("buyCount") int buyCount);

    /**
     * 加库存
     *
     * @param id
     * @param buyCount
     * @methodName: plusStock
     * @return: int
     * @author: ybwei
     * @date: 2022/9/1
     **/
    int plusStock(@Param("id") Long id, @Param("buyCount") int buyCount);

}
