package com.ybw.order.demo.mapper;

import com.ybw.order.demo.entity.MqConsumer;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ybwei
 * @since 2022-09-04
 */
public interface MqConsumerMapper extends BaseMapper<MqConsumer> {

}
