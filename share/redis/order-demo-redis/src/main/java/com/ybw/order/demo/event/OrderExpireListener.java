package com.ybw.order.demo.event;

import com.ybw.order.demo.constant.mq.MqConstant;
import com.ybw.order.demo.service.OrderInfoService;
import com.ybw.order.demo.vo.order.req.CancelOrderVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @program: order-demo
 * @description:
 * @author: geoffrey
 * @create: 2022-02-20 10:14
 */
@Component
@Slf4j
@RocketMQMessageListener(topic = MqConstant.TOPIC, consumerGroup = MqConstant.CONSUMER_GROUP)
public class OrderExpireListener implements RocketMQListener<MessageExt> {
    @Resource
    private OrderInfoService orderInfoService;

    @Override
    public void onMessage(MessageExt messageExt) {
        String orderNo = new String(messageExt.getBody());
        log.info("keys:{},msg:{}", messageExt.getKeys(), orderNo);
        orderInfoService.cancelOrder(new CancelOrderVO(orderNo));
    }
}
