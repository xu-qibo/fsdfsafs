package com.ybw.order.demo.exception;

import com.ybw.order.demo.constant.ReturnCode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 业务异常
 *
 * @author ybwei
 * @Description
 * @date 2021/4/7 15:46
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BizException extends RuntimeException {
    private Integer code;

    /**
     * @MethodName: MyException
     * @Description:
     * @Param: [returnCode]
     * @Return:
     * @Author: geoffrey
     * @Date: 2021/4/9
     **/
    public BizException(ReturnCode returnCode) {
        super(returnCode.getMsg());
        this.code=returnCode.getCode();
    }
}
