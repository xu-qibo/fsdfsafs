package com.ybw.order.demo.constant.mq;

/**
 * ma重试次数
 *
 * @author ybw
 * @version V1.0
 * @className MqRetryConstant
 * @date 2022/8/17
 **/
public interface MqRetryConstant {
    /**
     * mq发送最大重试次数
     *
     * @author: ybw
     * @date: 2022/8/17
     **/
    int MAX_SEND_RETRY_COUNT = 5;
}
