package com.ybw.order.demo.constant;

/**
 * 雪花算法
 *
 * @author ybw
 * @version V1.0
 * @className SnowFlakeConstant
 * @date 2022/6/10
 **/
public interface SnowFlakeConstant {
    /**
     * 工作机器（服务器）ID (0~31)
     *
     * @author: ybw
     * @date: 2022/6/10
     **/
    long WORKER_ID = 0;
    /**
     * 数据中心(机房)ID (0~31)
     *
     * @author: ybw
     * @date: 2022/6/10
     **/
    long DATACENTER_ID = 1;
}
