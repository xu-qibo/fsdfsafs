package com.ybw.order.demo.service;

import org.redisson.api.RLock;

import java.util.concurrent.TimeUnit;

/**
 * @author geoffrey
 * @version V1.0
 * @className RedisService
 * @date 2022/2/17
 **/
public interface RedisService {
    //----------------------------------------------分布式锁 start------------------------------------------------------

    /**
     * 阻塞当前线程，去竞争锁，直到竞争锁成功（极度不建议使用该api）
     *
     * @param lockKey
     * @return void
     * @throws
     * @methodName: lock
     * @author ybwei
     * @date 2022/2/18 14:13
     */
    RLock lock(String lockKey);

    /**
     * 去竞争锁，如果成功，返回值为true 。 如果失败，返回值为false。只尝试一次。
     *
     * @param lockKey
     * @return boolean
     * @throws
     * @methodName: tryLock
     * @author ybwei
     * @date 2022/2/18 14:15
     */
    boolean tryLock(String lockKey);

    /**
     * 去竞争锁，如果成功，返回值为true 。 如果失败，返回值为false。多次尝试，直到超过timeout。
     *
     * @param lockKey
     * @param timeout 等待锁的最长时间（单位：秒）
     * @return boolean
     * @throws
     * @methodName: tryLock
     * @author ybwei
     * @date 2022/2/18 14:06
     */
    boolean tryLock(String lockKey, long timeout);

    /**
     * 解锁
     *
     * @param lockKey
     * @return void
     * @throws
     * @methodName: unlock
     * @author ybwei
     * @date 2022/2/18 14:20
     */
    void unlock(String lockKey);

    /**
     * 解锁
     *
     * @methodName: unlock [lock]
     * @return: void
     * @author: geoffrey
     * @date: 2022/3/28
     **/
    void unlock(RLock lock);

    /**
     * 检查当前线程是否获得此锁
     *
     * @methodName: isHeldByCurrentThread [lock]
     * @return: boolean
     * @author: geoffrey
     * @date: 2022/3/28
     **/
    boolean isHeldByCurrentThread(RLock lock);
    //----------------------------------------------分布式锁 end------------------------------------------------------

    //------------------------------------hash start----------------------------------------------------------

    /**
     * @param key
     * @param t
     * @param time
     * @param timeUnit
     * @methodName: hmsetObject
     * @return: boolean
     * @author: ybwei
     * @date: 2022/9/4
     **/
    <T> boolean hmSetObject(String key, T t, Long time, TimeUnit timeUnit);

    /**
     * @param key
     * @methodName: hmget
     * @return: java.util.Map<java.lang.Object, java.lang.Object>
     * @author: ybwei
     * @date: 2022/9/4
     **/
    <T> T hmget(String key, Class<T> clazz);

    //------------------------------------hash end----------------------------------------------------------
}
