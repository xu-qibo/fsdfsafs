package com.ybw.order.demo.config.mq;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @program: order-demo-redis
 * @description:
 * @author: geoffrey
 * @create: 2022-09-04 09:48
 */
@Data
@ConfigurationProperties(prefix = "mq.topic")
@Configuration
public class MqTopicConfig {
    /**
     * @author: ybwei
     * @date: 2022/9/4
     **/
    private String orderExpireTime;
    private String orderExpireTimeProducerGroup;
    private String orderHandleStockStatus;
    private String orderHandleStockStatusProducerGroup;
}
