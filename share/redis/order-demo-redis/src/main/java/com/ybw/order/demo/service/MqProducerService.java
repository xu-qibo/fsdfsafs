package com.ybw.order.demo.service;

import com.ybw.order.demo.entity.MqProducer;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ybwei
 * @since 2022-09-04
 */
public interface MqProducerService extends IService<MqProducer> {

}
