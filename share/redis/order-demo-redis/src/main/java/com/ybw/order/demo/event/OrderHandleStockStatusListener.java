package com.ybw.order.demo.event;

import com.ybw.order.demo.dto.HandleStockStatusDTO;
import com.ybw.order.demo.service.GoodsInfoService;
import com.ybw.order.demo.service.MqConsumerService;
import com.ybw.order.demo.utils.GsonUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.consumer.listener.ConsumeOrderlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeOrderlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerOrderly;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.spring.annotation.ConsumeMode;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * 订单-加或减库存-顺序消费
 *
 * @program: order-demo-redis
 * @description:
 * @author: geoffrey
 * @create: 2022-09-04 18:20
 */
@Component("orderHandleStockStatusListener")
@Slf4j
@RocketMQMessageListener(topic = "${mq.topic.order-handle-stock-status}", consumerGroup = "${mq.topic.order-handle-stock-status-consumer-group}",consumeMode = ConsumeMode.ORDERLY)
public class OrderHandleStockStatusListener implements RocketMQListener<MessageExt> {

    @Resource
    private MqConsumerService mqConsumerService;
    @Resource
    private GoodsInfoService goodsInfoService;

    @Override
    public void onMessage(MessageExt message) {
        //保存消费记录
        mqConsumerService.saveMqConsumer(message, (body) -> {
            HandleStockStatusDTO handleStockStatusDTO = GsonUtils.fromJson(body, HandleStockStatusDTO.class);
            goodsInfoService.handleStockStatusInMysql(handleStockStatusDTO);
        });
    }
}
