package com.ybw.order.demo.service;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

/**
 * @author ybw
 * @version V1.0
 * @className RedisServiceTest
 * @date 2022/9/8
 **/
@SpringBootTest
@Slf4j
class RedisServiceTest {

    @Resource
    private RedisService redisService;

    /**
     * @methodName: tryLock
     * @return: void
     * @author: ybw
     * @date: 2022/9/8
     **/
    @Test
    void tryLock() {
        String lockKey="lock";
        redisService.tryLock(lockKey,3600L);
        log.info("锁成功");
        redisService.unlock(lockKey);
        log.info("解锁成功");

    }
}