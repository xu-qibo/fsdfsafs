package com.ybw.order.demo.utils;

import com.alibaba.fastjson.JSON;
import com.ybw.order.demo.entity.OrderInfo;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
class MapUtilsTest {

    /**
     * @methodName: beanToMap
     * @return: void
     * @author: geoffrey
     * @date: 2022/3/15
     **/
    @Test
    public void beanToMap(){
        OrderInfo orderInfo=new OrderInfo();
        orderInfo.setId(12L);
        orderInfo.setBuyCount(15);
        log.info("orderInfo:{}", JSON.toJSONString(orderInfo));

        Map<Object,Object> map=MapUtils.beanToMap(orderInfo);
        log.info("map:{}", JSON.toJSONString(map));

        OrderInfo orderInfo2=MapUtils.mapToBean(map,OrderInfo.class);
        log.info("orderInfo2:{}", JSON.toJSONString(orderInfo2));

    }
}