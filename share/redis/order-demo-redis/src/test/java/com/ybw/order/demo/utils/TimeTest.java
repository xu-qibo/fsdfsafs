package com.ybw.order.demo.utils;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

/**
 * @program: order-demo-redis
 * @description:
 * @author: geoffrey
 * @create: 2022-09-01 22:19
 */
@Slf4j
public class TimeTest {

    /**
     * @methodName: time
     * @return: void
     * @author: ybwei
     * @date: 2022/9/1
     **/
    @Test
    public void time() {
        LocalDateTime start = LocalDateTime.now().with(LocalTime.MIN);
        LocalDateTime end = LocalDateTime.now().with(LocalTime.MAX);
        log.info("start:{},end:{}", start, end);

        LocalDate localDate=LocalDate.now();
        log.info("localDate:{}",localDate.toString());
    }


}
