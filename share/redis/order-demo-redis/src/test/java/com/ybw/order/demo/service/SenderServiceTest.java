package com.ybw.order.demo.service;

import com.ybw.order.demo.dto.HandleStockStatusDTO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Slf4j
class SenderServiceTest {

    @Resource
    private SenderService senderService;

    /**
     * @methodName: sendOrderHandleStockStatus
     * @return: void
     * @author: ybwei
     * @date: 2022/9/4
     **/
    @Test
    public void sendOrderHandleStockStatus() throws InterruptedException {
        HandleStockStatusDTO handleStockStatusDTO = new HandleStockStatusDTO();
        handleStockStatusDTO.setHandle(HandleStockStatusDTO.Handle.MINUS);
        handleStockStatusDTO.setGoodsId(1L);
        senderService.sendOrderHandleStockStatus(handleStockStatusDTO);
        TimeUnit.DAYS.sleep(1);
    }
}