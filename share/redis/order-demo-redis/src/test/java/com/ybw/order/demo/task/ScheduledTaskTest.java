package com.ybw.order.demo.task;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author ybwei
 * @version V1.0
 * @className ScheduledTaskTest
 * @date 2022/9/4
 **/
@SpringBootTest
@Slf4j
class ScheduledTaskTest {

    @Resource
    private ScheduledTask scheduledTask;

    @Test
    public void todaySellGoodsInfoListTask() {
        scheduledTask.todaySellGoodsInfoListTask();
    }
}