package com.ybw.order.demo.utils;

import com.ybw.order.demo.vo.order.req.CancelOrderVO;
import com.ybw.order.demo.vo.order.req.CreateOrderReqVO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.concurrent.TimeUnit;

/**
 * @author geoffrey
 * @version V1.0
 * @className HttpUtilsTest
 * @date 2022/2/20
 **/
@Slf4j
class HttpUtilsTest {

    /**
     * 创建订单-秒杀
     *
     * @methodName: createOrder2
     * @return: void
     * @author: geoffrey
     * @date: 2022/2/20
     **/
    @Test
    public void createOrder() throws InterruptedException {
        final String url = "http://localhost:8080/order/createOrder";
        for (int i = 0; i < 25; i++) {
            new Thread(() -> {
                CreateOrderReqVO createOrderReqVO = new CreateOrderReqVO();
                createOrderReqVO.setOrderNo(MyStringUtils.generateUUIDNoCenterLine());
                createOrderReqVO.setGoodsInfoId(1L);
                createOrderReqVO.setBuyCount(5);
                HttpUtils.doPostJson(url, GsonUtils.toJsonString(createOrderReqVO));
            }).start();
        }
        TimeUnit.DAYS.sleep(1);
    }

    /**
     * 取消订单
     *
     * @methodName: cancelOrder
     * @return: void
     * @author: geoffrey
     * @date: 2022/2/20
     **/
    @Test
    public void cancelOrder() throws InterruptedException {
        final String url = "http://localhost:8080/order/cancelOrder";
        CancelOrderVO cancelOrderVO = new CancelOrderVO("6b7386a51aa244a5a28153e66a804f9b");
        HttpUtils.doPostJson(url, GsonUtils.toJsonString(cancelOrderVO));
    }

    /**
     * @className HttpUtilsTest
     * @author geoffrey
     * @date 2022/2/20
     * @version V1.0
     **/
    @Test
    public void notifyOrder() throws InterruptedException {
        final String url = "http://localhost:8080/order/notifyOrder?orderNo=08c9c73acff54753b43238323c569ab4";
        HttpUtils.doPostJson(url, null);
    }

}