package com.ybw.redis.page.service;

import com.ybw.redis.page.dto.VisitDTO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

@SpringBootTest
@Slf4j
class VisitServiceTest {

    @Resource
    private VisitService visitService;
    @Resource(name = "taskExecutor")
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    /**
     * @author ybwei
     * @date 2022/3/7 17:54
     **/
    @Test
    public void visit() throws InterruptedException {
        for (long i = 0; i < 100; i++) {
            final long index = i;
            threadPoolTaskExecutor.execute(() -> {
                VisitDTO visitDTO = new VisitDTO(index, 10000 + index);
                visitService.visit(visitDTO);
            });
        }
        TimeUnit.DAYS.sleep(1);
    }

    /**
     * @methodName: visitLimit
     * @return void
     * @throws 
     * @author ybwei
     * @date 2022/3/7 18:10
     */
    @Test
    public void visitLimit() throws InterruptedException {
        for (long i = 0; i < 100; i++) {
            threadPoolTaskExecutor.execute(() -> {
                VisitDTO visitDTO = new VisitDTO(1L, 1L);
                visitService.visit(visitDTO);
            });
        }
        TimeUnit.DAYS.sleep(1);
    }
}