package com.ybw.redis.page.date;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

/**
 * @author ybwei
 * @date 2022/3/8 10:00
 **/
@Slf4j
public class DateTest {

    /**
     * @return void
     * @throws
     * @methodName: print
     * @author ybwei
     * @date 2022/3/8 10:00
     */
    @Test
    public void print() {
        LocalDate now = LocalDate.now();
        log.info("now:{}", now.toString());
    }
}
