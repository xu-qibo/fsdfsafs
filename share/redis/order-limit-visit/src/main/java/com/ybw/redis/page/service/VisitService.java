package com.ybw.redis.page.service;

import com.ybw.redis.page.dto.VisitDTO;

/**
 * @author ybwei
 * @date 2022/3/7 17:54
 **/
public interface VisitService {
    /**
     * 访问
     *
     * @param visitDTO
     * @return void
     * @throws
     * @methodName: visit
     * @author ybwei
     * @date 2022/3/7 17:41
     */
    void visit(VisitDTO visitDTO);
}
