package com.ybw.redis.page.constant;

/**
 * @author ybwei
 * @Description redis前缀
 * @date 2021/3/20 14:15
 **/
public interface RedisPreConstant {
    /**
     * 访问限制
     *
     * @author: ybwei
     * @date: 2022/3/7 17:46
     */
    String VISIT_LIMIT = "VISIT:LIMIT";

}
