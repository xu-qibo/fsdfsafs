package com.ybw.redis.page.constant;

/**
 * @author ybwei
 * @date 2022/3/7 17:37
 **/
public interface VisitConstant {
    /**
     * 用户一天最大访问次数
     *
     * @author: ybwei
     * @date: 2022/3/7 17:38
     */
    Long MAX_VISIT_COUNT = 20L;
}
