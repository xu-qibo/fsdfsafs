package com.ybw.redis.page;

import com.alibaba.fastjson.JSON;
import com.ybw.redis.page.dto.UserDTO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.util.Set;

/**
 * 排序分页(多字段排序)
 *
 * @program: redis-page
 * @description:
 * @author: geoffrey
 * @create: 2022-02-27 22:56
 */
@SpringBootTest
@Slf4j
public class PageMutipleSortTest {

    @Resource
    private RedisTemplate redisTemplate;
    private final String key = "aa";

    /**
     * 初始化
     *
     * @methodName: init
     * @return: void
     * @author: geoffrey
     * @date: 2022/2/27
     **/
    @Test
    public void init() {
        for (long i = 0L; i < 1000L; i++) {
            UserDTO userDTO = new UserDTO(i, "zhangsan" + i, LocalDateTime.now().plusDays(i % 500).with(LocalTime.MIN), i);
            //积分不超过1000
            Long pointLimit=1000L;
            // 按时间升序、积分降序
            double score = userDTO.getCreateDateTime().toInstant(ZoneOffset.of("+8")).toEpochMilli()*pointLimit + (pointLimit - userDTO.getPoint());
            redisTemplate.opsForZSet().add(key, userDTO, score);
        }
    }


    /**
     * 分页
     *
     * @methodName: page
     * @return: void
     * @author: geoffrey
     * @date: 2022/2/27
     **/
    @Test
    public void page() {
        //页码
        int pageNum = 1;
        //一页条数
        int pageSize = 20;
        int start = (pageNum - 1) * pageSize;
        int end = start + pageSize - 1;
        // 时间从小到大排序
        Set<UserDTO> set = redisTemplate.opsForZSet().range(key, start, end);
        log.info("set:{}", JSON.toJSONString(set));
    }
}
