package com.ybw.redis.page.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * @program: redis-page
 * @description:
 * @author: geoffrey
 * @create: 2022-02-27 22:55
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO {
    private Long id;
    private String name;
    /**
     * 创建时间
     *
     * @author: ybw
     * @date: 2023/3/24
     **/
    private LocalDateTime createDateTime;
    /**
     * 积分
     *
     * @author: ybw
     * @date: 2023/3/24
     **/
    private Long point;
}
