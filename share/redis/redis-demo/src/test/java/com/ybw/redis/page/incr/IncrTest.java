package com.ybw.redis.page.incr;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

import javax.annotation.Resource;

/**
 * @program: redis-demo
 * @description:
 * @author: geoffrey
 * @create: 2022-07-13 20:58
 */
@SpringBootTest
public class IncrTest {
    @Resource
    private RedisTemplate redisTemplate;


    @Test
    public void increment() {
        redisTemplate.opsForHash().increment("aa", "bb", 1);
    }
}
