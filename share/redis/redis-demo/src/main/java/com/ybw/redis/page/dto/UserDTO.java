package com.ybw.redis.page.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @program: redis-page
 * @description:
 * @author: geoffrey
 * @create: 2022-02-27 22:55
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO {
    private Long id;
    private String name;
    private LocalDateTime createDateTime;
}
