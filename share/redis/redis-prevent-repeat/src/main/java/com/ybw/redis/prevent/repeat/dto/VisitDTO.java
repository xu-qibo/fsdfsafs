package com.ybw.redis.prevent.repeat.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author ybwei
 * @date 2022/3/7 17:48
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class VisitDTO {
    /**
     * 用户id
     *
     * @author: ybwei
     * @date: 2022/3/7 17:48
     */
    private Long userId;
    /**
     * 商品id
     *
     * @author: ybwei
     * @date: 2022/3/7 17:48
     */
    private Long goodsId;
}
