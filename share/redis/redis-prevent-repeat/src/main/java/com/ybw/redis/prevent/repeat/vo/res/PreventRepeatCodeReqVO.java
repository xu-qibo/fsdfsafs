package com.ybw.redis.prevent.repeat.vo.res;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 防重码
 *
 * @author ybw
 * @version V1.0
 * @className PreventRepeatCode
 * @date 2022/6/2
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PreventRepeatCodeReqVO {
    /**
     * 防重码
     *
     * @author: ybw
     * @date: 2022/6/2
     **/
    private String code;
}
