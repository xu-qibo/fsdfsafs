package com.ybw.redis.prevent.repeat.constant;

/**
 * @author ybwei
 * @Description redis前缀
 * @date 2021/3/20 14:15
 **/
public interface RedisPreConstant {
    /**
     * 防重码
     *
     * @author: ybw
     * @date: 2022/6/2
     **/
    String PREVENT_REPEAT_CODE = "PREVENT:REPEAT:CODE:";

}
