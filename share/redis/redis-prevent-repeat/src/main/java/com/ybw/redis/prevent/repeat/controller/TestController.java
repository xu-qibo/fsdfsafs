package com.ybw.redis.prevent.repeat.controller;

import com.ybw.redis.prevent.repeat.vo.res.base.ApiResult;
import com.ybw.redis.prevent.repeat.vo.res.base.ApiResultGenerator;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class TestController {

    /**
     *
     * @methodName: get
     * @return: java.lang.String
     * @author: ybw
     * @date: 2022/6/6
     **/
    @GetMapping("/get")
    public ApiResult get(){
        return ApiResultGenerator.successResult(null);
    }
}
