package com.ybw.redis.prevent.repeat.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * 防重码
 *
 * @author ybw
 * @version V1.0
 * @className PreventRepeatConfig
 * @date 2022/6/6
 **/
@Configuration
@ConfigurationProperties("common.prevent-repeat-code")
@Data
public class PreventRepeatConfig {
    /**
     * 仿重码超时时间
     *
     * @author: ybw
     * @date: 2022/6/6
     **/
    private Long timeout;
    /**
     * 否启动防重，0：不启用；1：启用；
     *
     * @author: ybw
     * @date: 2022/6/6
     **/
    private Integer enable;
    /**
     * 加密盐
     *
     * @author: ybw
     * @date: 2022/6/6
     **/
    private String salt;
    /**
     * 校验防重url
     *
     * @author: ybw
     * @date: 2022/6/6
     **/
    private List<String> includeUrlList;
}
