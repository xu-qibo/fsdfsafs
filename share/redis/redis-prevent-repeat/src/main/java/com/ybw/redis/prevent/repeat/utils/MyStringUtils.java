package com.ybw.redis.prevent.repeat.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.util.DigestUtils;

import java.io.UnsupportedEncodingException;
import java.util.UUID;

/**
 * @author ybwei
 * @date 2022/1/26 14:51
 **/
@Slf4j
public class MyStringUtils {

    /**
     * 获取uuid
     *
     * @return java.lang.String
     * @throws
     * @methodName: generateUUID
     * @author ybwei
     * @date 2022/1/26 14:52
     */
    @SuppressWarnings("AlibabaLowerCamelCaseVariableNaming")
    public static final String generateUUID() {
        return UUID.randomUUID().toString();
    }

    /**
     * 获取uuid（无中线）
     *
     * @return java.lang.String
     * @throws
     * @methodName: generateUUIDNoCenterLine
     * @author ybwei
     * @date 2022/2/18 17:35
     */
    @SuppressWarnings("AlibabaLowerCamelCaseVariableNaming")
    public static final String generateUUIDNoCenterLine() {
        return generateUUID().replace("-", "");
    }

    /**
     * md5加密
     *
     * @param param
     * @param salt
     * @methodName: md5
     * @return: java.lang.String
     * @author: ybw
     * @date: 2022/6/6
     **/
    public static final String md5(String param, String salt) {
        String str = new StringBuilder().append(param).append(salt).toString();
        log.info("MD5待加密字符串:{}", str);
        try {
            String md5 = DigestUtils.md5DigestAsHex(str.getBytes("utf-8"));
            log.info("MD5加密结果:{}", md5);
            return md5;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }
}
