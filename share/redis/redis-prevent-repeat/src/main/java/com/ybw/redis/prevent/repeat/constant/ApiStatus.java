package com.ybw.redis.prevent.repeat.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author ybwei
 * @Title: ApiStatus.java
 * @ProjectName com.spring.pro.mybatis.page
 * @Description:接口返回码
 * @date 2019年9月16日 下午7:46:59
 */
@Getter
@AllArgsConstructor
public enum ApiStatus {
    // ----------------------------系统错误码-------------------------
    /**
     * 成功
     *
     * @author weiyb
     */
    OK(200, "成功"),
    /**
     * 异常
     *
     * @author weiyb
     */
    EXCEPTION(10000, "网络或服务异常，请稍后重试！"),
    /**
     * 失败
     *
     * @author weiyb
     */
    FAIL(10001, "失败"),
    /**
     * 参数不全
     */
    PARAM_INCOMPLETE(10002, "参数不全"),
    /**
     * 鉴权失败
     *
     * @author ybwei
     */
    AUTH_FAIL(10003, "用户未登录"),

    // -------------------------业务错误码------------------------------------
    /**
     * 当前抽奖人数过多，请稍后重试
     *
     * @author ybwei
     */
    DRAW_LIMIT(1004, "当前抽奖人数过多，请稍后重试"),
    /**
     * 今天抽奖次数已满
     *
     * @author ybwei
     */
    DRAW_USER_LIMIT(1005, "今天抽奖次数已满"),
    /**
     * 积分不足
     *
     * @author ybwei
     */
    POINTS_NOT_ENOUGH(1006, "积分不足");

    private Integer code;
    private String depict;
}
