package com.ybw.redis.prevent.repeat.controller;

import com.ybw.redis.prevent.repeat.service.CommonService;
import com.ybw.redis.prevent.repeat.vo.res.PreventRepeatCodeReqVO;
import com.ybw.redis.prevent.repeat.vo.res.base.ApiResult;
import com.ybw.redis.prevent.repeat.vo.res.base.ApiResultGenerator;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 公共接口
 *
 * @author ybwei
 * @date 2022/2/24 13:37
 **/
@RestController
@RequestMapping("/common")
public class CommonController {

    @Resource
    private CommonService commonService;

    /**
     * 获取防重码
     *
     * @methodName: getPreCode
     * @return: com.zt.ztsc.server.vo.base.res.ResponseWrapper
     * @author: ybw
     * @date: 2022/6/2
     **/
    @GetMapping("/getPreCode")
    public ApiResult getPreCode() throws Exception {
        String code = commonService.generatePreCode();
        return ApiResultGenerator.successResult(new PreventRepeatCodeReqVO(code));
    }

}
