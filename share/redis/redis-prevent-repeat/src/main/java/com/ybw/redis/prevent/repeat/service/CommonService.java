package com.ybw.redis.prevent.repeat.service;

/**
 * @author ybw
 * @version V1.0
 * @className CommonService
 * @date 2022/6/2
 **/
public interface CommonService {

    /**
     * 生成防重码
     *
     * @methodName: getPreCode
     * @return: java.lang.String
     * @author: ybw
     * @date: 2022/6/2
     **/
    String generatePreCode();

    /**
     * 防重码是否存在，存在则删除
     *
     * @param code
     * @methodName: isExist
     * @return: boolean
     * @author: ybw
     * @date: 2022/6/2
     **/
    boolean checkExistAndDelKey(String code);
}
