package com.ybw.redis.prevent.repeat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RedisPageApplication {

    public static void main(String[] args) {
        SpringApplication.run(RedisPageApplication.class, args);
    }

}
