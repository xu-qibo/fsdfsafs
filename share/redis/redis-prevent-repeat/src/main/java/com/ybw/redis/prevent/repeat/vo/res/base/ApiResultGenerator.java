package com.ybw.redis.prevent.repeat.vo.res.base;


import com.ybw.redis.prevent.repeat.constant.ApiStatus;

/**
 * @author ybwei
 * @Title: ApiResultGenerator.java
 * @ProjectName com.spring.pro.mybatis.page
 * @Description:该类是用来创建成功、失败返回JSON的工具类
 * @date 2019年9月16日 下午7:50:36
 */
public class ApiResultGenerator {

    /**
     * @param obj
     * @return
     * @Description:返回执行成功视图方法
     * @Author: ybwei
     * @Date: 2019年9月17日 下午8:01:58
     */
    public static ApiResult successResult(Object obj) {
        return result(ApiStatus.OK, obj);
    }

    /**
     * @param apiStatus
     * @param obj
     * @return
     * @Description:创建普通消息方法
     * @Author: ybwei
     * @Date: 2019年9月16日 下午7:59:03
     */
    public static ApiResult result(ApiStatus apiStatus, Object obj) {
        ApiResult apiResult = ApiResult.getInstance();
        apiResult.setCode(apiStatus.getCode());
        apiResult.setData(obj);
        apiResult.setMsg(apiStatus.getDepict());
        return apiResult;
    }

    /**
     * @param apiStatus
     * @return
     * @Description:返回执行失败视图方法
     * @Author: ybwei
     * @Date: 2019年9月16日 下午7:57:43
     */
    public static ApiResult errorResult(ApiStatus apiStatus) {
        return result(apiStatus, null);
    }

    /**
     * @param code
     * @param msg
     * @return com.ybwei.exceptiondemo.vo.base.ApiResult
     * @throws
     * @methodName: errorResult
     * @author ybwei
     * @date 2022/3/17 14:02
     */
    public static ApiResult errorResult(Integer code, String msg) {
        ApiResult apiResult = ApiResult.getInstance();
        apiResult.setCode(code);
        apiResult.setMsg(msg);
        return apiResult;
    }
}
