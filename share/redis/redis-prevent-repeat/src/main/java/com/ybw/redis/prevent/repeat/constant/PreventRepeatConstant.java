package com.ybw.redis.prevent.repeat.constant;

/**
 * @author ybw
 * @version V1.0
 * @className PreventRepeatConstant
 * @date 2022/6/2
 **/
public interface PreventRepeatConstant {

    /**
     * 是否启动防重，0：不启用；1：启用；
     *
     * @author ybw
     * @version V1.0
     * @className PreventRepeatConstant
     * @date 2022/6/2
     **/
    interface Enable {
        /**
         * 不启用
         *
         * @author: ybw
         * @date: 2022/6/2
         **/
        Integer N = 0;
        /**
         * 启用
         *
         * @author: ybw
         * @date: 2022/6/2
         **/
        Integer Y = 1;
    }
}
