package com.ybw.redis.prevent.repeat.interceptor;

import com.ybw.redis.prevent.repeat.config.PreventRepeatConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;

/**
 * @author ybw
 * @version V1.0
 * @className WebConfig
 * @date 2022/6/2
 **/
@Configuration
public class WebConfig implements WebMvcConfigurer {
    @Resource
    private PreventRepeatInterceptor preventRepeatInterceptor;
    @Resource
    private PreventRepeatConfig preventRepeatConfig;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 自定义拦截器，添加拦截路径和排除拦截路径
        registry.addInterceptor(preventRepeatInterceptor).addPathPatterns(preventRepeatConfig.getIncludeUrlList());
    }
}