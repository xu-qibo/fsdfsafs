package com.ybw.redis.prevent.repeat.utils;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

@Slf4j
class MyStringUtilsTest {

    /**
     * @methodName: md5
     * @return: void
     * @author: ybw
     * @date: 2022/6/6
     **/
    @Test
    void md5() {
        //加密值
        String param = "123456";
        //加密盐
        String salt = "2@pqU91P0Nmqb5#e&tnC06PK";
        String md5 = MyStringUtils.md5(param, salt);
        log.info("{}", md5);
    }
}