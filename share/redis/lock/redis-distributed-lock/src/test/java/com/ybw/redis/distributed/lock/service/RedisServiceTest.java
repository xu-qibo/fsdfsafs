package com.ybw.redis.distributed.lock.service;

import com.ybw.redis.distributed.lock.dto.UserDTO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

import javax.annotation.Resource;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author geoffrey
 * @version V1.0
 * @className RedisServiceTest
 * @date 2022/2/17
 **/
@SpringBootTest
@Slf4j
class RedisServiceTest {

    @Resource
    private RedisTemplate<String, Object> redisTemplate;

    /**
     * @methodName: save
     * @return: void
     * @author: geoffrey
     * @date: 2022/2/17
     **/
    @Test
    public void save() {
        UserDTO userDTO = new UserDTO(1L, "zhangsan");
        redisTemplate.opsForValue().set("aa", userDTO);

        UserDTO userDTO1= (UserDTO) redisTemplate.opsForValue().get("aa");
        log.info("{}",userDTO1.toString());

    }
}