package com.ybw.redis.distributed.lock.controller;

import com.ybw.redis.distributed.lock.utils.HttpUtils;
import org.junit.jupiter.api.Test;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

class RedisControllerTest {

    /**
     * 无锁
     *
     * @return void
     * @throws
     * @methodName: testUnLock
     * @author ybwei
     * @date 2022/2/18 12:00
     */
    @Test
    public void testUnLock() throws InterruptedException {
        final String url = "http://localhost:8080/redis/testUnLock";
        for (int i = 0; i < 25; i++) {
            new Thread(() -> {
                HttpUtils.doGet(url);
            }).start();
        }
        TimeUnit.DAYS.sleep(1);
    }

    /**
     * 有锁
     *
     * @return void
     * @throws
     * @methodName: testLock
     * @author ybwei
     * @date 2022/2/18 13:38
     */
    @Test
    public void testLock() throws InterruptedException {
        final String url = "http://localhost:8080/redis/testLock";
        for (int i = 0; i < 25; i++) {
            new Thread(() -> {
                HttpUtils.doGet(url);
            }).start();
        }
        TimeUnit.DAYS.sleep(1);
    }
}