package com.ybw.redis.distributed.lock.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @program: redis-distributed-lock
 * @description:
 * @author: geoffrey
 * @create: 2022-02-17 23:47
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO {
    private Long id;
    private String name;
}
