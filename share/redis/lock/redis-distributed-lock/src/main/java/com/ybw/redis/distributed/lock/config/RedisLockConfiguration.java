package com.ybw.redis.distributed.lock.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.integration.redis.util.RedisLockRegistry;

/**
 * RedisLock的配置
 *
 * @author ybwei
 * @date 2022/2/18 11:44
 **/
@Configuration
public class RedisLockConfiguration {

    @Bean
    public RedisLockRegistry redisLockRegistry(RedisConnectionFactory redisConnectionFactory) {
        //registryKey是分布式锁在redis中的前缀。可以根据项目自己决定。
        return new RedisLockRegistry(redisConnectionFactory, "redis-distributed-lock");
    }

}