package com.ybw.redis.distributed.lock.service;

/**
 * @author geoffrey
 * @version V1.0
 * @className RedisService
 * @date 2022/2/17
 **/
public interface RedisService {
    /**
     * 阻塞当前线程，去竞争锁，直到竞争锁成功（极度不建议使用该api）
     *
     * @param lockKey
     * @return void
     * @throws
     * @methodName: lock
     * @author ybwei
     * @date 2022/2/18 14:13
     */
    void lock(String lockKey);

    /**
     * 去竞争锁，如果成功，返回值为true 。 如果失败，返回值为false。只尝试一次。
     *
     * @param lockKey
     * @return boolean
     * @throws
     * @methodName: tryLock
     * @author ybwei
     * @date 2022/2/18 14:15
     */
    boolean tryLock(String lockKey);

    /**
     * —获取锁成功则返回true；
     * —当失败是分为两种情况：
     * (1)在参数范围内，则不会立即返回值，会等待一段时间，这个时间就是seconds，在这个时间内获取锁成功，则依旧返回true；
     * (2)当过了参数范围后，还是获取锁失败，则立即返回false；
     *
     * 简化解释：去竞争锁，如果成功，返回值为true 。 如果失败，返回值为false。多次尝试，直到超过timeout。
     * @param lockKey
     * @param timeout 等待锁的最长时间（单位：秒）
     * @return boolean
     * @throws
     * @methodName: tryLock
     * @author ybwei
     * @date 2022/2/18 14:06
     */
    boolean tryLock(String lockKey, long timeout);

    /**
     * 解锁
     *
     * @param lockKey
     * @return void
     * @throws
     * @methodName: unlock
     * @author ybwei
     * @date 2022/2/18 14:20
     */
    void unlock(String lockKey);
}
