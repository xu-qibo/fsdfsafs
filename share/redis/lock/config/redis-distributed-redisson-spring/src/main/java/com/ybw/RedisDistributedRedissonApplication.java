package com.ybw;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RedisDistributedRedissonApplication {

    public static void main(String[] args) {
        SpringApplication.run(RedisDistributedRedissonApplication.class, args);
    }

}
