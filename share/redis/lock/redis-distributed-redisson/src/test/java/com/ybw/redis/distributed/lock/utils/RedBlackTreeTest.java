package com.ybw.redis.distributed.lock.utils;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RedBlackTreeTest {

    @Test
    public void testInsert() {
        RedBlackTree tree = new RedBlackTree();
        tree.insert(10);
        tree.insert(20);
        tree.insert(30);
        tree.insert(15);
        tree.insert(25);
        tree.insert(5);
        tree.insert(35);
        assertEquals("10B\n5R 20B\n15R 25R 30B 35R", tree.toString());
    }

    @Test
    public void testDelete() {
        RedBlackTree tree = new RedBlackTree();
        tree.insert(10);
        tree.insert(20);
        tree.insert(30);
        tree.insert(15);
        tree.insert(25);
        tree.insert(5);
        tree.insert(35);
        tree.delete(20);
        assertEquals("10B\n5R 15B 25R\n30B 35B", tree.toString());
    }

    @Test
    public void testSearch() {
        RedBlackTree<Integer> tree = new RedBlackTree<>();
        tree.insert(10);
        tree.insert(20);
        tree.insert(30);
        tree.insert(15);
        tree.insert(25);
        tree.insert(5);
        tree.insert(35);
        assertTrue(tree.search(20));
        assertFalse(tree.search(40));
    }

}