package com.ybw.redis.distributed.lock.service;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.redisson.api.RLock;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Slf4j
class DistributedLockerServiceTest {

    @Resource
    private DistributedLockerService distributedLockerService;

    /**
     * 测试看门狗（会一直续期）
     * 1. 普通的可重入锁
     * 2、拿锁失败时会不停的重试
     * 3、具有Watch Dog 自动延期机制 默认续30s 每隔30/3=10 秒续到30s
     *
     * @methodName: lockWatchDog
     * @return: void
     * @author: geoffrey
     * @date: 2022/3/27
     **/
    @Test
    void lock() {
        RLock rLock = distributedLockerService.lock("aa");
        try {
            while (true) {
                TimeUnit.SECONDS.sleep(5);
                if (distributedLockerService.isHeldByCurrentThread(rLock)) {
                    log.info("锁开始");
                } else {
                    log.info("锁结束,退出");
                    break;
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            if (distributedLockerService.isHeldByCurrentThread(rLock)) {
                distributedLockerService.unlock(rLock);
                log.info("锁结束");
            }
        }
    }

    /**
     * @methodName: lock
     * @return: void
     * @author: geoffrey
     * @date: 2022/3/27
     **/
    @Test
    void lockTimeout() throws InterruptedException {
        RLock rLock = distributedLockerService.lock("aa", TimeUnit.SECONDS, 60);
        log.info("锁开始");
        TimeUnit.SECONDS.sleep(70);
        log.info("锁结束");
        distributedLockerService.unlock(rLock);
    }

    /**
     * 1、尝试拿锁10s后停止重试,返回false
     * 2、具有Watch Dog 自动延期机制 默认续30s
     *
     * @methodName: tryLock
     * @return: void
     * @author: ybw
     * @date: 2023/3/29
     **/
    @Test
    void tryLock() throws InterruptedException {
        final String key="aa";
        for (int i = 0; i < 10; i++) {
            TimeUnit.SECONDS.sleep(2);
            new Thread(() -> {
                boolean rLock = distributedLockerService.tryLock(key, TimeUnit.SECONDS, 20);
                if (rLock) {
                    log.info("锁开始");
                    try {
                        TimeUnit.SECONDS.sleep(35);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                    log.info("锁结束");
                    distributedLockerService.unlock(key);
                } else {
                    log.info("没有获得锁");
                }
            }).start();
        }
        TimeUnit.DAYS.sleep(1);
    }
}