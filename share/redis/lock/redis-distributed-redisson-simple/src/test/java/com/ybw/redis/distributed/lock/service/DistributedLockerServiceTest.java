package com.ybw.redis.distributed.lock.service;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.redisson.api.RLock;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Slf4j
class DistributedLockerServiceTest {

    @Resource
    private DistributedLockerService distributedLockerService;

    /**
     * 测试看门狗（会一直续期）
     *
     * @methodName: lockWatchDog
     * @return: void
     * @author: geoffrey
     * @date: 2022/3/27
     **/
    @Test
    void lockWatchDog() {
        RLock rLock = distributedLockerService.lock("aa");
        try {
            while (true) {
                TimeUnit.SECONDS.sleep(5);
                if (distributedLockerService.isHeldByCurrentThread(rLock)) {
                    log.info("锁开始");
                } else {
                    log.info("锁结束,退出");
                    break;
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            if (distributedLockerService.isHeldByCurrentThread(rLock)) {
                distributedLockerService.unlock(rLock);
                log.info("锁结束");
            }
        }
    }

    /**
     * @methodName: lock
     * @return: void
     * @author: geoffrey
     * @date: 2022/3/27
     **/
    @Test
    void lock() throws InterruptedException {
        RLock rLock = distributedLockerService.lock("aa", TimeUnit.SECONDS, 60);
        log.info("锁开始");
        TimeUnit.SECONDS.sleep(70);
        log.info("锁结束");
        distributedLockerService.unlock(rLock);
    }
}