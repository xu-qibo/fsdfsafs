package com.ybw.order.demo.constant;

/**
 * @program: order-demo
 * @description:
 * @author: geoffrey
 * @create: 2022-02-20 10:08
 */
public interface MqConstant {
    /**
     * @author: geoffrey
     * @date: 2022/2/20
     **/
    String TOPIC = "test_delay_topic";
    /**
     * @Description 生产者-分组
     * @author: ybwei
     * @Date: 2021/10/9 13:54
     */
    String PRODUCER_GROUP = "test_producer_group";
    /**
     * @Description:
     * @Author: geoffrey
     * @Date: 2021/10/8
     **/
    String CONSUMER_GROUP="test_consumer_group";
}
