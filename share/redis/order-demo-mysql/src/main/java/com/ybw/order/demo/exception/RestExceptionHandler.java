package com.ybw.order.demo.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * result统一异常处理 具体controller不需要单独处理异常了
 *
 * @author ybwei
 * @date 2022/1/28 11:16
 **/
@Slf4j
@ControllerAdvice(annotations = RestController.class)
public class RestExceptionHandler {

    /**
     * 默认统一异常处理方法
     *
     * @param e 默认Exception异常对象
     * @return
     * @author weiyb
     */
    @ExceptionHandler
    @ResponseBody
    public String runtimeExceptionHandler(Exception e) {
        log.error("RestExceptionHandler runtimeExceptionHandler error", e);
        return e.getMessage();
    }


    @ExceptionHandler
    @ResponseBody
    public String myExceptionHandler(BizException e) {
        log.info("RestExceptionHandler myExceptionHandler error", e);
        return e.getMessage();
    }
}
