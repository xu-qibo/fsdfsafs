package com.ybw.order.demo.config;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author geoffrey
 * @version V1.0
 * @className MybatisPlusConfig
 * @date 2022/2/19
 **/
@Configuration
@MapperScan("com.ybw.order.demo.mapper")
public class MybatisPlusConfig {

    /**
     * @Description 最新版
     * @author: ybwei
     * @Date: 2021/5/21 18:45
     */
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.MYSQL));
        return interceptor;
    }

}
