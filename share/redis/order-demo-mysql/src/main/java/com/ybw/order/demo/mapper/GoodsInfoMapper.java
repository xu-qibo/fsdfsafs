package com.ybw.order.demo.mapper;

import com.ybw.order.demo.entity.GoodsInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 商品 Mapper 接口
 * </p>
 *
 * @author ybwei
 * @since 2022-02-20
 */
public interface GoodsInfoMapper extends BaseMapper<GoodsInfo> {

    /**
     * 修改库存
     *
     * @param goodsId
     * @param buyCount
     * @methodName: updateStockCount
     * @return: int
     * @author: geoffrey
     * @date: 2022/2/20
     **/
    int updateStockCount(@Param("goodsId") Long goodsId, @Param("buyCount") Integer buyCount);
}
