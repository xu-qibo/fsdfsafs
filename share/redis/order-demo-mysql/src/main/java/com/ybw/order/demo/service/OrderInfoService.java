package com.ybw.order.demo.service;

import com.ybw.order.demo.entity.OrderInfo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ybw.order.demo.vo.CancelOrderVO;
import com.ybw.order.demo.vo.CreateOrderVO;

/**
 * <p>
 * 订单 服务类
 * </p>
 *
 * @author ybwei
 * @since 2022-02-20
 */
public interface OrderInfoService extends IService<OrderInfo> {

    /**
     * @className OrderService
     * @author geoffrey
     * @date 2022/2/20
     * @version V1.0
     **/
    void createOrder(CreateOrderVO createOrderVO);

    /**
     * @param cancelOrderVO
     * @methodName: cancelOrder
     * @return: void
     * @author: geoffrey
     * @date: 2022/2/20
     **/
    void cancelOrder(CancelOrderVO cancelOrderVO);

    /**
     * 支付通知-成功
     *
     * @param orderNo
     * @methodName: notifyOrder
     * @return: void
     * @author: geoffrey
     * @date: 2022/2/20
     **/
    void notifyOrder(String orderNo);
}
