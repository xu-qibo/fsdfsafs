package com.ybw.order.demo.service;

import com.ybw.order.demo.entity.GoodsInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 商品 服务类
 * </p>
 *
 * @author ybwei
 * @since 2022-02-20
 */
public interface GoodsInfoService extends IService<GoodsInfo> {

}
