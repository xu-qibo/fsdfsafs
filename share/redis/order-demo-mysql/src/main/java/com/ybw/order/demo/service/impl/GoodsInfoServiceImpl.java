package com.ybw.order.demo.service.impl;

import com.ybw.order.demo.entity.GoodsInfo;
import com.ybw.order.demo.mapper.GoodsInfoMapper;
import com.ybw.order.demo.service.GoodsInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品 服务实现类
 * </p>
 *
 * @author ybwei
 * @since 2022-02-20
 */
@Service
public class GoodsInfoServiceImpl extends ServiceImpl<GoodsInfoMapper, GoodsInfo> implements GoodsInfoService {

}
