package com.ybw.order.demo.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 商品
 * </p>
 *
 * @author ybwei
 * @since 2022-02-20
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class GoodsInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 库存总数量
     */
    private Integer totalStockCount;

    /**
     * 当前库存数量
     */
    private Integer stockCount;

    /**
     * 上架状态 0：下架；1：上架；
     */
    private Integer isAllowSale;

    /**
     * 销售价格
     */
    private BigDecimal salePrice;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;


}
