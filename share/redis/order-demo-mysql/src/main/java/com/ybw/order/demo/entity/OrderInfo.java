package com.ybw.order.demo.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 订单
 * </p>
 *
 * @author ybwei
 * @since 2022-02-20
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class OrderInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 订单号
     */
    private String orderNo;

    /**
     * goods的id
     */
    private Long goodsId;

    /**
     * 购买数量
     */
    private Integer buyCount;

    /**
     * 销售价格
     */
    private BigDecimal salePrice;

    /**
     * 实付价格
     */
    private BigDecimal payPrice;

    /**
     * 状态 0：待付；1：支付成功；2：已取消；
     */
    private Integer status;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;


}
