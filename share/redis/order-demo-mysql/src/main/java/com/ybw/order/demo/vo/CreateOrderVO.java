package com.ybw.order.demo.vo;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @program: order-demo
 * @description:
 * @author: geoffrey
 * @create: 2022-02-19 21:37
 */
@Data
public class CreateOrderVO {
    /**
     * 订单号
     */
    @NotNull
    private String orderNo;
    /**
     * goods的id
     */
    @NotNull
    private Long goodsId;
    /**
     * 实付价格
     */
    @NotNull
    private BigDecimal payPrice;
    /**
     * 购买数量
     *
     * @author: geoffrey
     * @date: 2022/2/19
     **/
    @NotNull
    private Integer buyCount;
}
