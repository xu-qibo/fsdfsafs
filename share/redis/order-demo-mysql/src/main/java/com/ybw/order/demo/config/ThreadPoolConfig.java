package com.ybw.order.demo.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.lang.reflect.Method;
import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * 线程池配置
 *
 * @author ybwei
 * @date 2022/1/20 11:12
 **/
@Configuration
@EnableAsync
@Slf4j
public class ThreadPoolConfig implements AsyncConfigurer {

    /**
     * cpu 核心数量
     *
     * @author: ybwei
     * @date: 2022/1/20 11:13
     */
    private final int cpuNum = Runtime.getRuntime().availableProcessors();

    /**
     * 线程池配置
     *
     * @return java.util.concurrent.Executor
     * @throws
     * @methodName: getAsyncExecutor
     * @author ybwei
     * @date 2022/1/20 11:13
     */
    @Bean("taskExecutor")
    @Override
    public Executor getAsyncExecutor() {
        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        taskExecutor.setCorePoolSize(cpuNum);
        taskExecutor.setMaxPoolSize(cpuNum * 2);
        taskExecutor.setQueueCapacity(1024);
        taskExecutor.setAwaitTerminationSeconds(60);
        taskExecutor.setKeepAliveSeconds(60);
        taskExecutor.setWaitForTasksToCompleteOnShutdown(true);
        taskExecutor.setThreadNamePrefix("thread-pool-");
        taskExecutor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        // 线程池初始化
        taskExecutor.initialize();
        log.info("线程池初始化......, CPU 核心数量:{}", cpuNum);
        return taskExecutor;
    }

    /**
     * 重写捕获异常类
     *
     * @return org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler
     * @throws
     * @methodName: getAsyncUncaughtExceptionHandler
     * @author ybwei
     * @date 2022/1/20 11:15
     */
    @Override
    public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
        return new MyAsyncExceptionHandler();
    }

    /**
     * 自定义异常处理类
     *
     * @author ybwei
     * @date 2022/1/20 11:15
     **/
    class MyAsyncExceptionHandler implements AsyncUncaughtExceptionHandler {
        //手动处理捕获的异常
        @Override
        public void handleUncaughtException(Throwable throwable, Method method, Object... obj) {
            log.error("ExceptionMessage:{}", throwable.getMessage());
            log.error("MethodName:{}", method.getName());
            for (Object param : obj) {
                log.error("Parameter:{}", param);
            }
        }
    }
}