package com.ybw.order.demo.controller;

import com.ybw.order.demo.exception.BizException;
import com.ybw.order.demo.service.OrderInfoService;
import com.ybw.order.demo.service.RedisService;
import com.ybw.order.demo.utils.MyStringUtils;
import com.ybw.order.demo.vo.CancelOrderVO;
import com.ybw.order.demo.vo.CreateOrderVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * @program: order-demo
 * @description:
 * @author: geoffrey
 * @create: 2022-02-19 21:27
 */
@Slf4j
@RestController
@RequestMapping("/order")
public class PayController {

    @Resource
    private OrderInfoService orderService;
    @Resource
    private RedisService redisService;

    /**
     * 生成订单号
     *
     * @methodName: generateOrderNo
     * @return: java.lang.String
     * @author: geoffrey
     * @date: 2022/2/19
     **/
    @PostMapping("/generateOrderNo")
    public String generateOrderNo() throws Exception {
        return MyStringUtils.generateUUIDNoCenterLine();
    }

    /**
     * 下单
     *
     * @param createOrderVO
     * @methodName: createOrder
     * @return: java.lang.Integer
     * @author: geoffrey
     * @date: 2022/2/19
     **/
    @PostMapping("/createOrder")
    public String createOrder(@Valid @RequestBody CreateOrderVO createOrderVO) throws Exception {
        //1、获取分布式锁，等待时间为10秒
        boolean lockFlag = redisService.tryLock(createOrderVO.getGoodsId().toString(), 10);
        if (lockFlag) {
            try {
                orderService.createOrder(createOrderVO);
            } catch (BizException e) {
                log.error("createOrder error:", e);
                return e.getMessage();
            } finally {
                redisService.unlock(createOrderVO.getGoodsId().toString());
            }
            return "success";
        }
        log.info("fail");
        return "fail";
    }

    /**
     * 取消订单
     *
     * @param cancelOrderVO
     * @methodName: cancelOrder
     * @return: java.lang.String
     * @author: geoffrey
     * @date: 2022/2/20
     **/
    @PostMapping("/cancelOrder")
    public String cancelOrder(@Valid @RequestBody CancelOrderVO cancelOrderVO) throws Exception {
        orderService.cancelOrder(cancelOrderVO);
        return "success";
    }

    /**
     * 支付通知-成功（假的，真实的微信或支付宝以官方文档为准）
     *
     * @param orderNo
     * @methodName: notifyOrder
     * @return: java.lang.String
     * @author: geoffrey
     * @date: 2022/2/20
     **/
    @PostMapping("/notifyOrder")
    public String notifyOrder(String orderNo) {
        orderService.notifyOrder(orderNo);
        return "success";
    }
}