package com.ybw.order.demo.exception;

import lombok.Data;

/**
 * @author geoffrey
 * @version V1.0
 * @className BizException
 * @date 2022/2/20
 **/
@Data
public class BizException extends RuntimeException {

    /**
     * the serialVersionUID
     */
    private static final long serialVersionUID = 1381325479896057076L;

    /**
     * message key
     */
    private String code;

    private String data;

    public BizException(String message) {
        super(message);
    }

    public BizException(String code, String message) {
        super(message);
        this.code = code;
    }

    public BizException(String code, String data, String message) {
        super(message);
        this.data = data;
        this.code = code;
    }

}
