/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50725
 Source Host           : localhost:3306
 Source Schema         : order

 Target Server Type    : MySQL
 Target Server Version : 50725
 File Encoding         : 65001

 Date: 20/02/2022 22:13:28
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for goods_info
-- ----------------------------
DROP TABLE IF EXISTS `goods_info`;
CREATE TABLE `goods_info`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `total_stock_count` int(11) NOT NULL COMMENT '库存总数量',
  `stock_count` int(11) NOT NULL DEFAULT 0 COMMENT '当前库存数量',
  `is_allow_sale` tinyint(4) NOT NULL DEFAULT 0 COMMENT '上架状态 0：下架；1：上架；',
  `sale_price` decimal(10, 2) NOT NULL COMMENT '销售价格',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '商品' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of goods_info
-- ----------------------------
INSERT INTO `goods_info` VALUES (1, 100, 100, 1, 10.00, '2022-02-19 23:33:54', '2022-02-19 23:33:54');

-- ----------------------------
-- Table structure for order_info
-- ----------------------------
DROP TABLE IF EXISTS `order_info`;
CREATE TABLE `order_info`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_no` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '订单号',
  `goods_id` bigint(20) UNSIGNED NOT NULL COMMENT 'goods的id',
  `buy_count` int(11) NOT NULL COMMENT '购买数量',
  `sale_price` decimal(10, 2) NOT NULL COMMENT '销售价格',
  `pay_price` decimal(10, 2) NOT NULL COMMENT '实付价格',
  `status` tinyint(4) NOT NULL COMMENT '状态 0：待付；1：支付成功；2：已取消；',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `uk_order_no`(`order_no`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 69 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '订单' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_info
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
