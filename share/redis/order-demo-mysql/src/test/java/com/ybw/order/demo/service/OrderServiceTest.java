package com.ybw.order.demo.service;

import com.ybw.order.demo.entity.OrderInfo;
import com.ybw.order.demo.utils.MyStringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

import java.math.BigDecimal;

/**
 * @author geoffrey
 * @version V1.0
 * @className OrderServiceTest
 * @date 2022/2/20
 **/
@SpringBootTest
class OrderServiceTest {

    @Resource
    private OrderInfoService orderService;

    @Test
    public void save() {
        OrderInfo order = new OrderInfo();
        order.setOrderNo(MyStringUtils.generateUUIDNoCenterLine());
        order.setGoodsId(1L);
        order.setBuyCount(10);
        order.setSalePrice(BigDecimal.TEN);
        order.setPayPrice(BigDecimal.TEN);
        order.setStatus(0);
        orderService.save(order);
    }
}