package com.ybw.order.demo.utils;

import com.google.gson.Gson;
import com.ybw.order.demo.vo.CancelOrderVO;
import com.ybw.order.demo.vo.CreateOrderVO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author geoffrey
 * @version V1.0
 * @className HttpUtilsTest
 * @date 2022/2/20
 **/
@Slf4j
class HttpUtilsTest {

    /**
     * 创建订单-秒杀
     *
     * @methodName: createOrder
     * @return: void
     * @author: geoffrey
     * @date: 2022/2/20
     **/
    @Test
    public void createOrder() throws InterruptedException {
        final String url = "http://localhost:8080/order/createOrder";
        for (int i = 0; i < 25; i++) {
            new Thread(() -> {
                CreateOrderVO createOrderVO = new CreateOrderVO();
                createOrderVO.setOrderNo(MyStringUtils.generateUUIDNoCenterLine());
                createOrderVO.setGoodsId(1L);
                createOrderVO.setPayPrice(BigDecimal.TEN);
                createOrderVO.setBuyCount(5);
                HttpUtils.doPostJson(url, GsonUtils.toJsonString(createOrderVO));
            }).start();
        }
        TimeUnit.DAYS.sleep(1);
    }

    /**
     * 取消订单
     *
     * @methodName: cancelOrder
     * @return: void
     * @author: geoffrey
     * @date: 2022/2/20
     **/
    @Test
    public void cancelOrder() throws InterruptedException {
        final String url = "http://localhost:8080/order/cancelOrder";
        CancelOrderVO cancelOrderVO = new CancelOrderVO("6b7386a51aa244a5a28153e66a804f9b");
        HttpUtils.doPostJson(url, GsonUtils.toJsonString(cancelOrderVO));
    }

    /**
     * @className HttpUtilsTest
     * @author geoffrey
     * @date 2022/2/20
     * @version V1.0
     **/
    @Test
    public void notifyOrder() throws InterruptedException {
        final String url = "http://localhost:8080/order/notifyOrder?orderNo=08c9c73acff54753b43238323c569ab4";
        HttpUtils.doPostJson(url, null);
    }

}