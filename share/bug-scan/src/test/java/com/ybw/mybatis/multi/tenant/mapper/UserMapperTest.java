package com.ybw.mybatis.multi.tenant.mapper;

import com.alibaba.fastjson2.JSON;
import com.ybw.mybatis.multi.tenant.config.TenantContext;
import com.ybw.mybatis.multi.tenant.dto.TenantDTO;
import com.ybw.mybatis.multi.tenant.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author ybw
 * @version V1.0
 * @className UserMapperTest
 * @date 2022/10/10
 **/
@SpringBootTest
@Slf4j
class UserMapperTest {

    @Resource
    private UserMapper userMapper;

    @Test
    void getUserAndAddr() {
        TenantContext.set(new TenantDTO(3, 1L));
        List<User> userList = userMapper.getUserAndAddr(null);
        userList.forEach(u -> log.info("u:{}", JSON.toJSONString(u)));
        TenantContext.remove();
    }

    @Test
    void getAddrAndUser() {
    }
}