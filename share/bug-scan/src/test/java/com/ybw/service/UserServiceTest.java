package com.ybw.service;

import com.alibaba.fastjson2.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ybw.config.TenantContext;
import com.ybw.dto.TenantDTO;
import com.ybw.entity.User;
import com.ybw.mapper.UserMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author ybwei
 * @description
 * @date 2022/1/10 11:47
 **/
@SpringBootTest
@Slf4j
class UserServiceTest {

    @Resource
    private UserMapper mapper;

    @Test
    public void aInsert() {
        User user = new User();
        user.setName("一一");
        Assertions.assertTrue(mapper.insert(user) > 0);
        user = mapper.selectById(user.getId());
        Assertions.assertTrue(1 == user.getTenantId());
    }


    @Test
    public void bDelete() {
        Assertions.assertTrue(mapper.deleteById(3L) > 0);
    }


//    @Test
//    public void cUpdate() {
//        Assertions.assertTrue(mapper.updateById(new User().setId(1L).setName("mp")) > 0);
//    }

    @Test
    public void dSelect() {
        TenantContext.set(new TenantDTO(3, 1L));
        List<User> userList = mapper.selectList(null);
        userList.forEach(u -> log.info("u:{}", JSON.toJSONString(u)));
        TenantContext.remove();
    }


    /**
     * @methodName: page
     * @return: void
     * @author: ybw
     * @date: 2022/9/30
     **/
    @Test
    public void page() {
        IPage<User> page = new Page<User>(1, 2);
        mapper.selectPage(page, new LambdaQueryWrapper(User.class));
        log.info("userList:{}", JSON.toJSONString(page.getRecords()));
    }

    /**
     * 自定义SQL：默认也会增加多租户条件
     * 参考打印的SQL
     */
    @Test
    public void manualSqlTenantFilterTest() {
        System.out.println(mapper.myCount());
    }

    @Test
    public void testTenantFilter() {
//        mapper.getAddrAndUser(null).forEach(System.out::println);
//        mapper.getAddrAndUser("add").forEach(System.out::println);
//        mapper.getUserAndAddr(null).forEach(System.out::println);
        mapper.getUserAndAddr("J").forEach(System.out::println);
    }

}