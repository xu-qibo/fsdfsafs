/*
 Navicat Premium Data Transfer

 Source Server         : 本地
 Source Server Type    : MySQL
 Source Server Version : 80030
 Source Host           : localhost:3306
 Source Schema         : tenant

 Target Server Type    : MySQL
 Target Server Version : 80030
 File Encoding         : 65001

 Date: 09/10/2022 16:48:33
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_staff
-- ----------------------------
DROP TABLE IF EXISTS `sys_staff`;
CREATE TABLE `sys_staff`  (
  `id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `level` int(0) NOT NULL COMMENT '级别 1：集团；2：公司；3：员工（租户）；',
  `pid` bigint(0) NULL DEFAULT NULL COMMENT '父id，sys_staff的id',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_pid`(`pid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '员工' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_staff
-- ----------------------------
INSERT INTO `sys_staff` VALUES (1, 1, NULL, '2022-10-09 14:23:52', '2022-10-09 14:23:52');
INSERT INTO `sys_staff` VALUES (2, 2, 1, '2022-10-09 14:23:57', '2022-10-09 14:23:57');
INSERT INTO `sys_staff` VALUES (3, 3, 2, '2022-10-09 14:24:16', '2022-10-09 14:24:16');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '姓名',
  `company_group_id` bigint(0) NOT NULL COMMENT '租户-集团ID',
  `company_id` bigint(0) NOT NULL COMMENT '租户-公司id',
  `tenant_id` bigint(0) NOT NULL COMMENT '租户ID',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_tenant_id`(`tenant_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'Jone', 1, 1, 1);
INSERT INTO `user` VALUES (2, 'Jack', 1, 1, 2);
INSERT INTO `user` VALUES (3, 'Tom', 1, 2, 3);
INSERT INTO `user` VALUES (4, 'Sandy', 2, 3, 4);
INSERT INTO `user` VALUES (5, 'Billie', 2, 4, 5);

-- ----------------------------
-- Table structure for user_addr
-- ----------------------------
DROP TABLE IF EXISTS `user_addr`;
CREATE TABLE `user_addr`  (
  `id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `user_id` bigint(0) NOT NULL COMMENT 'user.id',
  `addr` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '地址名称',
  `company_group_id` bigint(0) NOT NULL COMMENT '租户-集团ID',
  `company_id` bigint(0) NOT NULL COMMENT '租户-公司id',
  `tenant_id` bigint(0) NOT NULL COMMENT '租户ID',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_id`(`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_addr
-- ----------------------------
INSERT INTO `user_addr` VALUES (1, 1, 'addr1', 1, 1, 1);
INSERT INTO `user_addr` VALUES (2, 1, 'addr2', 1, 2, 3);

SET FOREIGN_KEY_CHECKS = 1;
