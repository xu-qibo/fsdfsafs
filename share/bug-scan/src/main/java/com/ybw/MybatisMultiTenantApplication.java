package com.ybw;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MybatisMultiTenantApplication {

    public static void main(String[] args) {
        SpringApplication.run(MybatisMultiTenantApplication.class, args);
    }

}
