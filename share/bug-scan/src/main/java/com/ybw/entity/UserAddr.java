package com.ybw.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author ybw
 * @since 2022-10-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class UserAddr implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * user.id
     */
    private Long userId;

    /**
     * 地址名称
     */
    private String addr;

    /**
     * 租户-集团ID
     */
    private Long companyGroupId;

    /**
     * 租户-公司id
     */
    private Long companyId;

    /**
     * 租户ID
     */
    private Long tenantId;


}
