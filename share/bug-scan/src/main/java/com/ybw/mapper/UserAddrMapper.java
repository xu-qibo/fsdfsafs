package com.ybw.mapper;

import com.ybw.entity.UserAddr;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ybw
 * @since 2022-01-10
 */
public interface UserAddrMapper extends BaseMapper<UserAddr> {

}
