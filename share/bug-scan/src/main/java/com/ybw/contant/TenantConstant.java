package com.ybw.contant;

/**
 * @author ybw
 * @version V1.0
 * @className TenantConstant
 * @date 2022/10/9
 **/
public interface TenantConstant {

    /**
     * 字段
     *
     * @author ybw
     * @version V1.0
     * @className TenantConstant
     * @date 2022/10/9
     **/
    interface Field {
        /**
         * 集团id
         *
         * @author: ybw
         * @date: 2022/10/9
         **/
        String COMPANY_GROUP_ID = "company_group_id";
        /**
         * 公司id
         *
         * @author: ybw
         * @date: 2022/10/9
         **/
        String COMPANY_ID = "company_id";
        /**
         * 租户id
         *
         * @author: ybw
         * @date: 2022/10/9
         **/
        String TENANT_ID = "tenant_id";

    }
}
