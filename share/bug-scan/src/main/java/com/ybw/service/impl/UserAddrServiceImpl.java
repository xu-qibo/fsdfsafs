package com.ybw.service.impl;

import com.ybw.entity.UserAddr;
import com.ybw.mapper.UserAddrMapper;
import com.ybw.service.UserAddrService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ybw
 * @since 2022-01-10
 */
@Service
public class UserAddrServiceImpl extends ServiceImpl<UserAddrMapper, UserAddr> implements UserAddrService {

}
