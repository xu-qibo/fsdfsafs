package com.ybw.service;

import com.ybw.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ybw
 * @since 2022-01-10
 */
public interface UserService extends IService<User> {

    /**
     *
     * @methodName: getUserAndAddr
     * @param username
     * @return: java.util.List<com.ybw.entity.User>
     * @author: ybw
     * @date: 2023/6/13
     **/
    List<User> getUserAndAddr(String username);
}
