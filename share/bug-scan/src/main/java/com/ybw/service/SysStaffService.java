package com.ybw.service;

import com.ybw.entity.SysStaff;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 员工 服务类
 * </p>
 *
 * @author ybw
 * @since 2022-10-07
 */
public interface SysStaffService extends IService<SysStaff> {

}
