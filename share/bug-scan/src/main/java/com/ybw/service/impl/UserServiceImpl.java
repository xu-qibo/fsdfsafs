package com.ybw.service.impl;

import com.ybw.entity.User;
import com.ybw.mapper.UserMapper;
import com.ybw.service.UserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author ybw
 * @since 2022-01-10
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Resource
    private UserMapper userMapper;

    @Override
    public List<User> getUserAndAddr(String username) {
        return userMapper.getUserAndAddr(username);
    }
}
