package com.ybw.service;

import com.ybw.entity.UserAddr;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ybw
 * @since 2022-01-10
 */
public interface UserAddrService extends IService<UserAddr> {

}
