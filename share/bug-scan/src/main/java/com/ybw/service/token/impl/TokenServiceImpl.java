package com.ybw.service.token.impl;

import com.ybw.entity.SysStaff;
import com.ybw.service.token.TokenService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
public class TokenServiceImpl implements TokenService {

    private static Map<String, SysStaff> map = new HashMap<>();

    @Override
    public void generateToken(String token, SysStaff object) {
        map.put(token, object);
    }

    /**
     * @Description 延长有效期
     * @Author ybwei
     * @Date 2021/4/6 19:57
     * @Param [jwtToken]
     * @Return java.lang.Object
     * @Exception
     */
    @Override
    public SysStaff getObjectByToken(String token) {
        return map.get(token);
    }


    @Override
    public void removeToken(String token) {
        map.remove(token);
    }
}
