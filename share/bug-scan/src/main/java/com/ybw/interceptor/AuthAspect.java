package com.ybw.interceptor;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ybw.config.TenantContext;
import com.ybw.dto.TenantDTO;
import com.ybw.entity.SysStaff;
import com.ybw.service.token.TokenService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;

/**
 * 接口日志
 *
 * @author ybwei
 * @date 2022/2/17 11:40
 **/
@Aspect
@Component
@Slf4j
public class AuthAspect {

    @Resource
    private TokenService tokenService;
    private List<String> ignoreUrlList = Arrays.asList("/sysStaff/getToken");
    private final ObjectMapper mapper;

    @Autowired
    public AuthAspect(ObjectMapper mapper) {
        this.mapper = mapper;
    }

    @Pointcut("@annotation(org.springframework.web.bind.annotation.RequestMapping)")
    public void requestLog() {
    }

    @Pointcut("@annotation(org.springframework.web.bind.annotation.PostMapping)")
    public void postLog() {
    }

    @Pointcut("@annotation(org.springframework.web.bind.annotation.GetMapping)")
    public void getLog() {
    }

    /**
     * 请求参数
     *
     * @param joinPoint
     * @return void
     * @throws
     * @methodName: doBefore
     * @author ybwei
     * @date 2022/2/17 13:54
     */
    @Before("requestLog() || postLog() || getLog()")
    public void doBefore(JoinPoint joinPoint) {
        //1、获取访问路径
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        String servletPath = request.getServletPath();
        if (ignoreUrlList.contains(servletPath)) {
            //1.1 不需要鉴权
            return;
        }
        //2、获取token
        String token = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest().getHeader("token");
        if (StringUtils.isBlank(token)) {
            return;
        }
        //3、获取员工信息
        SysStaff sysStaff = tokenService.getObjectByToken(token);
        if (sysStaff == null) {
            return;
        }
        //4、处理多租户
        TenantContext.set(new TenantDTO(sysStaff.getLevel(), sysStaff.getId()));
    }


    /**
     * 返回参数
     *
     * @param response
     * @return void
     * @throws
     * @methodName: doAfterReturning
     * @author ybwei
     * @date 2022/2/17 13:54
     */
    @AfterReturning(returning = "response", pointcut = "requestLog() || postLog() || getLog()")
    public void doAfterReturning(Object response) {
        TenantContext.remove();
    }
}

