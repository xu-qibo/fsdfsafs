package com.ybw.test;

import com.google.common.hash.BloomFilter;
import com.google.common.hash.Funnels;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

/**
 * 布隆过滤器测试
 *
 * @author ybw
 * @version V1.0
 * @className BloomFilterTest
 * @date 2023/1/10
 **/
@Slf4j
public class BloomFilterTest {

    /**
     * 预计要插入多少数据
     */
    private int size = 1000000;

    /**
     * 期望的误判率
     */
    private double fpp = 0.01;

    /**
     * 布隆过滤器
     */
    private BloomFilter<Integer> bloomFilter = BloomFilter.create(Funnels.integerFunnel(), size, fpp);


    /**
     * 测试
     *
     * @methodName: filter
     * @return: void
     * @author: ybw
     * @date: 2023/1/10
     **/
    @Test
    void filter() {
        //1、插入10万样本数据
        for (int i = 0; i < size; i++) {
            bloomFilter.put(i);
        }

        //2、用另外十万测试数据，测试误判率
        //误判个数
        int count = 0;
        for (int i = size; i < size + 100000; i++) {
            if (bloomFilter.mightContain(i)) {
                count++;
                System.out.println(i + "误判了");
            }
        }
        log.info("总共的误判数:{}", count);
    }

}