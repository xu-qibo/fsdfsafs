package com.ybw.yaml.demo.generate;

import com.alibaba.fastjson2.JSON;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;

import java.io.*;
import java.util.Map;

/**
 * @program: yaml-demo
 * @description:
 * @author: geoffrey
 * @create: 2022-07-30 23:04
 */
@Slf4j
public class YamlGenerateTest {

    /**
     * @methodName: generate
     * @return: void
     * @author: ybwei
     * @date: 2022/7/30
     **/
    @Test
    public void generate() throws IOException {
        //1、数据库配置
        DataSourceProperties dataSourceProperties = new DataSourceProperties();
        dataSourceProperties.setUrl("jdbc:mysql://localhost:3306/prod?useUnicode=true&characterEncoding=utf-8&serverTimezone=UTC");
        dataSourceProperties.setUsername("prod");
        dataSourceProperties.setPassword("prod123");
        //2、生成文件
        generateApplication(dataSourceProperties);
    }

    /**
     * @param dataSourceProperties
     * @methodName: generateApplication
     * @return: void
     * @author: ybwei
     * @date: 2022/7/30
     **/
    private void generateApplication(DataSourceProperties dataSourceProperties) throws IOException {
        //1、读取置文件，以dev配置文件为模板
        String rootPath = System.getProperty("user.dir");
        String sourceFileName = "application-dev.yml";
        String sourceFilePath = rootPath + "\\src\\main\\resources\\" + sourceFileName;
        log.info(rootPath);
        log.info(sourceFilePath);

        Yaml yaml = new Yaml();
        File file = new File(sourceFilePath);
        Map<String, Object> map = yaml.load(new FileInputStream(file));
        log.info("map:{}", JSON.toJSONString(map));

        //3、替换参数
        //3.1 替换数据库
        Map<String, Object> spring = (Map<String, Object>) map.get("spring");
        Map<String, Object> datasource = (Map<String, Object>) spring.get("datasource");
        Map<String, Object> druid = (Map<String, Object>) datasource.get("druid");
        druid.put("url", dataSourceProperties.getUrl());
        druid.put("username", dataSourceProperties.getUsername());
        druid.put("password", dataSourceProperties.getPassword());


        //4、生成文件
        String targetFileName = "application-prod.yml";
        String targetFilePath = rootPath + "\\src\\main\\resources\\" + targetFileName;
        DumperOptions options = new DumperOptions();
        options.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
        Yaml yaml2 = new Yaml(options);
        FileWriter fw = new FileWriter(targetFilePath);
        yaml2.dump(map, fw);
    }
}
