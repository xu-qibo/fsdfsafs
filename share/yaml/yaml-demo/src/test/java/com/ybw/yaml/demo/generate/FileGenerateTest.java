package com.ybw.yaml.demo.generate;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Test;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * @program: yaml-demo
 * @description:
 * @author: geoffrey
 * @create: 2022-07-30 23:23
 */
@Slf4j
public class FileGenerateTest {

    /**
     * @methodName: generate
     * @return: void
     * @author: ybwei
     * @date: 2022/7/30
     **/
    @Test
    public void generate() throws IOException {
        //1、数据库配置
        DataSourceProperties dataSourceProperties = new DataSourceProperties();
        dataSourceProperties.setUrl("jdbc:mysql://localhost:3306/prod?useUnicode=true&characterEncoding=utf-8&serverTimezone=UTC");
        dataSourceProperties.setUsername("prod");
        dataSourceProperties.setPassword("prod123");
        //2、生成文件
        generateApplication(dataSourceProperties);
    }

    /**
     * @methodName: generateApplication
     * @param dataSourceProperties
     * @return: void
     * @author: ybwei
     * @date: 2022/7/30
     **/
    private void generateApplication(DataSourceProperties dataSourceProperties) throws IOException {
        //1、读取置文件，以dev配置文件为模板
        String rootPath = System.getProperty("user.dir");
        String sourceFileName = "application-dev.yml";
        String sourceFilePath = rootPath + "\\src\\main\\resources\\" + sourceFileName;
        log.info(rootPath);
        log.info(sourceFilePath);

        String content= FileUtils.readFileToString(new File(sourceFilePath), StandardCharsets.UTF_8);
        content=content.replace("jdbc:mysql://localhost:3306/test?useUnicode=true&characterEncoding=utf-8&serverTimezone=UTC",dataSourceProperties.getUrl());
        content=content.replace("username: root","username: "+dataSourceProperties.getUsername());
        content=content.replace("password: 123456","password: "+dataSourceProperties.getPassword());


        log.info("generateApplication content:{}", content);

        //4、生成文件
        String targetFileName = "application-prod.yml";
        String targetFilePath = rootPath + "\\src\\main\\resources\\" + targetFileName;
        FileUtils.writeStringToFile(new File(targetFilePath),content,StandardCharsets.UTF_8);
    }
}
