# 工程简介
#druid配置
https://github.com/alibaba/druid/tree/master/druid-spring-boot-starter
#表
##表结构
    CREATE TABLE `user` (
      `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
      `name` varchar(255) NOT NULL,
      `no` int(11) NOT NULL,
      `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
      `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
      PRIMARY KEY (`id`)
    ) ENGINE=InnoDB AUTO_ DEFAULT CHARSET=utf8mb4;
##生成海量数据SQL
    INSERT USER ( NAME, NO ) SELECT NAME
    ,
    NO 
    FROM
    	USER;

#接口测试
http://localhost:8080/user/getUserList
</br>
测试慢sql接口
</br>
http://localhost:8080/user/getSlowUserList
#监控地址
http://localhost:8080/druid
# 延伸阅读

