package com.ybw.mybatis.plus.generator.demo.service;

import com.ybw.mybatis.plus.generator.demo.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ybw
 * @since 2021-11-15
 */
public interface UserService extends IService<User> {

}
