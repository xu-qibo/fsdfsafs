package com.ybw.mybatis.plus.generator.demo.service.impl;

import com.ybw.mybatis.plus.generator.demo.entity.MqConsumer;
import com.ybw.mybatis.plus.generator.demo.mapper.MqConsumerMapper;
import com.ybw.mybatis.plus.generator.demo.service.MqConsumerService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ybw
 * @since 2021-10-08
 */
@Service
public class MqConsumerServiceImpl extends ServiceImpl<MqConsumerMapper, MqConsumer> implements MqConsumerService {

}
