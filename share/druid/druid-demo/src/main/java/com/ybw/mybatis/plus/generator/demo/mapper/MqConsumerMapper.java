package com.ybw.mybatis.plus.generator.demo.mapper;

import com.ybw.mybatis.plus.generator.demo.entity.MqConsumer;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ybw
 * @since 2021-10-08
 */
public interface MqConsumerMapper extends BaseMapper<MqConsumer> {

}
