package com.ybw.mybatis.plus.generator.demo.service.impl;

import com.ybw.mybatis.plus.generator.demo.entity.MqProducer;
import com.ybw.mybatis.plus.generator.demo.mapper.MqProducerMapper;
import com.ybw.mybatis.plus.generator.demo.service.MqProducerService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ybw
 * @since 2021-10-08
 */
@Service
public class MqProducerServiceImpl extends ServiceImpl<MqProducerMapper, MqProducer> implements MqProducerService {

}
