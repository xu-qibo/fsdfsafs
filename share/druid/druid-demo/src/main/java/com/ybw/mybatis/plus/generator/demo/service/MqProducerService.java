package com.ybw.mybatis.plus.generator.demo.service;

import com.ybw.mybatis.plus.generator.demo.entity.MqProducer;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ybw
 * @since 2021-10-08
 */
public interface MqProducerService extends IService<MqProducer> {

}
