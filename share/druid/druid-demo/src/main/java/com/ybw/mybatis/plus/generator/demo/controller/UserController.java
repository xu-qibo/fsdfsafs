package com.ybw.mybatis.plus.generator.demo.controller;


import com.ybw.mybatis.plus.generator.demo.entity.User;
import com.ybw.mybatis.plus.generator.demo.service.UserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author ybw
 * @since 2021-11-15
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Resource
    private UserService userService;

    /**
     * 慢sql测试
     *
     * @return java.util.List<com.ybw.mybatis.plus.generator.demo.entity.User>
     * @throws
     * @methodName: getUserList
     * @author ybwei
     * @date 2022/3/9 16:26
     */
    @GetMapping("/getSlowUserList")
    public List<User> getSlowUserList() {
        return userService.lambdaQuery()
                .list();
    }

    /**
     * @methodName: getUserList
     * @return java.util.List<com.ybw.mybatis.plus.generator.demo.entity.User>
     * @throws 
     * @author ybwei
     * @date 2022/3/9 16:36
     */
    @GetMapping("/getUserList")
    public List<User> getUserList() {
        return userService.lambdaQuery()
                .eq(User::getName, "张三980")
                .list();
    }
}

