package com.ybw.mybatis.plus.generator.demo.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author ybw
 * @since 2021-10-08
 */
@Controller
@RequestMapping("/mqConsumer")
public class MqConsumerController {

}

