package com.ybw.mybatis.plus.generator.demo.service.impl;

import com.ybw.mybatis.plus.generator.demo.entity.User;
import com.ybw.mybatis.plus.generator.demo.mapper.UserMapper;
import com.ybw.mybatis.plus.generator.demo.service.UserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ybw
 * @since 2021-11-15
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

}
