package com.ybw.mybatis.plus.generator.demo.service;

import com.ybw.mybatis.plus.generator.demo.entity.MqProducer;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class MqProducerServiceTest {

    @Resource
    private MqProducerService mqProducerService;

    @Test
    void save(){
        MqProducer mqProducer=new MqProducer();
        mqProducer.setMsgid("msgid");
        mqProducer.setPropertyKeys("keys");
        mqProducer.setMessage("msg");
        mqProducer.setCreateTime(LocalDateTime.now());
        mqProducer.setUpdateTime(LocalDateTime.now());
        mqProducerService.save(mqProducer);
    }

}