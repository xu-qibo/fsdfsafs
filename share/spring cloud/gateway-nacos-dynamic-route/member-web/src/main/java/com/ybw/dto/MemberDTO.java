package com.ybw.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author ybw
 * @version V1.0
 * @className UserDTO
 * @date 2023/3/22
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MemberDTO {
    private Long id;
    private String name;
}
