package com.ybw.controller;//package com.alibaba.member.controller;

import com.ybw.dto.MemberDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: my-spring-cloud-parent
 * @description:
 * @author: ybwei
 * @create: 2021-02-09 00:20
 */
@RestController
@RequestMapping("/member")
@Slf4j
public class MemberController {

    @GetMapping("/get")
    public MemberDTO get() {
        log.info("client请求开始");
        return new MemberDTO(1L, "张三");
    }
}
