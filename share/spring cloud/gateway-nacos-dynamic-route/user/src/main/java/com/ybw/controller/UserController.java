package com.ybw.controller;

import com.ybw.dto.UserDTO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author ybw
 * @version V1.0
 * @className UserController
 * @date 2023/3/22
 **/
@RestController
public class UserController {

    /**
     *
     * @methodName: getUser
     * @return: com.ybw.dto.UserDTO
     * @author: ybw
     * @date: 2023/3/22
     **/
    @GetMapping("/getUser")
    public UserDTO getUser(){
        return new UserDTO(1L,"王五",15);
    }
}
