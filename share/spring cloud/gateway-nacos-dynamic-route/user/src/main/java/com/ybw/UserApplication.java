package com.ybw;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @ClassName AlibabaApplication
 * @Description: 1、想要远程调用别的服务
 * 1）引入open-feign
 * 2）编写一个接口，告诉spring cloud这个接口需要调用远程服务
 * 声明接口的每个方法都是调用哪个远程服务的哪个请求
 * 3）开启远程调用功能(@EnableFeignClients，basePackages扫描包)
 * @Author: ybwei
 * @Date: 2021/2/5
 * @Version V1.0
 **/
@SpringBootApplication
public class UserApplication {


    public static void main(String[] args) {
        SpringApplication.run(UserApplication.class, args);
    }

}
