package com.ybw.export.doc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExportDocDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExportDocDemoApplication.class, args);
    }

}
