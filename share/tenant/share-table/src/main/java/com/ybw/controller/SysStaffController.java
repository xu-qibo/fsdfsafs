package com.ybw.controller;


import com.ybw.entity.SysStaff;
import com.ybw.service.SysStaffService;
import com.ybw.service.token.TokenService;
import com.ybw.utils.MyStringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 员工
 *
 * @author ybw
 * @since 2022-10-07
 */
@RestController
@RequestMapping("/sysStaff")
public class SysStaffController {

    @Resource
    private SysStaffService sysStaffService;
    @Resource
    private TokenService tokenService;

    /**
     * 获取token
     *
     * @param id
     * @methodName: getToken
     * @return: java.lang.String
     * @author: ybw
     * @date: 2022/10/9
     **/
    @GetMapping("/getToken")
    public String getToken(Long id) {
        SysStaff sysStaff = sysStaffService.getById(id);
        if (sysStaff == null) {
            return null;
        }
        String token = MyStringUtils.generateUUIDNoCenterLine();
        tokenService.generateToken(token, sysStaff);
        return token;
    }
}

