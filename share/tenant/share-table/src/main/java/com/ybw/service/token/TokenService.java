package com.ybw.service.token;


import com.ybw.entity.SysStaff;

public interface TokenService {

    /**
     * 生成token
     *
     * @param token
     * @param object
     * @methodName: generateToken
     * @return: void
     * @author: ybw
     * @date: 2022/10/9
     **/
    void generateToken(String token, SysStaff object);

    /**
     *
     * @methodName: getObjectByToken
     * @param token
     * @return: java.lang.Object
     * @author: ybw
     * @date: 2022/10/9
     **/
    SysStaff getObjectByToken(String token);


    /**
     * @param token
     * @methodName: removeToken
     * @return: void
     * @author: ybw
     * @date: 2022/10/9
     **/
    void removeToken(String token);
}
