package com.ybw.service.impl;

import com.ybw.entity.SysStaff;
import com.ybw.mapper.SysStaffMapper;
import com.ybw.service.SysStaffService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 员工 服务实现类
 * </p>
 *
 * @author ybw
 * @since 2022-10-07
 */
@Service
public class SysStaffServiceImpl extends ServiceImpl<SysStaffMapper, SysStaff> implements SysStaffService {

}
