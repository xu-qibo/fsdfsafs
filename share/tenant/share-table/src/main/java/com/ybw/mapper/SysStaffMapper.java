package com.ybw.mapper;

import com.ybw.entity.SysStaff;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 员工 Mapper 接口
 * </p>
 *
 * @author ybw
 * @since 2022-10-07
 */
public interface SysStaffMapper extends BaseMapper<SysStaff> {

}
