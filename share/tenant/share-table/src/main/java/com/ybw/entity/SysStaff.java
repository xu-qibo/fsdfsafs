package com.ybw.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 员工
 * </p>
 *
 * @author ybw
 * @since 2022-10-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class SysStaff implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 级别 1：集团；2：公司；3：员工（租户）；
     */
    private Integer level;

    /**
     * 父id，sys_staff的id
     */
    private Long pid;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    private LocalDateTime updateTime;


}
