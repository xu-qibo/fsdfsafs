package com.ybw.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * @author ybw
 * @version V1.0
 * @className TenantIgnoreConfig
 * @date 2023/6/14
 **/
@Configuration
@ConfigurationProperties("tenant.ignore")
@Data
public class TenantIgnoreConfig {
    /**
     * 多租户-公共表（忽略表）
     *
     * @author: ybw
     * @date: 2023/6/14
     **/
    private List<String> tableList;
}
