package com.ybw.config;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.handler.TenantLineHandler;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.TenantLineInnerInterceptor;
import com.ybw.contant.LevelConstant;
import com.ybw.contant.TenantConstant;
import com.ybw.dto.TenantDTO;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.LongValue;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;

/**
 * 多租户配置
 *
 * @author ybw
 * @version V1.0
 * @className MybatisPlusConfig
 * @date 2022/10/10
 **/
@Configuration
@MapperScan("com.ybw.mapper")
public class MybatisPlusConfig {

    @Resource
    private TenantIgnoreConfig tenantIgnoreConfig;

    /**
     * 新多租户插件配置,一缓和二缓遵循mybatis的规则,需要设置 MybatisConfiguration#useDeprecatedExecutor = false 避免缓存万一出现问题
     */
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        //1、租户插件
        interceptor.addInnerInterceptor(new TenantLineInnerInterceptor(new TenantLineHandler() {
            /**
             * 获取租户 ID 值表达式，只支持单个 ID 值
             * <p>
             *
             * @return 租户 ID 值表达式
             */
            @Override
            public Expression getTenantId() {
                TenantDTO tenantDTO = TenantContext.get();
                if (tenantDTO == null) {
                    //默认为0
                    return new LongValue(0);
                }
                return new LongValue(tenantDTO.getTenantId());
            }

            /**
             * 获取租户字段名
             * <p>
             * 默认字段名叫: tenant_id
             *
             * @return 租户字段名
             */
            @Override
            public String getTenantIdColumn() {
                TenantDTO tenantDTO = TenantContext.get();
                if (LevelConstant.COMPANY_GROUP.equals(tenantDTO.getLevel())) {
                    return TenantConstant.Field.COMPANY_GROUP_ID;
                }
                if (LevelConstant.COMPANY.equals(tenantDTO.getLevel())) {
                    return TenantConstant.Field.COMPANY_ID;
                }
                return TenantConstant.Field.TENANT_ID;
            }


            /**
             * 根据表名判断是否忽略拼接多租户条件
             * <p>
             * 默认都要进行解析并拼接多租户条件
             *
             * 这是 default 方法,默认返回 false 表示所有表都需要拼多租户条件
             * @param tableName 表名
             * @return 是否忽略, true:表示忽略，false:需要解析并拼接多租户条件
             */
            @Override
            public boolean ignoreTable(String tableName) {
                if (tenantIgnoreConfig.getTableList().contains(tableName)) {
                    return true;
                }
                return false;
            }
        }));
        //2、分页插件
        // 如果用了分页插件注意先 add TenantLineInnerInterceptor 再 add PaginationInnerInterceptor
        // 用了分页插件必须设置 MybatisConfiguration#useDeprecatedExecutor = false
        interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.MYSQL));
        return interceptor;
    }
}
