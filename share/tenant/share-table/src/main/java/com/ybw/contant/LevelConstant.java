package com.ybw.contant;

/**
 * @author ybw
 * @version V1.0
 * @className LevelConstant
 * @date 2022/9/30
 **/
public interface LevelConstant {

    /**
     * 集团
     *
     * @author: ybw
     * @date: 2022/9/30
     **/
    Integer COMPANY_GROUP = 1;

    /**
     * 公司
     *
     * @author: ybw
     * @date: 2022/9/30
     **/
    Integer COMPANY = 2;

    /**
     * 租户
     *
     * @author: ybw
     * @date: 2022/9/30
     **/
    Integer TENANT = 3;
}
