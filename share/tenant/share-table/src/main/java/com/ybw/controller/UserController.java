package com.ybw.controller;


import com.ybw.entity.User;
import com.ybw.service.UserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * 用户
 *
 * @author ybw
 * @since 2022-01-10
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Resource
    private UserService userService;

    @GetMapping("/getUser")
    public List<User> getUser() {
        return userService.lambdaQuery().list();
    }

    /**
     * 测试多租户
     * 1、left join: SELECT u.id, u.name, a.addr AS addr_name FROM user u LEFT JOIN user_addr a ON a.user_id = u.id AND a.company_group_id = 1 WHERE u.name LIKE concat(concat('%', ?), '%') AND u.company_group_id = 1
     * 2、inner join: SELECT u.id, u.name, a.addr AS addr_name FROM user u, user_addr a WHERE a.user_id = u.id AND u.name LIKE concat(concat('%', ?), '%') AND u.company_group_id = 1 AND a.company_group_id = 1
     *
     * @param username
     * @methodName: getUserAndAddr
     * @return: java.util.List<com.ybw.entity.User>
     * @author: ybw
     * @date: 2023/6/13
     **/
    @GetMapping("/getUserAndAddr")
    public List<User> getUserAndAddr(@RequestParam String username) {
        return userService.getUserAndAddr(username);
    }
}

