package com.ybw.rsa.demo.service;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

/**
 * @author ybwei
 * @version V1.0
 * @className RsaServiceTest
 * @date 2022/8/12
 **/
@SpringBootTest
@Slf4j
class RsaServiceTest {

    @Resource
    private RsaService rsaService;
    /**
     * @methodName: rsaTest
     * @return: void
     * @author: ybw
     * @date: 2022/8/12
     **/
    @Test
    public void rsaTest() throws Exception {
        log.info("公钥:{}", rsaService.getPublicKey());
        byte[] usernames = rsaService.encrypt("username66");
        log.info(rsaService.decrypt(usernames));
    }
}