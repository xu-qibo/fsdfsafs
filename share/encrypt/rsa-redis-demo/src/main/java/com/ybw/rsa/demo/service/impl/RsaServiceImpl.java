package com.ybw.rsa.demo.service.impl;

import com.ybw.rsa.demo.constant.RedisPreConstant;
import com.ybw.rsa.demo.constant.RsaConstant;
import com.ybw.rsa.demo.service.RsaService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.crypto.Cipher;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.*;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.concurrent.TimeUnit;

/**
 * rsa加密
 *
 * @author ybw
 * @version V1.0
 * @className RsaServiceImpl
 * @date 2022/8/12
 **/
@Service
@Slf4j
public class RsaServiceImpl implements RsaService {
    @Resource
    private RedisTemplate redisTemplate;

    /**
     * 初始化
     *
     * @methodName: init
     * @return: void
     * @author: ybw
     * @date: 2022/8/12
     **/
    @PostConstruct
    public void init() throws Exception {
        log.info("RsaServiceImpl init start");
        Provider provider = new org.bouncycastle.jce.provider.BouncyCastleProvider();
        Security.addProvider(provider);
        SecureRandom random = new SecureRandom();
        KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA", provider);
        generator.initialize(RsaConstant.KEY_SIZE, random);
        KeyPair keyPair = generator.generateKeyPair();
        //将公钥和私钥存放，登录时会不断请求获取公钥，我们可以将其放到缓存中，而不放入数据库了
        //我在想，这个是不是有必要存放到Redis，在分布式场景中？
        //貌似有些必要，万一获取到的pubkey是server1中的，拿着server1的pubkey去server2去解密？
        storeRsa(keyPair);
        log.info("RsaServiceImpl init end");
    }

    /**
     * 将RSA存入缓存
     *
     * @param keyPair
     * @methodName: storeRsa
     * @return: void
     * @author: ybwei
     * @date: 2022/8/13
     **/
    private void storeRsa(KeyPair keyPair) {
        //1、存储公钥key
        String publicRedisKey = getRedisKey(RsaConstant.PUBLIC_KEY);
        PublicKey publicKey = keyPair.getPublic();
        //公钥字符串
        String publicKeyStr = new String(Base64.encodeBase64(publicKey.getEncoded()));
        redisTemplate.opsForValue().set(publicRedisKey, publicKeyStr, 1, TimeUnit.DAYS);

        //2、存储私钥key
        String privateRedisKey = getRedisKey(RsaConstant.PRIVATE_KEY);
        PrivateKey privateKey = keyPair.getPrivate();
        //私钥字符串
        String privateKeyStr = new String(Base64.encodeBase64(privateKey.getEncoded()));
        redisTemplate.opsForValue().set(privateRedisKey, privateKeyStr, 1, TimeUnit.DAYS);
    }

    /**
     * @className RsaServiceImpl
     * @author ybw
     * @date 2022/8/12
     * @version V1.0
     **/
    private String getRedisKey(String publicKey) {
        return new StringBuilder()
                .append(RedisPreConstant.RSA)
                .append(publicKey)
                .toString();
    }

    /**
     * 从字符串中加载公钥
     *
     * @methodName: loadPublicKeyByStr
     * @return: java.security.interfaces.RSAPublicKey
     * @author: ybwei
     * @date: 2022/8/13
     **/
    public RSAPublicKey loadPublicKeyByStr() throws Exception {
        try {
            //公钥数据字符串
            String publicKeyStr = getPublicKey();
            byte[] buffer = Base64.decodeBase64(publicKeyStr);
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(buffer);
            return (RSAPublicKey) keyFactory.generatePublic(keySpec);
        } catch (NoSuchAlgorithmException e) {
            throw new Exception("无此算法");
        } catch (InvalidKeySpecException e) {
            throw new Exception("公钥非法");
        } catch (NullPointerException e) {
            throw new Exception("公钥数据为空");
        }
    }

    /**
     * 从字符串中加载私钥
     *
     * @methodName: loadPrivateKeyByStr
     * @return: java.security.interfaces.RSAPrivateKey
     * @author: ybwei
     * @date: 2022/8/13
     **/
    public RSAPrivateKey loadPrivateKeyByStr() throws Exception {
        try {
            //私钥数据字符串
            String privateKeyStr = getPrivateKey();
            byte[] buffer = Base64.decodeBase64(privateKeyStr);
            PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(buffer);
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            return (RSAPrivateKey) keyFactory.generatePrivate(keySpec);
        } catch (NoSuchAlgorithmException e) {
            throw new Exception("无此算法");
        } catch (InvalidKeySpecException e) {
            throw new Exception("私钥非法");
        } catch (NullPointerException e) {
            throw new Exception("私钥数据为空");
        }
    }


    /**
     * 私钥解密(解密前台公钥加密的密文)
     *
     * @param encryptText 公钥加密的数据
     * @return 私钥解密出来的数据
     * @throws Exception e
     */
    @Override
    public String decryptWithPrivate(String encryptText) throws Exception {
        if (StringUtils.isBlank(encryptText)) {
            return null;
        }
        byte[] en_byte = Base64.decodeBase64(encryptText.getBytes());
        Provider provider = new BouncyCastleProvider();
        Security.addProvider(provider);
        Cipher ci = Cipher.getInstance("RSA/ECB/PKCS1Padding", provider);
        PrivateKey privateKey = loadPrivateKeyByStr();
        ci.init(Cipher.DECRYPT_MODE, privateKey);
        byte[] res = ci.doFinal(en_byte);
        return new String(res);
    }

    /**
     * 公钥加密
     *
     * @param plaintext 明文内容
     * @return byte[]
     * @throws UnsupportedEncodingException e
     */
    @Override
    public byte[] encrypt(String plaintext) throws Exception {
        String encode = URLEncoder.encode(plaintext, "utf-8");
        RSAPublicKey rsaPublicKey = loadPublicKeyByStr();
        //获取公钥指数
        BigInteger e = rsaPublicKey.getPublicExponent();
        //获取公钥系数
        BigInteger n = rsaPublicKey.getModulus();
        //获取明文字节数组
        BigInteger m = new BigInteger(encode.getBytes());
        //进行明文加密
        BigInteger res = m.modPow(e, n);
        return res.toByteArray();

    }

    /**
     * 私钥解密
     *
     * @param cipherText 加密后的字节数组
     * @return 解密后的数据
     * @throws UnsupportedEncodingException e
     */
    @Override
    public String decrypt(byte[] cipherText) throws Exception {
        RSAPrivateKey prk = loadPrivateKeyByStr();
        // 获取私钥参数-指数/系数
        BigInteger d = prk.getPrivateExponent();
        BigInteger n = prk.getModulus();
        // 读取密文
        BigInteger c = new BigInteger(cipherText);
        // 进行解密
        BigInteger m = c.modPow(d, n);
        // 解密结果-字节数组
        byte[] mt = m.toByteArray();
        //转成String,此时是乱码
        String en = new String(mt);
        //再进行编码,最后返回解密后得到的明文
        return URLDecoder.decode(en, "UTF-8");
    }

    /**
     * 获取公钥
     *
     * @methodName: getPublicKey
     * @return: java.lang.String
     * @author: ybw
     * @date: 2022/8/12
     **/
    @Override
    public String getPublicKey() throws Exception {
        //1、获取redis key
        String publicRedisKey = getRedisKey(RsaConstant.PUBLIC_KEY);
        //2、获取公钥字符串
        String publicKeyStr = (String) redisTemplate.opsForValue().get(publicRedisKey);
        if (StringUtils.isNotBlank(publicKeyStr)) {
            log.info("RsaServiceImpl getPublicKey publicKeyStr:{}", publicKeyStr);
            return publicKeyStr;
        }
        //3、初始化
        init();
        //4、重新获取公钥字符串
        return getPublicKey();
    }

    /**
     * 获取私钥
     *
     * @methodName: getPrivateKey
     * @return: java.lang.String
     * @author: ybw
     * @date: 2022/8/12
     **/
    public String getPrivateKey() throws Exception {
        //1、获取redis key
        String privateRedisKey = getRedisKey(RsaConstant.PRIVATE_KEY);
        //2、获取私钥数据字符串
        String privateKeyStr = (String) redisTemplate.opsForValue().get(privateRedisKey);
        if (StringUtils.isNotBlank(privateKeyStr)) {
            log.info("RsaServiceImpl getPrivateKey privateKeyStr:{}", privateKeyStr);
            return privateKeyStr;
        }
        //3、初始化
        init();
        //4、重新获取公钥字符串
        return getPrivateKey();
    }
}
