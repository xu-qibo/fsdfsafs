package com.ybw.rsa.demo.constant;

/**
 * @author ybw
 * @version V1.0
 * @className RsaConstant
 * @date 2022/8/12
 **/
public interface RsaConstant {
    /**
     * 私钥
     *
     * @author: ybw
     * @date: 2022/8/12
     **/
    String PRIVATE_KEY = "PRIVATE_KEY";
    /**
     * 公钥
     *
     * @author: ybw
     * @date: 2022/8/12
     **/
    String PUBLIC_KEY = "PUBLIC_KEY";

    /**
     * 秘钥大小
     *
     * @author: ybw
     * @date: 2022/8/12
     **/
    Integer KEY_SIZE = 1024;
}
