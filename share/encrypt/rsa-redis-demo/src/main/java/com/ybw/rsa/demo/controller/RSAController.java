package com.ybw.rsa.demo.controller;

import com.ybw.rsa.demo.service.RsaService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author ybwei
 * @version V1.0
 * @className RSAController
 * @date 2022/8/12
 **/
@RestController
public class RSAController {
    @Resource
    private RsaService rsaService;

    /**
     * 获取公钥
     *
     * @methodName: getPublicKey
     * @return: java.lang.String
     * @author: ybw
     * @date: 2022/8/12
     **/
    @GetMapping("/getPublicKey")
    public String getPublicKey() throws Exception {
        return rsaService.getPublicKey();
    }
}
