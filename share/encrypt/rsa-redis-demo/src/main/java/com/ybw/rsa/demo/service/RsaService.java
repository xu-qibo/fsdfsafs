package com.ybw.rsa.demo.service;

/**
 * RSA接口
 *
 * @author ybwei
 * @version V1.0
 * @className RsaService
 * @date 2022/8/13
 **/
public interface RsaService {
    /**
     * 私钥解密
     *
     * @param encryptText
     * @methodName: decryptWithPrivate
     * @return: java.lang.String
     * @author: ybwei
     * @date: 2022/8/12
     **/
    String decryptWithPrivate(String encryptText) throws Exception;

    /**
     * 公钥加密-测试
     *
     * @param plaintext 明文内容
     * @methodName: encrypt
     * @return: byte[]
     * @author: ybwei
     * @date: 2022/8/12
     **/
    byte[] encrypt(String plaintext) throws Exception;

    /**
     * 私钥解密-测试
     *
     * @param cipherText 加密后的字节数组
     * @methodName: decrypt
     * @return: java.lang.String
     * @author: ybwei
     * @date: 2022/8/12
     **/
    String decrypt(byte[] cipherText) throws Exception;

    /**
     * 获取公钥
     *
     * @methodName: getPublicKey
     * @return: java.lang.String
     * @author: ybwei
     * @date: 2022/8/12
     **/
    String getPublicKey() throws Exception;
}
