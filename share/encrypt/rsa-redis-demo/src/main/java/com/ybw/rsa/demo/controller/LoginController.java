package com.ybw.rsa.demo.controller;

import com.alibaba.fastjson2.JSON;
import com.ybw.rsa.demo.service.RsaService;
import com.ybw.rsa.demo.vo.req.LoginReqVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@Controller
@Slf4j
public class LoginController {

    @Resource
    private RsaService rsaService;

    /**
     *
     * @methodName: login
     * @param model
     * @return: java.lang.String
     * @author: ybw
     * @date: 2022/8/12
     **/
    @GetMapping("/login")
    public String login(Model model){
        model.addAttribute("msg", "RSA前端加密，后端解密测试");
        return "login";
    }


    @PostMapping(value = "/login")
    @ResponseBody
    public String login(@RequestBody LoginReqVO loginReqVO) throws Exception {
        log.info("loginReqVO:{}", JSON.toJSONString(loginReqVO));
        //解密后的密码
        String password = rsaService.decryptWithPrivate(loginReqVO.getPassword());
        String username = rsaService.decryptWithPrivate(loginReqVO.getUsername());
        log.info("解密后密码，password:{}", password);
        log.info("解密后用户名，username:{}", username);
        return password+"---"+username;
    }
}
