# 工程简介
参考：https://blog.csdn.net/weixin_38192427/article/details/120588208
<br/>
##访问登录页面
http://localhost:8080/login
##调用流程
1、前端调用后端接口获取公钥，对密码进行加密。
<br/>
2、前端调用登录接口，将加密密码传到后端。
<br/>
3、后端对密码解密，存入数据库。
# 延伸阅读

