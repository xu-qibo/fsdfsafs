//package com.ybw.mybatis.plus.generator.demo.generator;
//
//import com.baomidou.mybatisplus.annotation.IdType;
//import com.baomidou.mybatisplus.generator.AutoGenerator;
//import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
//import com.baomidou.mybatisplus.generator.config.GlobalConfig;
//import com.baomidou.mybatisplus.generator.config.PackageConfig;
//import com.baomidou.mybatisplus.generator.config.StrategyConfig;
//import com.baomidou.mybatisplus.generator.config.converts.MySqlTypeConvert;
//import com.baomidou.mybatisplus.generator.config.rules.DbColumnType;
//import com.baomidou.mybatisplus.generator.config.rules.IColumnType;
//import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
//
///**
// * @ClassName MysqlGenerator
// * @Description:
// * @Author geoffrey
// * @Date 2021/4/18
// * @Version V1.0
//**/
//public class MysqlGenerator {
//
//
//    public static void main(String[] args) {
//        AutoGenerator mpg = new AutoGenerator();
//        //1、全局配置
//        GlobalConfig gc = new GlobalConfig();
//        String projectPath = System.getProperty("user.dir");
//        //生成路径(一般都是生成在此项目的src/main/java下面)
//        gc.setOutputDir(projectPath + "/src/main/java");
//        //设置作者
//        gc.setAuthor("ybw");
//        gc.setOpen(false);
//        //第二次生成会把第一次生成的覆盖掉
//        gc.setFileOverride(true);
//        //生成的service接口名字首字母是否为I，这样设置就没有
//        gc.setServiceName("%sService");
//        //实体命名方式
////        gc.setEntityName("%sDO");
////        gc.setMapperName("%sDOMapper");
////        gc.setXmlName("%sDOMapper");
//        // XML columList
//        gc.setBaseColumnList(true);
//        //生成resultMap
//        gc.setBaseResultMap(true);
//        //主键策略
//        gc.setIdType(IdType.AUTO);
//        mpg.setGlobalConfig(gc);
//
//        //2、数据源配置
//        DataSourceConfig dsc = new DataSourceConfig();
//        dsc.setUrl("jdbc:mysql://localhost:3306/test?useUnicode=true&serverTimezone=GMT&useSSL=false&characterEncoding=utf8");
//        dsc.setDriverName("com.mysql.cj.jdbc.Driver");
//        dsc.setUsername("root");
//        dsc.setPassword("123456");
//        //自定义数据库表字段类型转换
//        dsc.setTypeConvert(new MySqlTypeConvert(){
//            @Override
//            public IColumnType processTypeConvert(GlobalConfig config, String fieldType) {
//                if(fieldType.toLowerCase().contains("tinyint")){
//                    //tinyint转Integer
//                    return DbColumnType.INTEGER;
//                }
//                if(fieldType.toLowerCase().contains("bit")){
//                    return DbColumnType.INTEGER;
//                }
//                return super.processTypeConvert(config, fieldType);
//            }
//        });
//        mpg.setDataSource(dsc);
//
//        // 3、包配置
//        PackageConfig pc = new PackageConfig();
////        pc.setModuleName("sys");
//        pc.setParent("com.ybw.mybatis.plus.generator.demo");
//        mpg.setPackageInfo(pc);
//
//        // 4、策略配置
//        StrategyConfig strategy = new StrategyConfig();
//        strategy.setNaming(NamingStrategy.underline_to_camel);
//        strategy.setColumnNaming(NamingStrategy.underline_to_camel);
////        strategy.setSuperControllerClass("com.lcy.demo.sys.controller.BaseController");
////        strategy.setSuperEntityClass("com.lcy.demo.sys.entity.BaseEntity");
//        // 表名前缀
//        // strategy.setTablePrefix("t_");
//        // 使用lombok
//        strategy.setEntityLombokModel(true);
//        // 逆向工程使用的表   如果要生成多个,这里可以传入String[]
//        strategy.setInclude("user");
//        mpg.setStrategy(strategy);
//
//        //5、执行
//        mpg.execute();
//    }
//
//}