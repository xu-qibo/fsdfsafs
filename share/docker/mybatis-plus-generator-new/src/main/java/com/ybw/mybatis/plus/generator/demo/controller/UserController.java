package com.ybw.mybatis.plus.generator.demo.controller;

import com.ybw.mybatis.plus.generator.demo.entity.User;
import com.ybw.mybatis.plus.generator.demo.service.UserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author ybw
 * @since 2022-09-08
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Resource
    private UserService userService;

    /**
     * 列表
     *
     * @methodName: list
     * @return: java.util.List<com.ybw.mybatis.plus.generator.demo.entity.User>
     * @author: ybw
     * @date: 2022/9/25
     **/
    @GetMapping("/list")
    public List<User> list() {
        return userService.lambdaQuery()
                .last("limit 10")
                .list();
    }
}
