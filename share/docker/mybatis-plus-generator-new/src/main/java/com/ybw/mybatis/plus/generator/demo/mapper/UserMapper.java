package com.ybw.mybatis.plus.generator.demo.mapper;

import com.ybw.mybatis.plus.generator.demo.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ybw
 * @since 2022-09-08
 */
public interface UserMapper extends BaseMapper<User> {

}
