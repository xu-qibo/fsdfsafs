package com.ybw.mybatis.plus.generator.demo.generator;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.config.converts.MySqlTypeConvert;
import com.baomidou.mybatisplus.generator.config.querys.MySqlQuery;
import com.baomidou.mybatisplus.generator.config.rules.DbColumnType;
import com.baomidou.mybatisplus.generator.config.rules.IColumnType;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import com.baomidou.mybatisplus.generator.keywords.MySqlKeyWordsHandler;

import java.util.Collections;

/**
 * @ClassName MysqlGenerator
 * @Description:
 * @Author geoffrey
 * @Date 2021/4/18
 * @Version V1.0
 **/
public class MysqlGenerator2 {


    public static void main(String[] args) {
        //1、数据库配置
        DataSourceConfig.Builder DATA_SOURCE_CONFIG = new DataSourceConfig.Builder("jdbc:mysql://localhost:3306/test?useUnicode=true&serverTimezone=GMT&useSSL=false&characterEncoding=utf8", "root", "123456")
                .dbQuery(new MySqlQuery())
//                .schema("test")
                .typeConvert(new MySqlTypeConvert() {
                    @Override
                    public IColumnType processTypeConvert(GlobalConfig config, String fieldType) {
                        if (fieldType.toLowerCase().contains("tinyint")) {
                            //tinyint转Integer
                            return DbColumnType.INTEGER;
                        }
                        if (fieldType.toLowerCase().contains("bit")) {
                            return DbColumnType.INTEGER;
                        }
                        return super.processTypeConvert(config, fieldType);
                    }
                })
                .keyWordsHandler(new MySqlKeyWordsHandler());

        String projectPath = System.getProperty("user.dir");
        //生成路径(一般都是生成在此项目的src/main/java下面)
        FastAutoGenerator.create(DATA_SOURCE_CONFIG)
                .globalConfig(builder -> {
                    // 设置作者
                    builder.author("ybw")
                            // 开启 swagger 模式
//                            .enableSwagger()
//                            .fileOverride()
                            // 指定输出目录
                            .outputDir(projectPath + "/src/main/java")
                    ;
                })
                .packageConfig(builder -> {
                    // 设置父包名
                    builder.parent("com.ybw.mybatis.plus.generator.demo")
                            // 设置父包模块名
//                            .moduleName("system")
                            // 设置mapperXml生成路径
                            .pathInfo(Collections.singletonMap(OutputFile.xml, projectPath + "/src/main/resources/mapper"));
                })
                .strategyConfig(builder -> {
                    // 设置需要生成的表名,多个英文逗号分隔？所有输入 all
                    builder.addInclude("user")
                            // 设置过滤表前缀
//                            .addTablePrefix("t_", "c_")
//                            .entityBuilder().formatFileName("%sEntity")
//                            .mapperBuilder().formatMapperFileName("%sDao").formatXmlFileName("%sXml")
//                            .controllerBuilder().formatFileName("%sAction")
                            .serviceBuilder().formatServiceFileName("%sService")
                            //enableColumnConstant:开启生成字段常量
                            .entityBuilder().idType(IdType.AUTO).enableLombok().fileOverride().enableColumnConstant()
                            .mapperBuilder().enableBaseResultMap().enableBaseColumnList().fileOverride()
                    ;
                })
                // 使用Freemarker引擎模板，默认的是Velocity引擎模板
                .templateEngine(new FreemarkerTemplateEngine())
                .execute();

    }

}