package com.ybw.mybatis.plus.generator.demo.config;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @ClassName MybatisPlusConfig
 * @Description: 
 * @Author geoffrey
 * @Date 2021/10/8 
 * @Version V1.0
 **/
@Configuration
@MapperScan("com.ybw.mybatis.plus.generator.demo.mapper")
public class MybatisPlusConfig {

    /**
     * @Description 最新版
     * @author: ybwei
     * @Date: 2021/5/21 18:45
     */
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.MYSQL));
        return interceptor;
    }

}
