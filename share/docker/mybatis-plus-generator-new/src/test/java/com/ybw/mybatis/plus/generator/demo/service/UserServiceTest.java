package com.ybw.mybatis.plus.generator.demo.service;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.pagehelper.PageHelper;
import com.ybw.mybatis.plus.generator.demo.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @program: mybatis-plus-generator-demo
 * @description:
 * @author: geoffrey
 * @create: 2021-04-18 23:50
 */
@SpringBootTest
@Slf4j
public class UserServiceTest {

    @Resource
    private UserService userService;

    /**
     * @MethodName: save
     * @Description:建议
     * @Param: []
     * @Return: void
     * @Author: geoffrey
     * @Date: 2021/4/19
     **/
    @Test
    public void save() {
        for (int i = 0; i < 10; i++) {
            User user = new User();
            user.setName("张三" + i);
            user.setNo(15 + i);
            user.setCreateTime(LocalDateTime.now());
            userService.save(user);
        }
    }

    @Test
    public void updateById() {
        User user = new User();
        user.setId(1L);
        user.setNo(16);
        user.setUpdateTime(LocalDateTime.now());
        userService.updateById(user);
    }

    /**
     * @MethodName: update
     * @Description: 条件构造器作为参数进行更新
     * @Param: []
     * @Return: void
     * @Author: geoffrey
     * @Date: 2021/4/19
     **/
    @Test
    public void update() {
        UpdateWrapper<User> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("no", 16);

        User user = new User();
        user.setNo(17);
        user.setUpdateTime(LocalDateTime.now());
        userService.update(user, updateWrapper);
    }

    /**
     * @MethodName: update
     * @Description: 条件构造器Set方法
     * @Param: []
     * @Return: void
     * @Author: geoffrey
     * @Date: 2021/4/19
     **/
    @Test
    public void updateAndSet() {
        UpdateWrapper<User> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id", 1L).set("no", 19).set("update_time", LocalDateTime.now());

        userService.update(updateWrapper);
    }

    /**
     * @MethodName: updateAndChain
     * @Description: 链式更改
     * @Param: []
     * @Return: void
     * @Author: geoffrey
     * @Date: 2021/4/19
     **/
    @Test
    public void updateAndChain() {
        userService.lambdaUpdate().eq(User::getId, 1L).set(User::getNo, 21).set(User::getUpdateTime, LocalDateTime.now()).update();
    }

    /**
     * @MethodName: updateAndChainEntity
     * @Description: 链式更改(建议)
     * @Param: []
     * @Return: void
     * @Author: geoffrey
     * @Date: 2021/4/19
     **/
    @Test
    public void updateAndChainEntity() {
        User user = new User();
        user.setNo(22);
        user.setUpdateTime(LocalDateTime.now());
        userService.lambdaUpdate().eq(User::getId, 1L).update(user);
    }


    /**
     * @MethodName: removeById
     * @Description:删除（建议）
     * @Param: []
     * @Return: void
     * @Author: geoffrey
     * @Date: 2021/4/19
     **/
    @Test
    public void removeById() {
        userService.removeById(2L);
    }

    /**
     * @MethodName: removeByQuery
     * @Description:删除（建议）
     * @Param: []
     * @Return: void
     * @Author: geoffrey
     * @Date: 2021/4/19
     **/
    @Test
    public void removeByQuery() {
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("id", 1L);
        userService.remove(queryWrapper);
    }

    /**
     * @Description 链式删除（推荐）
     * @Author ybwei
     * @Date 2021/5/21 20:46
     * @Param []
     * @Return void
     * @Exception
     */
    @Test
    public void lambdaRemoveByQuery() {
        userService.remove(Wrappers.<User>lambdaQuery().eq(User::getId, 13L));
    }


    /**
     * @MethodName: get
     * @Description:
     * @Param: []
     * @Return: void
     * @Author: geoffrey
     * @Date: 2021/4/19
     **/
    @Test
    public void get() {
        User user = userService.getById(2L);
        log.info("user:{}", JSON.toJSONString(user));
    }

    /**
     * @MethodName: getOne
     * @Description:
     * @Param: []
     * @Return: void
     * @Author: geoffrey
     * @Date: 2021/4/19
     **/
    @Test
    public void getOne() {
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("id", 2L);
        User user = userService.getOne(queryWrapper);
        log.info("user:{}", JSON.toJSONString(user));
    }

    /**
     * @Description 链式查询（推荐）
     * @Author ybwei
     * @Date 2021/5/21 16:18
     * @Param []
     * @Return void
     * @Exception
     */
    @Test
    public void getLambdaOne() {
        User user = userService.lambdaQuery().eq(User::getId, "13").one();
        log.info("user:{}", JSON.toJSONString(user));
    }


    /**
     * @MethodName: list
     * @Description:
     * @Param: []
     * @Return: void
     * @Author: geoffrey
     * @Date: 2021/4/19
     **/
    @Test
    public void list() {
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.in("id", 2L, 3L, 4L);
        List<User> userList = userService.list(queryWrapper);
        log.info("userList:{}", JSON.toJSONString(userList));
    }

    /**
     * @Description 链式查询(推荐)
     * @Author ybwei
     * @Date 2021/5/21 16:20
     * @Param []
     * @Return void
     * @Exception
     */
    @Test
    public void lambdaList() {
        List<User> userList = userService.lambdaQuery().in(User::getId, 13L, 14L, 15L).le(User::getCreateTime, LocalDateTime.now()).list();
        log.info("userList:{}", JSON.toJSONString(userList));
    }


    /**
     * @return void
     * @throws
     * @description 带判断条件的链式查询(推荐)
     * @author ybwei
     * @date 2021/11/15 15:28
     * @params []
     */
    @Test
    public void lambdaListWithCondition() {
        List<Long> idList = new ArrayList<>();
        //如果idList不为空，才会作为判断条件
        List<User> userList = userService.lambdaQuery().in(CollectionUtils.isNotEmpty(idList), User::getId, idList).le(User::getCreateTime, LocalDateTime.now()).list();
        log.info("userList:{}", JSON.toJSONString(userList));
    }


    /**
     * @MethodName: page
     * @Description: mybatis plus分页
     * @Param: []
     * @Return: void
     * @Author: geoffrey
     * @Date: 2021/4/19
     **/
    @Test
    public void page() {
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.in("id", 2L, 3L, 4L);


        //参数一是当前页，参数二是每页个数
        IPage<User> page = new Page<User>(2, 2);

        IPage<User> iPage = userService.page(page, queryWrapper);
        List<User> userList = iPage.getRecords();
        log.info("userList:{}", JSON.toJSONString(userList));
    }

    /**
     * @Description 链式查询分页(推荐)
     * @Author ybwei
     * @Date 2021/5/21 20:38
     * @Param []
     * @Return void
     * @Exception
     */
    @Test
    public void lambdaPage() {
        //参数一是当前页，参数二是每页个数
        IPage<User> page = new Page<User>(2, 2);

        IPage<User> iPage = userService.page(page, Wrappers.lambdaQuery(User.class).in(User::getId, 13L, 14L, 15L));
        List<User> userList = iPage.getRecords();
        log.info("userList:{}", JSON.toJSONString(userList));
    }

    /**
     * 链式查询分页-(推荐)
     *
     * @return void
     * @throws
     * @methodName: lambdaPage2
     * @author ybwei
     * @date 2022/2/16 16:36
     */
    @Test
    public void lambdaPage2() {
        //参数一是当前页，参数二是每页个数
        IPage<User> page = new Page<User>(1, 2);
        userService.lambdaQuery()
                .page(page);

        log.info("page:{}", JSON.toJSONString(page));
    }

    /**
     * @MethodName: pageHelper
     * @Description:
     * @Param: []
     * @Return: void
     * @Author: geoffrey
     * @Date: 2021/12/25
     **/
    @Test
    public void pageHelper() {
        com.github.pagehelper.Page<Object> page = PageHelper.startPage(1, 2);
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.in("id", 2L, 3L, 4L);
        List<User> userList = userService.list(queryWrapper);

//        List<User> userList=userService.lambdaQuery()
//                .in(User::getId,13L, 14L, 15L)
//                .list();
        log.info("userList:{}", JSON.toJSONString(userList));
    }

    /**
     * @MethodName: lambdaPageHelper
     * @Description:
     * @Param: []
     * @Return: void
     * @Author: geoffrey
     * @Date: 2021/12/25
     **/
    @Test
    public void lambdaPageHelper() {
        com.github.pagehelper.Page<Object> page = PageHelper.startPage(1, 2)
                .doSelectPage(() ->
                        userService.lambdaQuery()
                                .in(User::getId, 1L, 2L, 3L)
                                .list()
                );
        log.info("userList:{}", JSON.toJSONString(page.getResult()));
    }

    /**
     * @MethodName: lambda2PageHelper
     * @Description:
     * @Param: []
     * @Return: void
     * @Author: geoffrey
     * @Date: 2021/12/25
     **/
    @Test
    public void lambda2PageHelper() {
        com.github.pagehelper.Page<Object> page = PageHelper.startPage(1, 2);
        List<User> userList = userService.lambdaQuery()
                .in(User::getId, 1L, 2L, 3L)
                .list();
        log.info("userList:{}", JSON.toJSONString(userList));
    }


    //--------------------------------------or-----------------------------------------------

    /**
     * @return void
     * @throws
     * @description or查询
     * https://blog.csdn.net/ab1024249403/article/details/110288959
     * SELECT id,name,no,create_time,update_time FROM user WHERE (id IN (4,5) OR (no = 18 OR no = 17))
     * @author ybwei
     * @date 2022/1/7 11:53
     * @params []
     */
    @Test
    public void lambdaOrList() {
        List<User> userList = userService.lambdaQuery()
                .in(User::getId, 4L, 5L)
                .or(userLambdaQueryWrapper -> userLambdaQueryWrapper.eq(User::getNo, 18).or().eq(User::getNo, 17))
                .list();
        log.info("userList:{}", JSON.toJSONString(userList));
    }

    /**
     * SELECT id,name,no,create_time,update_time FROM user WHERE (id = ? OR id = ? OR id = ?)
     *
     * @return void
     * @throws
     * @methodName: or2
     * @author ybwei
     * @date 2022/3/17 15:49
     */
    @Test
    public void or2() {
        List<User> userList = userService.lambdaQuery()
                .eq(User::getId, 3L)
                .or()
                .eq(User::getId, 4L)
                .or()
                .eq(User::getId, 5L)
                .list();
        log.info("userList:{}", JSON.toJSONString(userList));
    }


    //----------------------------------------count start------------------------------------------------

    //----------------------------------------条件判断 start------------------------------------------------

    /**
     * @return void
     * @throws
     * @description 判断
     * SELECT id,name,no,create_time,update_time FROM user WHERE (id = 4)
     * @author ybwei
     * @date 2022/1/7 13:47
     * @params []
     */
    @Test
    public void lambdaJudgeList() {
        List<User> userList = userService.lambdaQuery()
                .eq(true, User::getId, 4L)
                .eq(false, User::getNo, 16)
                .list();
        log.info("userList:{}", JSON.toJSONString(userList));
    }
    //----------------------------------------条件判断 end------------------------------------------------

    /**
     * @return void
     * @throws
     * @methodName: count
     * @author ybwei
     * @date 2022/3/17 15:31
     */
    @Test
    public void count() {
        Long count = userService.lambdaQuery()
                .in(User::getId, 3L, 4L, 5L)
                .count();
        log.info("count:{}", count);
    }

    //----------------------------------------count end-----------------------------------------


    /**
     * @return void
     * @throws
     * @methodName: like
     * @author ybwei
     * @date 2022/3/17 15:41
     */
    @Test
    public void like() {
        List<User> userList = userService.lambdaQuery()
                .like(User::getName, "张三")
                .list();
        log.info("userList:{}", JSON.toJSONString(userList));
    }


}
