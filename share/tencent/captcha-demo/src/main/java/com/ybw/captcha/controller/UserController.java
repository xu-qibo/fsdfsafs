package com.ybw.captcha.controller;

import com.ybw.captcha.service.UserService;
import com.ybw.captcha.vo.req.SendSmsReqVO;
import com.ybw.captcha.vo.res.SendSmsResVO;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author ybw
 * @version V1.0
 * @className UserController
 * @date 2022/9/15
 **/
@RestController
@RequestMapping("/user")
public class UserController {

    @Resource
    private UserService userService;

    /**
     * 发送短信验证码
     *
     * @param sendSmsReqVO
     * @methodName: sendSms
     * @return: com.ybw.captcha.vo.res.SendSmsResVO
     * @author: ybw
     * @date: 2022/9/15
     **/
    @PostMapping("/sendSms")
    public SendSmsResVO sendSms(@Validated @RequestBody SendSmsReqVO sendSmsReqVO) {
        //1、图形验证码校验
        boolean flag = userService.checkCaptcha(sendSmsReqVO);
        if (!flag) {
            //图形验证码校验失败
            return null;
        }
        //2、发送短信验证码
        return new SendSmsResVO();
    }
}
