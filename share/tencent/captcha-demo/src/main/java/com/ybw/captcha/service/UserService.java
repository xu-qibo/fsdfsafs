package com.ybw.captcha.service;

import com.ybw.captcha.vo.req.SendSmsReqVO;

/**
 * @author ybw
 * @version V1.0
 * @className UserService
 * @date 2022/9/15
 **/
public interface UserService {
    /**
     * 校验验证码
     *
     * @param sendSmsReqVO
     * @methodName: checkCaptcha
     * @return: java.lang.Boolean
     * @author: ybw
     * @date: 2022/9/15
     **/
    Boolean checkCaptcha(SendSmsReqVO sendSmsReqVO);
}
