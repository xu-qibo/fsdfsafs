package com.ybw.captcha.constant;

/**
 * @author ybw
 * @version V1.0
 * @className CaptchaCodeConstant
 * @date 2022/9/15
 **/
public interface CaptchaCodeConstant {
    /**
     * 1 OK 验证通过
     *
     * @author: ybw
     * @date: 2022/9/15
     **/
    Long OK = 1L;
}
