package com.ybw.captcha.config;

import com.tencentcloudapi.captcha.v20190722.CaptchaClient;
import com.tencentcloudapi.common.Credential;
import com.tencentcloudapi.common.profile.ClientProfile;
import com.tencentcloudapi.common.profile.HttpProfile;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;
/**
 * @className CaptchaConfigration
 * @author ybw
 * @date 2022/9/15 
 * @version V1.0
 **/
@Configuration
public class CaptchaConfigration {
    @Resource
    private CaptchaProperties captchaProperties;

    @Bean
    public CaptchaClient captchaClient(){
        // 实例化一个认证对象，入参需要传入腾讯云账户secretId，secretKey,此处还需注意密钥对的保密
        // 密钥可前往https://console.cloud.tencent.com/cam/capi网站进行获取
        Credential cred = new Credential(captchaProperties.getSecretId(), captchaProperties.getSecretKey());
        // 实例化一个http选项，可选的，没有特殊需求可以跳过
        HttpProfile httpProfile = new HttpProfile();
        httpProfile.setEndpoint("captcha.tencentcloudapi.com");
        // 实例化一个client选项，可选的，没有特殊需求可以跳过
        ClientProfile clientProfile = new ClientProfile();
        clientProfile.setHttpProfile(httpProfile);
        // 实例化要请求产品的client对象,clientProfile是可选的
        return new CaptchaClient(cred, "", clientProfile);
    }
}
