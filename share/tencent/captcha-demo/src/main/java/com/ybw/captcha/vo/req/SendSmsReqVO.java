package com.ybw.captcha.vo.req;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author ybw
 * @version V1.0
 * @className SendSmsReqVO
 * @date 2022/9/15
 **/
@Data
public class SendSmsReqVO {
    /**
     * 前端回调函数返回的用户验证票据
     *
     * @author: ybw
     * @date: 2022/9/15
     **/
    @NotNull
    private String ticket;
    /**
     * 前端回调函数返回的随机字符串
     *
     * @author: ybw
     * @date: 2022/9/15
     **/
    @NotNull
    private String randstr;
}
