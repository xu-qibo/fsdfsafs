package com.ybw.captcha.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 腾讯云-图形验证码
 *
 * @author ybw
 * @version V1.0
 * @className CaptchaProperties
 * @date 2022/9/15
 **/
@Data
@ConfigurationProperties(prefix = "tencent.captcha")
@Configuration
public class CaptchaProperties {

    /**
     * 云API密钥-secretId
     *
     * @author: ybw
     * @date: 2022/9/15
     **/
    private String secretId;
    /**
     * 云API密钥-secretKey
     *
     * @author: ybw
     * @date: 2022/9/15
     **/
    private String secretKey;
    /**
     * 验证码应用ID。登录 验证码控制台，在验证列表的【密钥】列，即可查看到CaptchaAppId。
     *
     * @author: ybw
     * @date: 2022/9/15
     **/
    private Long captchaAppId;
    /**
     * 验证码应用密钥。登录 验证码控制台，在验证列表的【密钥】列，即可查看到AppSecretKey。AppSecretKey属于服务器端校验验证码票据的密钥，请妥善保密，请勿泄露给第三方。
     *
     * @author: ybw
     * @date: 2022/9/15
     **/
    private String appSecretKey;
    /**
     * 固定填值：9。可在控制台配置不同验证码类型。
     *
     * @author: ybw
     * @date: 2022/9/15
     **/
    private Long captchaType;
}
