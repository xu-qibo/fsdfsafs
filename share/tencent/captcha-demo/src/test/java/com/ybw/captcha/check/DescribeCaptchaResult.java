package com.ybw.captcha.check;

import com.tencentcloudapi.common.Credential;
import com.tencentcloudapi.common.profile.ClientProfile;
import com.tencentcloudapi.common.profile.HttpProfile;
import com.tencentcloudapi.common.exception.TencentCloudSDKException;
import com.tencentcloudapi.captcha.v20190722.CaptchaClient;
import com.tencentcloudapi.captcha.v20190722.models.*;

public class DescribeCaptchaResult
{
    public static void main(String [] args) {
        try{
            // 实例化一个认证对象，入参需要传入腾讯云账户secretId，secretKey,此处还需注意密钥对的保密
            // 密钥可前往https://console.cloud.tencent.com/cam/capi网站进行获取
            Credential cred = new Credential("AKIDe7MM7NRHsNccNs88uSCiy0mJacOvCE0X", "JNCl6C3e060e3C6lAKEtJVS9bnjQr3zg");
            // 实例化一个http选项，可选的，没有特殊需求可以跳过
            HttpProfile httpProfile = new HttpProfile();
            httpProfile.setEndpoint("captcha.tencentcloudapi.com");
            // 实例化一个client选项，可选的，没有特殊需求可以跳过
            ClientProfile clientProfile = new ClientProfile();
            clientProfile.setHttpProfile(httpProfile);
            // 实例化要请求产品的client对象,clientProfile是可选的
            CaptchaClient client = new CaptchaClient(cred, "", clientProfile);
            // 实例化一个请求对象,每个接口都会对应一个request对象
            DescribeCaptchaResultRequest req = new DescribeCaptchaResultRequest();
            req.setCaptchaAppId(196288610L);
            req.setAppSecretKey("PlLJDrexE80LyZ6C1M0Bx27vm");
            req.setCaptchaType(9L);
            req.setRandstr("@zr3");
            req.setUserIp("127.0.0.1");
            req.setTicket("t03MN47jPK3UBuz8-E3zsJDLdAzCZ9PVu_Ec5aIn0T15BBjfZRoz5l1LsVM0dSLiN1b-NgvYIu-yrA5XJY_kat_NXLl-9qPZ26jB8EAfr3y5Yt8d-g_-H0p40iHAt6jvMQzIrIl3UzGGks*");


            // 返回的resp是一个DescribeCaptchaResultResponse的实例，与请求对象对应
            DescribeCaptchaResultResponse resp = client.DescribeCaptchaResult(req);
            // 输出json格式的字符串回包
            System.out.println(DescribeCaptchaResultResponse.toJsonString(resp));
        } catch (TencentCloudSDKException e) {
            System.out.println(e.toString());
        }
    }
}