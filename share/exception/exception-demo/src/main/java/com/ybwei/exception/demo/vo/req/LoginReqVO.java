package com.ybwei.exception.demo.vo.req;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author ybwei
 * @date 2022/3/17 14:13
 **/
@Data
public class LoginReqVO {
    @NotNull(message = "name不为空")
    private String name;
    @NotNull(message = "password不为空")
    private String password;
}
