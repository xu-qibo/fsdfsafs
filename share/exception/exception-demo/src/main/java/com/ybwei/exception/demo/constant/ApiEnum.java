package com.ybwei.exception.demo.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author weixiansheng
 * @version V1.0
 * @className ApiEnum
 * @date 2023/9/11
 **/
public interface ApiEnum {


    /**
     * 类型-6、7位数
     *
     * @author weixiansheng
     * @version V1.0
     * @className ApiEnum
     * @date 2023/9/11
     **/
    interface Type {
        /**
         * 系统
         *
         * @author: weixiansheng
         * @date: 2023/9/11
         **/
        Integer SYSTEM = 1 * 100000;
        /**
         * 用户
         *
         * @author: weixiansheng
         * @date: 2023/9/11
         **/
        Integer USER = 2 * SYSTEM;
    }

    /**
     * 场景4、5位数
     *
     * @author weixiansheng
     * @version V1.0
     * @className ApiEnum
     * @date 2023/9/11
     **/
    interface Scene {
        /**
         * 登录
         *
         * @author: weixiansheng
         * @date: 2023/9/11
         **/
        Integer SYSTEM = 1000;
        /**
         * 抽奖
         *
         * @author: weixiansheng
         * @date: 2023/9/11
         **/
        Integer DRAW = 2 * SYSTEM;
        /**
         * 积分
         *
         * @author: weixiansheng
         * @date: 2023/9/11
         **/
        Integer POINT = 3 * SYSTEM;
    }

    /**
     * @author ybwei
     * @Title: ApiStatusEnum.java
     * @ProjectName com.spring.pro.mybatis.page
     * @Description:接口返回码
     * @date 2019年9月16日 下午7:46:59
     */
    @Getter
    @AllArgsConstructor
    enum ApiStatusEnum {
        // ----------------------------系统错误码-------------------------
        /**
         * 成功
         *
         * @author weiyb
         */
        OK(200, "成功"),
        /**
         * 异常
         *
         * @author weiyb
         */
        EXCEPTION(Type.SYSTEM + Scene.SYSTEM + 0, "网络或服务异常，请稍后重试！"),
        /**
         * 失败
         *
         * @author weiyb
         */
        FAIL(Type.SYSTEM + Scene.SYSTEM + 1, "失败"),
        /**
         * 参数不全
         */
        PARAM_INCOMPLETE(Type.SYSTEM + Scene.SYSTEM + 2, "参数不全"),
        /**
         * 鉴权失败
         *
         * @author ybwei
         */
        AUTH_FAIL(Type.SYSTEM + Scene.SYSTEM + 3, "用户未登录"),

        // -------------------------业务错误码------------------------------------
        /**
         * 当前抽奖人数过多，请稍后重试
         *
         * @author ybwei
         */
        DRAW_LIMIT(Type.USER + Scene.DRAW + 0, "当前抽奖人数过多，请稍后重试"),
        /**
         * 今天抽奖次数已满
         *
         * @author ybwei
         */
        DRAW_USER_LIMIT(Type.USER + Scene.DRAW + 1, "今天抽奖次数已满"),
        /**
         * 积分不足
         *
         * @author ybwei
         */
        POINTS_NOT_ENOUGH(Type.USER + Scene.POINT + 0, "积分不足");

        private Integer code;
        private String depict;
    }
}
