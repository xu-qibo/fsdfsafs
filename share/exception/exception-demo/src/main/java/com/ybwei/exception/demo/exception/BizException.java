package com.ybwei.exception.demo.exception;

import com.ybwei.exception.demo.constant.ApiEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 业务逻辑异常
 *
 * @author ybwei
 * @Description
 * @date 2021/4/7 15:46
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BizException extends RuntimeException {
    private ApiEnum.ApiStatusEnum apiStatusEnum;
}
