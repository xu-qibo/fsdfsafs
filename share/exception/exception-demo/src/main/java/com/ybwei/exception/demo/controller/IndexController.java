package com.ybwei.exception.demo.controller;

import com.ybwei.exception.demo.constant.ApiEnum;
import com.ybwei.exception.demo.exception.BizException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName: UserController 
 * @Description:  
 * @author ybw 
 * @date 2017年4月14日 下午3:24:50 
 */
@RestController
public class IndexController {

	/**
	 * @param number
	 * @return
	 * @author weiyb 
	 */
	@GetMapping("/index/{number}")
	public String index(@PathVariable int number) {
		System.out.println(20/number);
		return "SUCCESS";
	}

	@GetMapping("/test/error")
	public String error() {
		throw new BizException(ApiEnum.ApiStatusEnum.AUTH_FAIL);
	}
}
