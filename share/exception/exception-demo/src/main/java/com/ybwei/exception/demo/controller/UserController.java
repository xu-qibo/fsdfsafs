package com.ybwei.exception.demo.controller;


import com.alibaba.fastjson.JSON;
import com.ybwei.exception.demo.vo.base.ApiResult;
import com.ybwei.exception.demo.vo.base.ApiResultGenerator;
import com.ybwei.exception.demo.vo.req.LoginReqVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * @author ybwei
 * @date 2022/3/17 14:09
 **/
@RestController
@RequestMapping("/user")
@Slf4j
public class UserController {

    /**
     * 登录
     *
     * @param loginReqVO
     * @return com.ybwei.exception.demo.vo.base.ApiResult
     * @throws
     * @methodName: login
     * @author ybwei
     * @date 2022/3/17 14:13
     */
    @PostMapping("/login")
    public ApiResult login(@Valid @RequestBody LoginReqVO loginReqVO) {
        log.info("loginReqVO:{}", JSON.toJSONString(loginReqVO));
        return ApiResultGenerator.successResult(null);
    }
}
