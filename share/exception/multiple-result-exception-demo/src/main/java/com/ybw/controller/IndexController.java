package com.ybw.controller;

import com.ybw.constant.ApiEnum;
import com.ybw.exception.BizException;
import com.ybw.vo.base.ApiResult;
import com.ybw.vo.base.ApiResultGenerator;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * 首页
 *
 * @author ybw
 * @ClassName: UserController
 * @Description:
 * @date 2017年4月14日 下午3:24:50
 */
@RestController
public class IndexController {

    /**
     * 首页-传入参数
     *
     * @param number
     * @methodName: index
     * @return: java.lang.String
     * @author: weixiansheng
     * @date: 2023/11/9
     **/
    @GetMapping("/index/{number}")
    public ApiResult index(@PathVariable int number) {
        System.out.println(20 / number);
        return ApiResultGenerator.successResult(null);
    }

    /**
     * 测试错误
     *
     * @methodName: error
     * @return: java.lang.String
     * @author: weixiansheng
     * @date: 2023/11/9
     **/
    @GetMapping("/test/error")
    public ApiResult error() {
        throw new BizException(ApiEnum.ApiStatusEnum.AUTH_FAIL);
    }
}
