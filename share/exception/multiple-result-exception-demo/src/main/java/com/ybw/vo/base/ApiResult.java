package com.ybw.vo.base;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

/**
 * @author weiyb
 * @ClassName: ApiResult
 * @Description:
 * @date 2017年11月16日 下午2:16:50
 */
@Data
@AllArgsConstructor
public class ApiResult implements Serializable {

	private static final long serialVersionUID = -2464446675828413133L;
	/**
	 * 消息提示
	 *
	 * @author weiyb
	 */
	private String msg;
	/**
	 * 返回结构
	 *
	 * @author weiyb
	 */
	private Object data;
	/**
	 * 返回码
	 *
	 * @author weiyb
	 */
	private int code;

	/**
	 * 禁止new创建对象
	 */
	private ApiResult() {
	}

	/**
	 * 统一创建ApiResultBean对象 方便以后扩展
	 *
	 * @return
	 * @author weiyb
	 */
	public static ApiResult getInstance() {
		return new ApiResult();
	}

}
