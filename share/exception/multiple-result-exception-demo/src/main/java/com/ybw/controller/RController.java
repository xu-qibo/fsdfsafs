package com.ybw.controller;

import com.ybw.constant.ApiEnum;
import com.ybw.exception.BizException;
import com.ybw.vo.base.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * R结果返回
 *
 * @author weixiansheng
 * @version V1.0
 * @className RController
 * @date 2023/11/9
 **/
@RestController
@RequestMapping("/r")
@Slf4j
public class RController {

    /**
     * 获取R结果
     *
     * @methodName: getR
     * @return: com.ybw.vo.base.R
     * @author: weixiansheng
     * @date: 2023/11/9
     **/
    @GetMapping("/getR")
    public R getR() {
        throw new BizException(ApiEnum.ApiStatusEnum.AUTH_FAIL);
//        return R.ok().put("returnMsg", "ok");
    }
}
