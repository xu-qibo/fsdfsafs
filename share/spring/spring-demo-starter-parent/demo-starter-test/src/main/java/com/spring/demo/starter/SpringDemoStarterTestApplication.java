package com.spring.demo.starter;

import com.ybw.spring.demo.autoconfigure.EnableDemo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//@EnableDemo
public class SpringDemoStarterTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringDemoStarterTestApplication.class, args);
    }

}
