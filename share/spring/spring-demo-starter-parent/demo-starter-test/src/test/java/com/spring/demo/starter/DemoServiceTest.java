package com.spring.demo.starter;

import com.ybw.spring.demo.service.DemoService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

/**
 * @author ybw
 * @version V1.0
 * @className DemoServiceTest
 * @date 2022/6/30
 **/
@SpringBootTest
@Slf4j
public class DemoServiceTest {
    @Resource
    private DemoService demoService;

    /**
     * @methodName: demoStarter
     * @return: void
     * @author: ybw
     * @date: 2022/6/30
     **/
    @Test
    public void demoStarter() {
        demoService.demoStarter();
    }
}
