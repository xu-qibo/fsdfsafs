package com.ybw.spring.demo.autoconfigure;
 
import com.ybw.spring.demo.service.DemoService;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * @className DemoAutoConfiguration
 * @author ybw
 * @date 2022/6/30 
 * @version V1.0
 **/
@Configuration
////条件判断，只有在使用了@EnableUser时，才会启动
//@ConditionalOnBean(annotation = EnableDemo.class)
public class DemoAutoConfiguration {
	@Bean
	public DemoService dbCountRunner() {
		return new DemoService();
	}
}