package com.ybw.spring.demo.autoconfigure;

import java.lang.annotation.*;

/**
 * @author ybw
 * @version V1.0
 * @className EnableDemo
 * @date 2022/6/30
 **/
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface EnableDemo {

}