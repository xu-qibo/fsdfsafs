package com.ybw.spring.demo.service;

import lombok.extern.slf4j.Slf4j;

/**
 * @author ybw
 * @version V1.0
 * @className DemoService
 * @date 2022/6/30
 **/
@Slf4j
public class DemoService {
    public void demoStarter() {
        log.info("hello world");
    }
}