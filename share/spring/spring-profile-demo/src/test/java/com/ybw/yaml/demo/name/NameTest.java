package com.ybw.yaml.demo.name;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

@Slf4j
@SpringBootTest
public class NameTest {

    @Value("${name}")
    private String name;
    @Value("${tt}")
    private String tt;

    @Test
    public void test() {
        log.info("name:{}", name);
        log.info("tt:{}", tt);
    }
}
