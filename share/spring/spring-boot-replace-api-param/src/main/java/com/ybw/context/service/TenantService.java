package com.ybw.context.service;

/**
 * @className TenantService
 * @author ybw
 * @date 2022/10/1 
 * @version V1.0
 **/
public interface TenantService {

    /**
     *
     * @methodName: print
     * @return: void
     * @author: ybw
     * @date: 2022/10/1
     **/
    void print();
}
