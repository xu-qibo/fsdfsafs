package com.ybw.context.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author ybw
 * @version V1.0
 * @className TenantDTO
 * @date 2022/9/30
 **/
@NoArgsConstructor
@AllArgsConstructor
@Data
public class TenantDTO {
    /**
     * 组合层级
     *
     * @author: ybw
     * @date: 2022/9/30
     **/
    private Integer level;
    /**
     * 租户id
     *
     * @author: ybw
     * @date: 2022/9/30
     **/
    private Long tenantId;
}
