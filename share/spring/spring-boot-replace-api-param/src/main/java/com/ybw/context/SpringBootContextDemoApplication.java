package com.ybw.context;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootContextDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootContextDemoApplication.class, args);
    }

}
