package com.ybw.context.service.impl;

import com.alibaba.fastjson2.JSON;
import com.ybw.context.config.TenantContext;
import com.ybw.context.dto.TenantDTO;
import com.ybw.context.service.TenantService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class TenantServiceImpl implements TenantService {

    @Override
    public void print() {
        TenantDTO tenantDTO = TenantContext.get();
        TenantContext.remove();
        if (tenantDTO == null) {
            return;
        }
        log.info("tenantDTO:{}", JSON.toJSONString(tenantDTO));
    }
}
