package com.ybw.validation.demo.vo;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import javax.validation.constraints.Size;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author geoffrey
 * @version V1.0
 * @className UserTest
 * @date 2022/2/8
 **/
@Slf4j
class UserTest {

    /**
     * @className UserTest
     * @author geoffrey
     * @date 2022/2/8
     * @version V1.0
     **/
    @Test
    public void buildReqIn() {
        User user = new User(1L, "张三2", 12);
        ReqIn<User> reqIn = new ReqIn<>("header", user);
        log.info("{}", JSON.toJSONString(reqIn));
    }
}