package com.ybw.validation.demo.controller;

import com.alibaba.fastjson.JSON;
import com.ybw.validation.demo.vo.ReqIn;
import com.ybw.validation.demo.vo.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;


/**
 * @author geoffrey
 * @version V1.0
 * @className UserController
 * @date 2022/2/8
 **/
@RestController
@Slf4j
public class UserController {

    /**
     * 校验
     *
     * @param reqIn
     * @methodName: valid
     * @return: org.springframework.http.HttpStatus
     * @author: geoffrey
     * @date: 2022/2/8
     **/
    @PostMapping("/valid")
    public HttpStatus valid(@Valid @RequestBody ReqIn<User> reqIn) {
        log.info("reqIn:{}", JSON.toJSONString(reqIn));
        return HttpStatus.OK;
    }
}