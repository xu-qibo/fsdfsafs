package com.ybw.validation.demo.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author geoffrey
 * @version V1.0
 * @className User
 * @date 2022/2/8
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User implements Serializable {

	private static final long serialVersionUID = 6747944028911495569L;
	private Long id;
	@NotBlank
	private String name;
	@NotNull
	private Integer age;
}