package com.ybw.validation.demo.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import java.io.Serializable;

/**
 * 请求参数
 *
 * @author geoffrey
 * @version V1.0
 * @className ReqIn
 * @date 2022/2/8
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReqIn<T> implements Serializable {

    private static final long serialVersionUID = 25549320423002325L;
    /**
     * 请求头信息
     */
    private String head;
    /**
     * 请求主体信息
     */
    @Valid
    private T data;

}