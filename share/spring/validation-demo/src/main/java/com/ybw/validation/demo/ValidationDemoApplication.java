package com.ybw.validation.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/**
 * @author geoffrey
 * @version V1.0
 * @className ValidationDemoApplication
 * @date 2022/2/8
 **/
@SpringBootApplication
public class ValidationDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(ValidationDemoApplication.class, args);
    }

}
