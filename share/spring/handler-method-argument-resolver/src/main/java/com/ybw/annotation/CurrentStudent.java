package com.ybw.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 当前学生
 *
 * @author weixiansheng
 * @version V1.0
 * @className CurrentUser
 * @date 2023/11/3
 **/
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
public @interface CurrentStudent {
}
