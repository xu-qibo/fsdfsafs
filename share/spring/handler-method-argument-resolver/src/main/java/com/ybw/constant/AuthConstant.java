package com.ybw.constant;

/**
 * @author ybw
 * @version V1.0
 * @className AuthConstant
 * @date 2023/11/4
 **/
public class AuthConstant {
    /**
     * token
     *
     * @author: ybw
     * @date: 2023/11/4
     **/
    public static final String TOKEN = "token";
    /**
     * 渠道
     *
     * @author: ybw
     * @date: 2023/11/4
     **/
    public static final String CHANNEL = "channel";

    /**
     * 学生
     *
     * @author: ybw
     * @date: 2023/11/4
     **/
    public static final Integer STUDENT = 1;
    /**
     * 老师
     *
     * @author: ybw
     * @date: 2023/11/4
     **/
    public static final Integer TEACHER = 2;
}
