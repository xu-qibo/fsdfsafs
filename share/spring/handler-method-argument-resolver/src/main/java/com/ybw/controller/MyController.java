package com.ybw.controller;

import com.ybw.annotation.CurrentTeacher;
import com.ybw.annotation.CurrentStudent;
import com.ybw.entity.Teacher;
import com.ybw.entity.Student;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class MyController {

    /**
     * 获取当前学生
     *
     * @param currentStudent
     * @methodName: getStudent
     * @return: java.lang.String
     * @author: ybw
     * @date: 2023/11/4
     **/
    @GetMapping("/getStudent")
    public Student getStudent(@CurrentStudent Student currentStudent) {
        log.info("current user info: {}", currentStudent);
        return currentStudent;
    }


    /**
     * 获取当前老师信息
     *
     * @param
     * @methodName: getTeacher
     * @return: java.lang.String
     * @author: ybw
     * @date: 2023/11/4
     **/
    @GetMapping("/getTeacher")
    public Teacher getTeacher(@CurrentTeacher Teacher currentTeacher) {
        log.info("current teacher info: {}", currentTeacher);
        return currentTeacher;
    }


    /**
     * 获取当前用户或教师对象
     *
     * @param currentStudent 当前学生对象
     * @param teacher        当前教师对象
     * @return 当前用户对象或教师对象，如果两者都为空则返回null
     */
    @GetMapping("/getTeacherOrUser")
    public Object getTeacherOrUser(@CurrentStudent Student currentStudent, @CurrentTeacher Teacher currentTeacher) {
        log.info("currentUser: {}，currentTeacher:{}", currentStudent, currentTeacher);
        if (currentTeacher != null) {
            return currentTeacher;
        } else if (currentStudent != null) {
            return currentStudent;
        }
        return null;
    }


}
