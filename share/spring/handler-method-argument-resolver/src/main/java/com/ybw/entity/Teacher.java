package com.ybw.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 老师
 *
 * 老师实体类，用于存储老师的相关信息
 *
 * @author ybw
 * @version V1.0
 * @className Teacher
 * @date 2023/11/4
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Teacher {
    private Long id;
    private String name;
}
