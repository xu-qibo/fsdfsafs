package com.ybw.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 当前老师
 *
 * @author ybw
 * @version V1.0
 * @className CurrentTeacher
 * @date 2023/11/4
 **/
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
public @interface CurrentTeacher {
}
