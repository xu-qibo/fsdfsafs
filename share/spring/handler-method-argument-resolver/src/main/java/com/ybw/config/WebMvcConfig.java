package com.ybw.config;

import jakarta.annotation.Resource;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

/**
 * @className WebMvcConfig
 * @author ybw
 * @date 2023/11/4
 * @version V1.0
 **/
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

    @Resource
    private CurrentUserResolver currentUserResolver;

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> resolvers) {
        resolvers.add(currentUserResolver);
    }
}
