package com.ybw.context.service;

import com.ybw.context.config.TenantContext;
import com.ybw.context.dto.TenantDTO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Slf4j
class TenantServiceTest {

    @Resource
    private TenantService tenantService;

    @Test
    void print() {
        TenantContext.set(new TenantDTO(1,12L));
        tenantService.print();
    }
}