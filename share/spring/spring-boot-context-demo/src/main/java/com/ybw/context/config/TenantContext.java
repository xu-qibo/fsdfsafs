package com.ybw.context.config;


import com.ybw.context.dto.TenantDTO;

/**
 * @author ybw
 * @version V1.0
 * @className TenantContext
 * @date 2022/9/30
 **/
public class TenantContext {

    private static final ThreadLocal<TenantDTO> context = new ThreadLocal<>();

    /**
     * 构造方法私有化
     *
     * @methodName: TenantContext
     * @return:
     * @author: ybw
     * @date: 2022/9/30
     **/
    private TenantContext() {

    }


    /**
     * 存放租户信息
     *
     * @param tenantDTO
     * @methodName: set
     * @return: void
     * @author: ybw
     * @date: 2022/9/30
     **/
    public static void set(TenantDTO tenantDTO) {
        context.set(tenantDTO);
    }

    /**
     * 获取组合信息
     *
     * @methodName: get
     * @return: com.ybw.mybatis.multi.tenant.dto.TenantDTO
     * @author: ybw
     * @date: 2022/9/30
     **/
    public static TenantDTO get() {
        return context.get();
    }

    /**
     * 清除当前线程内引用，防止内存泄漏
     *
     * @methodName: remove
     * @return: void
     * @author: ybw
     * @date: 2022/9/30
     **/
    public static void remove() {
        context.remove();
    }
}
