package com.ybw.service;

import com.ybw.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author weixiansheng
 * @version V1.0
 * @className UserServiceTest
 * @date 2023/9/28
 **/
@Slf4j
@SpringBootTest
class UserServiceTest {

    @Resource
    private UserService userService;

    @Test
    void save() {
        User user = new User(1L, "张三");
        userService.save(user);
    }
}