package com.ybw.event.listener;

import com.ybw.event.pojo.MyEvent;
import com.ybw.util.SpringContextHolder;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.concurrent.TimeUnit;

/**
 * @author weixiansheng
 * @version V1.0
 * @className MyListenerTest
 * @date 2023/9/28
 **/
@SpringBootTest
@Slf4j
class MyListenerTest {


    /**
     * 发布消息
     *
     * @methodName: publishEvent
     * @return: void
     * @author: weixiansheng
     * @date: 2023/9/28
     **/
    @Test
    public void publishEvent() throws InterruptedException {
        log.info("publishEvent start");
        SpringContextHolder.publishEvent(new MyEvent(this, "测试"));
        log.info("publishEvent end");
        TimeUnit.DAYS.sleep(1);
    }
}