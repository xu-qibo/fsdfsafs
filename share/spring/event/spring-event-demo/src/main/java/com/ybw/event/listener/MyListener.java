package com.ybw.event.listener;

import com.ybw.event.pojo.MyEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * 简单监听
 *
 * @author weixiansheng
 * @version V1.0
 * @className MyListener
 * @date 2023/9/28
 **/
@Component
@Slf4j
public class MyListener {

    /**
     * 普通监听
     *
     * @param event
     * @methodName: handleDemoEvent
     * @return: void
     * @author: weixiansheng
     * @date: 2023/9/28
     **/
    @EventListener
    public void handleEvent(MyEvent event) throws InterruptedException {
        TimeUnit.SECONDS.sleep(1);
        log.info("handleEvent data:{}", event.getData());
    }


    /**
     * 条件监听
     *
     * @param event
     * @methodName: handleConditionEvent
     * @return: void
     * @author: weixiansheng
     * @date: 2023/9/28
     **/
    @EventListener(condition = "#event.data=='张三'")
    public void handleConditionEvent(MyEvent event) {
        log.info("handleConditionEvent data:{}", event.getData());
    }
}