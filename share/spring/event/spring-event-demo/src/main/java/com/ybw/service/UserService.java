package com.ybw.service;

import com.ybw.entity.User;

/**
 * @author weixiansheng
 * @version V1.0
 * @className UserService
 * @date 2023/9/28
 **/
public interface UserService {

    /**
     * 保存用户
     *
     * @param user
     * @methodName: save
     * @return: void
     * @author: weixiansheng
     * @date: 2023/9/28
     **/
    void save(User user);
}
