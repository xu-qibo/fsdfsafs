package com.ybw.config.thread;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.ThreadPoolExecutor;


/**
 * 线程池配置
 *
 * @author weixiansheng
 * @version V1.0
 * @className ThreadPoolConfig
 * @date 2023/9/28
 **/
@Configuration
@EnableAsync
@Slf4j
public class ThreadPoolConfig {

    /**
     * cpu 核心数量
     *
     * @author: ybwei
     * @date: 2022/1/20 11:13
     */
    private final int cpuNum = Runtime.getRuntime().availableProcessors();

    /**
     * 线程池配置
     *
     * @return java.util.concurrent.Executor
     * @throws
     * @methodName: getAsyncExecutor
     * @author ybwei
     * @date 2022/1/20 11:13
     */
    @Bean("taskExecutor")
    public ThreadPoolTaskExecutor getAsyncExecutor() {
        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        taskExecutor.setCorePoolSize(cpuNum);
        taskExecutor.setMaxPoolSize(cpuNum * 2);
        taskExecutor.setQueueCapacity(1024);
        taskExecutor.setAwaitTerminationSeconds(60);
        taskExecutor.setKeepAliveSeconds(60);
        taskExecutor.setWaitForTasksToCompleteOnShutdown(true);
        taskExecutor.setThreadNamePrefix("thread-pool-");
        taskExecutor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        // 线程池初始化
        taskExecutor.initialize();
        log.info("线程池初始化......, CPU 核心数量:{}", cpuNum);
        return taskExecutor;
    }
}