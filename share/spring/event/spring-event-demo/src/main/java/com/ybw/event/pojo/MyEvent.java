package com.ybw.event.pojo;

import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;

/**
 * @className MyEvent
 * @author weixiansheng
 * @date 2023/9/28 
 * @version V1.0
 **/
@Setter
@Getter
public class MyEvent extends ApplicationEvent {
    private String data;

    public MyEvent(Object source, String data) {
        super(source);
        this.data = data;
    }
}