package com.ybw.service.impl;

import com.alibaba.fastjson2.JSON;
import com.ybw.entity.User;
import com.ybw.event.pojo.MyEvent;
import com.ybw.service.UserService;
import com.ybw.util.SpringContextHolder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author weixiansheng
 * @version V1.0
 * @className UserServiceImpl
 * @date 2023/9/28
 **/
@Service
@Slf4j
public class UserServiceImpl implements UserService {


    @Override
    public void save(User user) {
        log.info("user:{}", JSON.toJSONString(user));
        SpringContextHolder.publishEvent(new MyEvent(this, user.getName()));
    }
}
