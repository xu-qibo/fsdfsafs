package com.ybw.state;

import com.ybw.constant.RedisPreConstant;
import com.ybw.constant.RegEventEnum;
import com.ybw.constant.RegStatusEnum;
import com.ybw.entity.Order;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.data.redis.RedisStateMachinePersister;

/**
 * @author weixiansheng
 * @version V1.0
 * @className StateTest
 * @date 2023/12/26
 **/
@SpringBootTest
@Slf4j
public class StateTest {
    @Resource
    private StateMachine<RegStatusEnum, RegEventEnum> stateMachine;
    @Resource(name = "stateMachineRedisPersister")
    private RedisStateMachinePersister<RegStatusEnum, RegEventEnum> persister;


    /**
     * @MethodName: testState
     * @Description:
     * @Param: []
     * @Return: void
     * @Author: ybwei
     * @Date: 2020/3/26
     **/
    @Test
    public void testState() {
        try {
            //1、订单数据
            Order order = Order.builder()
                    .id(1L)
                    .name("张三")
                    .status(RegStatusEnum.CONNECTED)
                    .build();
            //2、获取redis key
            String redisKey = getRedisKey(order);
            //3、启动状态机
            stateMachine.start();
            //4、尝试恢复状态机状态(read)
            persister.restore(stateMachine, redisKey);
            Message<RegEventEnum> message = MessageBuilder.withPayload(RegEventEnum.REGISTER).setHeader("order", order).build();
            stateMachine.sendEvent(message);
            //5、持久化状态机状态(write)
            persister.persist(stateMachine, redisKey);
        } catch (Exception e) {
            log.error("testState error:", e);
        }
    }

    /**
     * 获取redis key
     *
     * @param order
     * @methodName: getRedisKey
     * @return: java.lang.String
     * @author: weixiansheng
     * @date: 2023/12/27
     **/
    private static String getRedisKey(Order order) {
        return RedisPreConstant.STATE_MACHINE + order.getId();
    }
}
