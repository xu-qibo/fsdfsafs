package com.ybw.constant;


/**
 * 事件
 *
 * @author weixiansheng
 * @version V1.0
 * @className RegEventEnum
 * @date 2023/12/26
 **/
public enum RegEventEnum {
    // 连接
    CONNECT,
    // 注册
    REGISTER,
    // 注册成功
    REGISTER_SUCCESS,
    // 注册失败
    REGISTER_FAILED,
    // 注销
    UN_REGISTER;
}
