package com.ybw.config;

import com.alibaba.fastjson2.JSON;
import com.ybw.constant.RegEventEnum;
import com.ybw.entity.Order;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.Message;
import org.springframework.statemachine.annotation.OnTransition;
import org.springframework.statemachine.annotation.WithStateMachine;

/**
 * 状态机的转换事件配置
 *
 * @author weixiansheng
 * @version V1.0
 * @className StateMachineEventConfig
 * @date 2023/12/26
 **/
@WithStateMachine
@Slf4j
public class StateMachineEventConfig {
    /**
     * 连接事件
     *
     * @param message
     * @methodName: connect
     * @return: void
     * @author: weixiansheng
     * @date: 2023/12/26
     **/
    @OnTransition(source = "UNCONNECTED", target = "CONNECTED")
    public void connect(Message<RegEventEnum> message) {
        Order order = (Order) message.getHeaders().get("order");
        log.info("///////////////////");
        log.info("连接事件, 未连接 -> 已连接,order:{}", JSON.toJSONString(order));
        log.info("///////////////////");
    }

    /**
     * 注册事件
     *
     * @param message
     * @methodName: register
     * @return: void
     * @author: weixiansheng
     * @date: 2023/12/26
     **/
    @OnTransition(source = "CONNECTED", target = "REGISTERING")
    public void register(Message<RegEventEnum> message) {
        log.info("///////////////////");
        log.info("注册事件, 已连接 -> 注册中");
        log.info("///////////////////");
    }

    /**
     * 注册成功事件
     *
     * @param message
     * @methodName: registerSuccess
     * @return: void
     * @author: weixiansheng
     * @date: 2023/12/26
     **/
    @OnTransition(source = "REGISTERING", target = "REGISTERED")
    public void registerSuccess(Message<RegEventEnum> message) {
        log.info("///////////////////");
        log.info("注册成功事件, 注册中 -> 已注册");
        log.info("///////////////////");
    }

    /**
     * 注销事件
     *
     * @param message
     * @methodName: unRegister
     * @return: void
     * @author: weixiansheng
     * @date: 2023/12/26
     **/
    @OnTransition(source = "REGISTERED", target = "UNCONNECTED")
    public void unRegister(Message<RegEventEnum> message) {
        log.info("///////////////////");
        log.info("注销事件, 已注册 -> 未连接");
        log.info("///////////////////");
    }
}
