package com.ybw.entity;

import com.ybw.constant.RegStatusEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @ClassName Order
 * @Description:
 * @Author ybwei
 * @Date 2020/3/26
 * @Version V1.0
 **/
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Order {
    private Long id;
    private String name;
    private RegStatusEnum status;
}
