package com.ybw.constant;


/**
 * 状态
 *
 * @author weixiansheng
 * @version V1.0
 * @className RegStatusEnum
 * @date 2023/12/26
 **/
public enum RegStatusEnum {
    // 未连接
    UNCONNECTED,
    // 已连接
    CONNECTED,
    // 注册中
    REGISTERING,
    // 已注册
    REGISTERED;
}
