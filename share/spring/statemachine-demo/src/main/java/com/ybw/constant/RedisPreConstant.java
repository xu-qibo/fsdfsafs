package com.ybw.constant;

/**
 * redis前缀常量
 *
 * @author weixiansheng
 * @version V1.0
 * @className RedisPreConstant
 * @date 2023/12/27
 **/
public interface RedisPreConstant {

    String STATE_MACHINE = "STATE:MACHINE";
}
