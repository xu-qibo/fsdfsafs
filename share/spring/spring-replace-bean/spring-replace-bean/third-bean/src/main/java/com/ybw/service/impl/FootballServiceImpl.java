package com.ybw.service.impl;

import com.ybw.service.SportService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * 足球
 *
 * @program: spring-replace-bean
 * @description:
 * @author: geoffrey
 * @create: 2022-06-18 18:03
 */
@Service
@Slf4j
public class FootballServiceImpl implements SportService {
    @Override
    public void printName() {
        log.info("运动项目名称:足球");
    }
}
