package com.ybw.service;

/**
 * @author ybw
 * @version V1.0
 * @className MyService
 * @date 2022/6/16
 **/
public interface MyService {

    /**
     *
     * @methodName: getProjectName
     * @return: java.lang.String
     * @author: ybw
     * @date: 2022/6/16
     **/
    String getProjectName();
}
