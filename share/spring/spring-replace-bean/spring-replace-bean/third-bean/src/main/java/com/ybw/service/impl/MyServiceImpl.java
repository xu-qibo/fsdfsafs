package com.ybw.service.impl;

import com.ybw.service.MyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author ybw
 * @version V1.0
 * @className MyServiceImpl
 * @date 2022/6/16
 **/
@Service
@Slf4j
public class MyServiceImpl implements MyService {

    @Override
    public String getProjectName() {
        return "third-bean";
    }
}
