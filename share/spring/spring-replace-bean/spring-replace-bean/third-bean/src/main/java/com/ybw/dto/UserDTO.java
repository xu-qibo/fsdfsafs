package com.ybw.dto;

import com.alibaba.fastjson2.JSON;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author ybw
 * @version V1.0
 * @className UserDTO
 * @date 2022/6/16
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDTO {
    protected Long id;
    protected String name;

    /**
     * @methodName: print
     * @return: java.lang.String
     * @author: ybw
     * @date: 2022/6/16
     **/
    public String toJson() {
        return JSON.toJSONString(this);
    }
}
