package com.ybw.config;

import org.springframework.context.annotation.Configuration;

/**
 * @program: spring-replace-bean
 * @description:
 * @author: geoffrey
 * @create: 2022-06-15 23:14
 */
@Configuration
public class MyConfig {
    /**
     * @methodName: getProjectName
     * @return: java.lang.String
     * @author: ybwei
     * @date: 2022/6/15
     **/
    public String getProjectName() {
        return "third-bean";
    }
}
