package com.ybw.service;

public interface ProductService {
    /**
     * 添加商品
     *
     * @methodName: addProduct
     * @return: void
     * @author: ybw
     * @date: 2022/6/17
     **/
    void addProduct();
}
