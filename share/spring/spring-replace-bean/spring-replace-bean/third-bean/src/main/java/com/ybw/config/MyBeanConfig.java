package com.ybw.config;

import com.ybw.dto.UserDTO;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MyBeanConfig {

    @Bean
    @ConditionalOnMissingBean
    public UserDTO userDTO(){
        return new UserDTO(1L,"张三");
    }
}
