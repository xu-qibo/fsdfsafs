package com.ybw.service;

/**
 * @program: spring-replace-bean
 * @description:
 * @author: geoffrey
 * @create: 2022-06-18 18:01
 */
public interface SportService {
    /**
     * 打印运动名称
     *
     * @methodName: printName
     * @return: void
     * @author: ybwei
     * @date: 2022/6/18
     **/
    void printName();
}
