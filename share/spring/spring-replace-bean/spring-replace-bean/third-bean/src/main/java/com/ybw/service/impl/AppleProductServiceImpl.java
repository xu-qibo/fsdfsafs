package com.ybw.service.impl;

import com.ybw.service.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author ybw
 * @version V1.0
 * @className AppleProductServiceImpl
 * @date 2022/6/17
 **/
@Service("productService")
@Slf4j
public class AppleProductServiceImpl implements ProductService {
    @Override
    public void addProduct() {
        log.info("添加苹果");
    }
}
