package com.ybw.service;

public interface UserService {

    /**
     *
     * @methodName: toJson
     * @return: java.lang.String
     * @author: ybw
     * @date: 2022/6/16
     **/
    String toJson();
}
