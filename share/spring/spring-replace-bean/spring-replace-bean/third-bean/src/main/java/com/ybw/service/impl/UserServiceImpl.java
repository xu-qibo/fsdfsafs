package com.ybw.service.impl;

import com.ybw.dto.UserDTO;
import com.ybw.service.UserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class UserServiceImpl implements UserService {
    @Resource
    private UserDTO userDTO;

    @Override
    public String toJson() {
        return userDTO.toJson();
    }
}
