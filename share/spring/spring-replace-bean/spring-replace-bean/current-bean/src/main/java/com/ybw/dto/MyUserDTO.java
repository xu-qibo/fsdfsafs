package com.ybw.dto;

import com.alibaba.fastjson2.JSON;
import lombok.Data;
import org.springframework.stereotype.Component;

/**
 * @author ybw
 * @version V1.0
 * @className MyUserDTO
 * @date 2022/6/16
 **/
@Component
@Data
public class MyUserDTO extends UserDTO {

    private Long no;

    @Override
    public String toJson() {
        super.id = 2L;
        super.name = "李四";
        this.no = 50L;
        return JSON.toJSONString(this);
    }
}
