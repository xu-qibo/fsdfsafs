package com.ybw.service.impl;

import com.ybw.service.ProductService;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.PostConstruct;

@Slf4j
public class BananaProductServiceImpl implements ProductService {

    @PostConstruct
    public void init() {
        log.info("我替换了默认实现");
    }

    @Override
    public void addProduct() {
        log.info("添加香蕉");
    }
}
