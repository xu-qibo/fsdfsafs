package com.ybw.controller;

import com.ybw.dto.UserDTO;
import com.ybw.service.MyService;
import com.ybw.service.ProductService;
import com.ybw.service.SportService;
import com.ybw.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @program: spring-replace-bean
 * @description:
 * @author: geoffrey
 * @create: 2022-06-15 23:20
 */
@RestController
@Slf4j
public class TestController {
//    @Resource
//    private MyConfig myConfig;


//    @GetMapping("/getProjectName")
//    public String getProjectName() {
//        return myConfig.getProjectName();
//    }

    @Resource
    private MyService myService;

    @GetMapping("/getProjectName")
    public String getProjectName() {
        return myService.getProjectName();
    }

    @Resource
    private UserService userService;

    /**
     * @methodName: getUser
     * @return: java.lang.String
     * @author: ybw
     * @date: 2022/6/16
     **/
    @GetMapping("/getUser")
    public String getUser() {
        return userService.toJson();
    }


    @Resource
    private ProductService productService;

    /**
     *
     * @methodName: getUser2
     * @return: java.lang.String
     * @author: ybw
     * @date: 2022/6/16
     **/
    @GetMapping("/addProduct")
    public String addProduct() {
        productService.addProduct();
        return "OK";
    }


    @Resource
    private SportService sportService;

    /**
     * @methodName: printName
     * @return: java.lang.String
     * @author: ybwei
     * @date: 2022/6/18
     **/
    @GetMapping("/printName")
    public String printName() {
        sportService.printName();
        return "OK";
    }

}
