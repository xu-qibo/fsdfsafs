package com.ybw.service.impl;

import com.ybw.service.MyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class MyTestServiceImpl implements MyService {

    @Override
    public String getProjectName() {
        return "current-bean";
    }
}
