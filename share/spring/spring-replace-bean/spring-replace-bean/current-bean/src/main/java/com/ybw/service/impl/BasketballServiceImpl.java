package com.ybw.service.impl;

import com.ybw.service.SportService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

/**
 * @program: spring-replace-bean
 * @description:
 * @author: geoffrey
 * @create: 2022-06-18 18:05
 */
@Service
@Slf4j
@Primary
public class BasketballServiceImpl implements SportService {
    @Override
    public void printName() {
        log.info("运动项目名称:篮球");
    }
}
