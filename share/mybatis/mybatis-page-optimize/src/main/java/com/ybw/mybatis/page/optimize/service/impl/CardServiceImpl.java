package com.ybw.mybatis.page.optimize.service.impl;

import com.ybw.mybatis.page.optimize.entity.Card;
import com.ybw.mybatis.page.optimize.mapper.CardMapper;
import com.ybw.mybatis.page.optimize.service.CardService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 卡 服务实现类
 * </p>
 *
 * @author ybw
 * @since 2021-12-25
 */
@Service
public class CardServiceImpl extends ServiceImpl<CardMapper, Card> implements CardService {

}
