package com.ybw.mybatis.page.optimize.service;

import com.ybw.mybatis.page.optimize.entity.OrderPay;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 订单支付 服务类
 * </p>
 *
 * @author ybw
 * @since 2021-12-25
 */
public interface OrderPayService extends IService<OrderPay> {

}
