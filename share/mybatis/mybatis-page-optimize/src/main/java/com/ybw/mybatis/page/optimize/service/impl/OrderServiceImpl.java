package com.ybw.mybatis.page.optimize.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ybw.mybatis.page.optimize.dto.Order3DTO;
import com.ybw.mybatis.page.optimize.dto.OrderDTO;
import com.ybw.mybatis.page.optimize.entity.Order;
import com.ybw.mybatis.page.optimize.entity.User;
import com.ybw.mybatis.page.optimize.mapper.OrderMapper;
import com.ybw.mybatis.page.optimize.service.OrderService;
import com.ybw.mybatis.page.optimize.service.UserService;
import com.ybw.mybatis.page.optimize.utils.StringUtils;
import com.ybw.mybatis.page.optimize.vo.req.Order3Req;
import com.ybw.mybatis.page.optimize.vo.req.OrderReq;
import com.ybw.mybatis.page.optimize.vo.res.OrderRes;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * <p>
 * 订单 服务实现类
 * </p>
 *
 * @author ybw
 * @since 2021-12-25
 */
@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order> implements OrderService {
    @Resource
    private OrderMapper orderMapper;
    @Resource
    private UserService userService;

    @Override
    public List<OrderRes> getOrder1List(OrderReq orderReq) {
        //参数一是当前页，参数二是每页个数
        IPage<OrderRes> page = new Page<OrderRes>(orderReq.getPageNum(), orderReq.getPageSize());
        return orderMapper.getOrder1List(page, orderReq);
    }

    @Override
    public List<OrderRes> getOrder2List(OrderReq orderReq) {
        //1、获取userIdList
        List<Long> userIdList = getUserIdList(orderReq.getName());
        OrderDTO orderDTO = new OrderDTO(orderReq, userIdList);
        //2、分页查询(三张表内)
        //参数一是当前页，参数二是每页个数
        IPage<OrderRes> page = new Page<OrderRes>(orderReq.getPageNum(), orderReq.getPageSize());
        List<OrderRes> orderResList = orderMapper.getOrder2List(page, orderDTO);
        //3、补充用户信息
        List<User> userList = getUserByUserId(orderResList);
        orderResList.forEach(orderRes -> {
            if (orderRes.getUserId() == null) {
                return;
            }
            Optional<User> optionalUser = userList.stream().filter(user -> user.getId().equals(orderRes.getUserId())).findFirst();
            if (!optionalUser.isPresent()) {
                return;
            }
            orderRes.setUserName(optionalUser.get().getName());
        });
        return orderResList;
    }



    /**
     * @return java.util.List<com.ybw.mybatis.page.optimize.entity.User>
     * @throws
     * @description
     * @author ybwei
     * @date 2022/1/7 17:16
     * @params [orderResList]
     */
    private List<User> getUserByUserId(List<OrderRes> orderResList) {
        if (CollectionUtils.isEmpty(orderResList)) {
            return new ArrayList<>();
        }
        List<Long> userIdList = orderResList.stream().map(OrderRes::getUserId).collect(Collectors.toList());
        if (CollectionUtils.isEmpty(userIdList)) {
            return new ArrayList<>();
        }
        return userService.lambdaQuery()
                .in(User::getId, userIdList)
                .list();
    }

    /**
     * @return java.util.List<java.lang.Long>
     * @throws
     * @description
     * @author ybwei
     * @date 2022/1/7 15:58
     * @params [name]
     */
    private List<Long> getUserIdList(String name) {
        if (StringUtils.isBlank(name)) {
            return new ArrayList<>();
        }
        List<User> userList = userService.lambdaQuery()
                .eq(User::getName, name)
                .list();
        if (CollectionUtils.isEmpty(userList)) {
            return new ArrayList<>();
        }
        return userList.stream().map(User::getId).collect(Collectors.toList());
    }

    @Override
    public List<OrderRes> getOrder3List(Order3Req order3Req) {
        //1、获取userIdList
        List<Long> userIdList = getUserIdList(order3Req.getName());
        Order3DTO order3DTO = new Order3DTO(order3Req, userIdList);
        //2、分页查询(三张表内)
        List<OrderRes> orderResList = orderMapper.getOrder3List(order3DTO);
        //3、补充用户信息
        List<User> userList = getUserByUserId(orderResList);
        orderResList.forEach(orderRes -> {
            if (orderRes.getUserId() == null) {
                return;
            }
            Optional<User> optionalUser = userList.stream().filter(user -> user.getId().equals(orderRes.getUserId())).findFirst();
            if (!optionalUser.isPresent()) {
                return;
            }
            orderRes.setUserName(optionalUser.get().getName());
        });
        return orderResList;
    }
}
