package com.ybw.mybatis.page.optimize.vo.req;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @program: mybatis-page-optimize
 * @description:
 * @author: geoffrey
 * @create: 2021-12-25 18:54
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class OrderReq {

    /**
     * 订单号
     * @Author: geoffrey
     * @Date: 2021/12/26
     **/
    private String orderNo;
    /**
     * user的name
     * @Author: geoffrey
     * @Date: 2021/12/26
     **/
    private String name;
    /**
     * card的id
     * @Author: geoffrey
     * @Date: 2021/12/26
     **/
    private Long cardId;
    /**
     * 页码
     * @Author: geoffrey
     * @Date: 2021/12/25
     **/
    private Long pageNum;
    /**
     * 每页数量
     * @Author: geoffrey
     * @Date: 2021/12/25
     **/
    private Long pageSize;
}
