package com.ybw.mybatis.page.optimize.service;

import com.ybw.mybatis.page.optimize.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户 服务类
 * </p>
 *
 * @author ybw
 * @since 2021-12-25
 */
public interface UserService extends IService<User> {

}
