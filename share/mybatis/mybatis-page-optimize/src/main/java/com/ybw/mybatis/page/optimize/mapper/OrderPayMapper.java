package com.ybw.mybatis.page.optimize.mapper;

import com.ybw.mybatis.page.optimize.entity.OrderPay;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单支付 Mapper 接口
 * </p>
 *
 * @author ybw
 * @since 2021-12-25
 */
public interface OrderPayMapper extends BaseMapper<OrderPay> {

}
