package com.ybw.mybatis.page.optimize.service.impl;

import com.ybw.mybatis.page.optimize.entity.OrderPay;
import com.ybw.mybatis.page.optimize.mapper.OrderPayMapper;
import com.ybw.mybatis.page.optimize.service.OrderPayService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单支付 服务实现类
 * </p>
 *
 * @author ybw
 * @since 2021-12-25
 */
@Service
public class OrderPayServiceImpl extends ServiceImpl<OrderPayMapper, OrderPay> implements OrderPayService {

}
