package com.ybw.mybatis.page.optimize.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.ybw.mybatis.page.optimize.dto.Order3DTO;
import com.ybw.mybatis.page.optimize.dto.OrderDTO;
import com.ybw.mybatis.page.optimize.entity.Order;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ybw.mybatis.page.optimize.vo.req.OrderReq;
import com.ybw.mybatis.page.optimize.vo.res.OrderRes;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 订单 Mapper 接口
 * </p>
 *
 * @author ybw
 * @since 2021-12-25
 */
public interface OrderMapper extends BaseMapper<Order> {

    /**
     * @MethodName: getOrder1List
     * @Description: 第一版-查询订单列表
     * 分页对象,xml中可以从里面进行取值,传递参数 Page 即自动分页,必须放在第一位(你可以继承Page实现自己的分页对象)
     * @Param: [page, orderReq]
     * @Return: java.util.List<com.ybw.mybatis.page.optimize.vo.res.OrderRes>
     * @Author: geoffrey
     * @Date: 2021/12/25
     **/
    List<OrderRes> getOrder1List(IPage<OrderRes> page, @Param("orderReq") OrderReq orderReq);
    /**
     * @return java.util.List<com.ybw.mybatis.page.optimize.vo.res.OrderRes>
     * @throws 
     * @description 
     * @author ybwei
     * @date 2022/1/7 17:07
     * @params [page, orderDTO]
     */
    List<OrderRes> getOrder2List(@Param("page") IPage<OrderRes> page, @Param("orderDTO") OrderDTO orderDTO);

    /**
     * @MethodName: getOrder3List
     * @Description:  
     * @Param: [order3DTO]
     * @Return: java.util.List<com.ybw.mybatis.page.optimize.vo.res.OrderRes>
     * @Author: geoffrey
     * @Date: 2022/1/11
     **/
    List<OrderRes> getOrder3List(@Param("order3DTO") Order3DTO order3DTO);
}
