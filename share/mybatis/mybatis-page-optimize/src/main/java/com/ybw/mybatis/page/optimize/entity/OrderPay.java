package com.ybw.mybatis.page.optimize.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 订单支付
 * </p>
 *
 * @author ybw
 * @since 2021-12-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("t_order_pay")
public class OrderPay implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * order的id
     */
    private Long orderId;

    /**
     * card的id
     */
    private Long cardId;

    /**
     * 支付金额
     */
    private BigDecimal money;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;


}
