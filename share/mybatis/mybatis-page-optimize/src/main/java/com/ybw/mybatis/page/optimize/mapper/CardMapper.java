package com.ybw.mybatis.page.optimize.mapper;

import com.ybw.mybatis.page.optimize.entity.Card;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 卡 Mapper 接口
 * </p>
 *
 * @author ybw
 * @since 2021-12-25
 */
public interface CardMapper extends BaseMapper<Card> {

}
