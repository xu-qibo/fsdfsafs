package com.ybw.mybatis.page.optimize.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ybw.mybatis.page.optimize.entity.Order;
import com.ybw.mybatis.page.optimize.vo.req.Order3Req;
import com.ybw.mybatis.page.optimize.vo.req.OrderReq;
import com.ybw.mybatis.page.optimize.vo.res.OrderRes;

import java.util.List;

/**
 * <p>
 * 订单 服务类
 * </p>
 *
 * @author ybw
 * @since 2021-12-25
 */
public interface OrderService extends IService<Order> {

    /**
     * @MethodName: getOrder1List
     * @Description: 第一版-查询订单列表
     * @Param: [orderReq]
     * @Return: java.util.List<com.ybw.mybatis.page.optimize.entity.Order>
     * @Author: geoffrey
     * @Date: 2021/12/25
     */
    List<OrderRes> getOrder1List(OrderReq orderReq);

    /**
     * @MethodName: getOrder2List
     * @Description: 第二版（优化版）-查询订单列表
     * @Param: [orderReq]
     * @Return: java.util.List<com.ybw.mybatis.page.optimize.vo.res.OrderRes>
     * @Author: geoffrey
     * @Date: 2021/12/27
     **/
    List<OrderRes> getOrder2List(OrderReq orderReq);

    /**
     * @MethodName: getOrder3List
     * @Description:  
     * @Param: [order3Req]
     * @Return: java.util.List<com.ybw.mybatis.page.optimize.vo.res.OrderRes>
     * @Author: geoffrey
     * @Date: 2022/1/11
     **/
    List<OrderRes> getOrder3List(Order3Req order3Req);
}
