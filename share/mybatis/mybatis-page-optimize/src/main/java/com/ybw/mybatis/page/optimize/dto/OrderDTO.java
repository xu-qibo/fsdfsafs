package com.ybw.mybatis.page.optimize.dto;

import com.ybw.mybatis.page.optimize.vo.req.OrderReq;
import lombok.Data;
import org.springframework.beans.BeanUtils;

import java.util.List;

/**
 * @author ybwei
 * @description
 * @date 2022/1/7 17:04
 **/
@Data
public class OrderDTO extends OrderReq {
    private List<Long> userIdList;

    /**
     * @return
     * @throws
     * @description
     * @author ybwei
     * @date 2022/1/7 17:05
     * @params [orderReq, userIdList]
     */
    public OrderDTO(OrderReq orderReq, List<Long> userIdList) {
        BeanUtils.copyProperties(orderReq, this);
        this.userIdList = userIdList;
    }

}
