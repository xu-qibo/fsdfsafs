package com.ybw.mybatis.page.optimize.mapper;

import com.ybw.mybatis.page.optimize.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户 Mapper 接口
 * </p>
 *
 * @author ybw
 * @since 2021-12-25
 */
public interface UserMapper extends BaseMapper<User> {

}
