package com.ybw.mybatis.page.optimize;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MybatisPageOptimizeApplication {

    public static void main(String[] args) {
        SpringApplication.run(MybatisPageOptimizeApplication.class, args);
    }

}
