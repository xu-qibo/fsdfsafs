package com.ybw.mybatis.page.optimize.service.impl;

import com.ybw.mybatis.page.optimize.entity.User;
import com.ybw.mybatis.page.optimize.mapper.UserMapper;
import com.ybw.mybatis.page.optimize.service.UserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户 服务实现类
 * </p>
 *
 * @author ybw
 * @since 2021-12-25
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

}
