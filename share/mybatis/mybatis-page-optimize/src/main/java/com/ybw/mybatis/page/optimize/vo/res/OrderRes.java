package com.ybw.mybatis.page.optimize.vo.res;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @program: mybatis-page-optimize
 * @description:
 * @author: geoffrey
 * @create: 2021-12-25 18:54
 */
@Data
public class OrderRes {

    private String orderNo;
    private BigDecimal money;
    /**
     * 创建时间
     */
    private LocalDateTime createTime;
    private BigDecimal opMoney;
    private String cardName;
    private String userName;
    private Long userId;
}
