package com.ybw.mybatis.page.optimize.service;

import com.ybw.mybatis.page.optimize.entity.Card;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 卡 服务类
 * </p>
 *
 * @author ybw
 * @since 2021-12-25
 */
public interface CardService extends IService<Card> {

}
