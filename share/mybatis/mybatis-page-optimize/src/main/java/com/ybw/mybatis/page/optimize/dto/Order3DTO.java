package com.ybw.mybatis.page.optimize.dto;

import com.ybw.mybatis.page.optimize.vo.req.Order3Req;
import lombok.Data;
import org.springframework.beans.BeanUtils;

import java.util.List;

/**
 * @author ybwei
 * @description
 * @date 2022/1/7 17:04
 **/
@Data
public class Order3DTO extends Order3Req {
    private List<Long> userIdList;

    /**
     * @MethodName: OrderDTO
     * @Description:
     * @Param: [order3Req, userIdList]
     * @Return:
     * @Author: geoffrey
     * @Date: 2022/1/11
     **/
    public Order3DTO(Order3Req order3Req, List<Long> userIdList) {
        BeanUtils.copyProperties(order3Req, this);
        this.userIdList = userIdList;
    }
}
