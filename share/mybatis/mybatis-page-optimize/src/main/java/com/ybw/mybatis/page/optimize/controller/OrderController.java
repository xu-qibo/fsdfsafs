package com.ybw.mybatis.page.optimize.controller;

import com.ybw.mybatis.page.optimize.service.OrderService;
import com.ybw.mybatis.page.optimize.vo.req.Order3Req;
import com.ybw.mybatis.page.optimize.vo.req.OrderReq;
import com.ybw.mybatis.page.optimize.vo.res.OrderRes;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * 订单
 *
 * @module 归属项目
 * @program: mybatis-page-optimize
 * @description: 订单
 * @author: geoffrey
 * @create: 2021-12-25 18:51
 */
@RestController
@RequestMapping("/order")
public class OrderController {

    @Resource
    private OrderService orderService;


    /**
     * 第一版-查询订单列表
     * @MethodName: getOrderList
     * @Param: []
     * @Return: java.util.List<com.ybw.mybatis.page.optimize.entity.Order>
     * @Author: geoffrey
     * @Date: 2021/12/25
     **/
    @GetMapping("/getOrder1List")
    public List<OrderRes> getOrder1List(OrderReq orderReq){
        return orderService.getOrder1List(orderReq);
    }

    /**
     * 第二版（优化版）-查询订单列表
     * @MethodName: getOrderList
     * @Param: []
     * @Return: java.util.List<com.ybw.mybatis.page.optimize.entity.Order>
     * @Author: geoffrey
     * @Date: 2021/12/25
     **/
    @GetMapping("/getOrder2List")
    public List<OrderRes> getOrder2List(OrderReq orderReq){
        return orderService.getOrder2List(orderReq);
    }

    /**
     * 第三版（优化版）-查询订单列表
     * @MethodName: getOrder2List
     * @Param: [orderReq]
     * @Return: java.util.List<com.ybw.mybatis.page.optimize.vo.res.OrderRes>
     * @Author: geoffrey
     * @Date: 2022/1/11
     **/
    @GetMapping("/getOrder3List")
    public List<OrderRes> getOrder3List(Order3Req order3Req){
        return orderService.getOrder3List(order3Req);
    }
}
