package com.ybw.mybatis.page.optimize.utils;

import java.util.UUID;

/**
 * @program: mybatis-page-optimize
 * @description:
 * @author: geoffrey
 * @create: 2021-12-25 17:38
 */
public class StringUtils extends org.apache.commons.lang3.StringUtils {


    /**
     * @MethodName: generateUUID
     * @Description:
     * @Param: []
     * @Return: java.lang.String
     * @Author: geoffrey
     * @Date: 2021/12/25
     **/
    public static String generateUUID() {
        return UUID.randomUUID().toString().replace("-", "");
    }
}
