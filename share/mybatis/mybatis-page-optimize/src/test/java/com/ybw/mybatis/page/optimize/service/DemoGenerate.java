package com.ybw.mybatis.page.optimize.service;

import com.ybw.mybatis.page.optimize.entity.Card;
import com.ybw.mybatis.page.optimize.entity.Order;
import com.ybw.mybatis.page.optimize.entity.OrderPay;
import com.ybw.mybatis.page.optimize.entity.User;
import com.ybw.mybatis.page.optimize.utils.StringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.math.BigDecimal;

/**
 * @program: mybatis-page-optimize
 * @description: 生成测试数据
 * @author: geoffrey
 * @create: 2021-12-25 17:43
 */
@SpringBootTest
public class DemoGenerate {

    @Resource
    private UserService userService;
    @Resource
    private CardService cardService;
    @Resource
    private OrderService orderService;
    @Resource
    private OrderPayService orderPayService;

    /**
     * @MethodName: generate
     * @Description: 生成测试数据
     * @Param: []
     * @Return: void
     * @Author: geoffrey
     * @Date: 2021/12/25
     **/
    @Test
    public void generate() {
        for (int i = 0; i < 1000; i++) {
            User user = new User();
            user.setName("张三" + i);
            userService.save(user);

            Card card = new Card();
            card.setUserId(user.getId());
            card.setName("卡" + i);
            cardService.save(card);

            Order order = new Order();
            order.setMoney(BigDecimal.ZERO.add(new BigDecimal(i)));
            order.setOrderNo(StringUtils.generateUUID());
            orderService.save(order);

            OrderPay orderPay = new OrderPay();
            orderPay.setCardId(card.getId());
            orderPay.setOrderId(order.getId());
            orderPay.setMoney(order.getMoney());
            orderPayService.save(orderPay);

        }
    }

}
