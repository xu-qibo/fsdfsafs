package com.ybw.mybatis.page.optimize.service;

import com.ybw.mybatis.page.optimize.utils.GsonUtils;
import com.ybw.mybatis.page.optimize.vo.req.Order3Req;
import com.ybw.mybatis.page.optimize.vo.req.OrderReq;
import com.ybw.mybatis.page.optimize.vo.res.OrderRes;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.List;

@SpringBootTest
@Slf4j
class OrderServiceTest {

    @Resource
    private OrderService orderService;

    /**
     * 一版分页（普通分页）
     * @methodName: getOrder1List
     * @return: void
     * @author: geoffrey
     * @date: 2022/2/7
     **/
    @Test
    public void getOrder1List() {
        OrderReq orderReq = OrderReq
                .builder()
                .orderNo("a48c853c76414ea09667404a5be91bb9")
                .cardId(1L)
                .name("张三0")
                .pageNum(1L)
                .pageSize(20L)
                .build();
        List<OrderRes> orderResList = orderService.getOrder1List(orderReq);
        log.info("orderResList:{}", GsonUtils.gsonString(orderResList));
    }


    /**
     * 二版分页-SQL优化（通过id分页）
     * @methodName: getOrder2List
     * @return: void
     * @author: geoffrey
     * @date: 2022/2/7
     **/
    @Test
    public void getOrder2List() {
        OrderReq orderReq = OrderReq
                .builder()
                .orderNo("a48c853c76414ea09667404a5be91bb9")
                .cardId(1L)
                .name("张三0")
                .pageNum(1L)
                .pageSize(20L)
                .build();
        List<OrderRes> orderResList = orderService.getOrder2List(orderReq);
        log.info("orderResList:{}", GsonUtils.gsonString(orderResList));
    }


    /**
     * @MethodName: getOrder3List
     * @Description:  
     * @Param: []
     * @Return: void
     * @Author: geoffrey
     * @Date: 2022/1/12
     **/
    @Test
    public void getOrder3List() {
        Order3Req orderReq = Order3Req
                .builder()
                .preOrderId(9L)
                .pageSize(5L)
                .build();
        List<OrderRes> orderResList = orderService.getOrder3List(orderReq);
        log.info("orderResList:{}", GsonUtils.gsonString(orderResList));
    }

}