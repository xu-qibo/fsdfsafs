# 工程简介
分页SQL<br/>
`SELECT
 	o.order_no,
 	o.money,
 	o.create_time,
 	op.money op_money,
 	c.`name` card_name,
 	u.`name` user_name 
 FROM
 	t_order o
 	LEFT JOIN t_order_pay op ON o.id = op.order_id
 	LEFT JOIN t_card c ON c.id = op.card_id
 	LEFT JOIN t_user u ON u.id = c.user_id 
 	LIMIT 0,
 	10;`



# 延伸阅读

