package com.ybw.rocketmq.demo.mq;

import com.alibaba.fastjson.JSON;
import com.ybw.rocketmq.demo.dto.OrderStep;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author ybwei
 * @date 2022/3/8 17:30
 **/
@SpringBootTest
@Slf4j
class SenderTest {
    @Resource
    private Sender sender;

    /**
     * 生产者顺序：创建、付款、推送、完成
     * 消费者顺序：创建、付款、推送、完成
     *
     * @return void
     * @throws
     * @methodName: syncSendOrderly
     * @author ybwei
     * @date 2022/3/8 17:31
     */
    @Test
    public void syncSendOrderly() throws InterruptedException {
        // 订单列表
        List<OrderStep> orderStepList = buildOrders();
        log.info("orderStepList:{}", JSON.toJSONString(orderStepList));
        orderStepList.forEach(orderStep -> {
            sender.syncSendOrderly(orderStep);
        });
        TimeUnit.DAYS.sleep(1);
    }

    /**
     * 生成模拟订单数据
     */
    private List<OrderStep> buildOrders() {
        List<OrderStep> orderList = new ArrayList<OrderStep>();

        OrderStep orderDemo = new OrderStep();
        orderDemo.setOrderId(15103111039L);
        orderDemo.setDesc("创建");
        orderList.add(orderDemo);

        orderDemo = new OrderStep();
        orderDemo.setOrderId(15103111039L);
        orderDemo.setDesc("付款");
        orderList.add(orderDemo);


        orderDemo = new OrderStep();
        orderDemo.setOrderId(15103111039L);
        orderDemo.setDesc("推送");
        orderList.add(orderDemo);

        orderDemo = new OrderStep();
        orderDemo.setOrderId(15103111039L);
        orderDemo.setDesc("完成");
        orderList.add(orderDemo);

        return orderList;
    }
}