package com.ybw.rocketmq.demo.dto;

import lombok.Data;

/**
 * 订单的步骤
 *
 * @author ybwei
 * @date 2022/3/8 17:01
 **/
@Data
public class OrderStep {
    /**
     * 订单id
     *
     * @author: ybwei
     * @date: 2022/3/8 17:15
     */
    private Long orderId;
    /**
     * 描述
     *
     * @author: ybwei
     * @date: 2022/3/8 17:15
     */
    private String desc;
}