package com.ybw.rocketmq.demo.constant;

/**
 * @program: rocketmq-demo
 * @description:
 * @author: geoffrey
 * @create: 2021-10-08 22:57
 */
public interface MqConstant {

    /**
     * @Description: topic
     * @Author: geoffrey
     * @Date: 2021/10/8
     **/
    String TOPIC="test-topic-1";

    /**
     * @Description: 
     * @Author: geoffrey
     * @Date: 2021/10/8
     **/
    String CONSUMER_GROUP="my-consumer_test-topic-1";


    /**
     * @Description 生产者-分组
     * @author: ybwei
     * @Date: 2021/10/9 13:54
     */
    String PRODUCER_GROUP ="test_group";
}
