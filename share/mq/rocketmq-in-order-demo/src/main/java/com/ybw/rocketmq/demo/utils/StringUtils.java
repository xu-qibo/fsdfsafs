package com.ybw.rocketmq.demo.utils;

import java.util.UUID;

/**
 * @author ybwei
 * @Title: StringUtils.java
 * @ProjectName com.spring.pro.rocketmq
 * @Description:
 * @date 2019年10月23日 下午4:43:42
 */
public class StringUtils extends org.apache.commons.lang3.StringUtils {

	/**
	 * @return
	 * @Description:生成uuid
	 * @Author: ybwei
	 * @Date: 2019年10月23日 下午4:44:54
	 */
	public static String generateUUID() {
		return UUID.randomUUID().toString().replace("-", "");
	}
}
