package com.ybw.spi.service;


/**
 * Demo service. Implements should be use SPI.
 *
 * @author ybw
 * @version V1.0
 * @className DemoService
 * @date 2022/6/29
 **/
public interface DemoService {

    String sayHello();

}