package com.ybw.spi.service.impl;


import com.ybw.spi.service.DemoService;

/**
 * Implement for DemoService
 *
 * @author ybw
 * @version V1.0
 * @className DemoOneServiceImpl
 * @date 2022/6/29
 **/
public class DemoOneServiceImpl implements DemoService {

    @Override
    public String sayHello() {
        return "hello world one";
    }
}