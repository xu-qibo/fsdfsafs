package com.ybw.spi.test;

import com.ybw.spi.service.DemoService;
import com.ybw.spi.service.impl.DemoOneServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.ServiceLoader;

/**
 * 加载使用
 *
 * @author ybw
 * @version V1.0
 * @className SpiTest
 * @date 2022/6/29
 **/
@Slf4j
public class SpiTest {


    /**
     * @methodName: spiTest
     * @return: void
     * @author: ybw
     * @date: 2022/6/29
     **/
    @Test
    public void spiTest() {
        ServiceLoader<DemoService> demoServices = ServiceLoader.load(DemoService.class);
        demoServices.forEach(demoService -> {
            log.info(demoService.getClass().getName());
            log.info("ClassLoader:{}",demoService.getClass().getClassLoader());
            log.info(demoService.sayHello());
        });
    }
}