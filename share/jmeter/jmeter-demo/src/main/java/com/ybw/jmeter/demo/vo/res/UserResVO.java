package com.ybw.jmeter.demo.vo.res;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author ybwei
 * @date 2022/3/2 18:20
 **/
@Data
@AllArgsConstructor
public class UserResVO {
    private Long id;
    private String name;
    private String uuid;
}
