package com.ybw.jmeter.demo.vo.req;

import lombok.Data;

/**
 * @author ybwei
 * @date 2022/3/2 18:13
 **/
@Data
public class UserReqVO {
    private String uuid;
}
