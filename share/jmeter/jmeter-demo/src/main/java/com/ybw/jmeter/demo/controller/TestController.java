package com.ybw.jmeter.demo.controller;

import com.alibaba.fastjson.JSON;
import com.ybw.jmeter.demo.vo.req.UserReqVO;
import com.ybw.jmeter.demo.vo.res.PreDataResVO;
import com.ybw.jmeter.demo.vo.res.UserResVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

/**
 * @author ybwei
 * @date 2022/3/2 18:02
 **/
@RestController
@Slf4j
@RequestMapping("/test")
public class TestController {

    /**
     * 获取准备的数据
     *
     * @return java.lang.String
     * @throws
     * @methodName: getPreData
     * @author ybwei
     * @date 2022/3/2 18:03
     */
    @GetMapping("/getPreData")
    public PreDataResVO getPreData() {
        log.info("getPreData");
        return new PreDataResVO(UUID.randomUUID().toString(),10);
    }

    /**
     * 数据处理
     *
     * @param userReqVO
     * @return com.ybw.jmeter.demo.vo.res.UserResVO
     * @throws
     * @methodName: handle
     * @author ybwei
     * @date 2022/3/2 18:21
     */
    @PostMapping("/handle")
    public UserResVO handle(@RequestBody UserReqVO userReqVO) {
        log.info("userReqVO:{}", JSON.toJSONString(userReqVO));
        return new UserResVO(1L, "张SN", userReqVO.getUuid());
    }
}
