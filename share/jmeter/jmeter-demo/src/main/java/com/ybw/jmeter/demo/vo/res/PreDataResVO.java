package com.ybw.jmeter.demo.vo.res;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @program: jmeter-demo
 * @description:
 * @author: geoffrey
 * @create: 2022-03-02 23:18
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PreDataResVO {
    private String uuid;
    private Integer no;
}
