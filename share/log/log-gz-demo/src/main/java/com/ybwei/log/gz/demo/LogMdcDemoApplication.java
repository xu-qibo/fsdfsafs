package com.ybwei.log.gz.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @Description https://mp.weixin.qq.com/s/LVUeV6CujfE5wbeYeirMIg
 * @author ybwei
 * @date 2021/4/9 14:24
 **/
@SpringBootApplication
@EnableScheduling
public class LogMdcDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(LogMdcDemoApplication.class, args);
    }

}
