package com.ybwei.log.gz.demo.test;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

/**
 * @program: logback-gz-demo
 * @description:
 * @author: geoffrey
 * @create: 2022-08-06 16:06
 */
@Slf4j
public class LogTest {

    /**
     * @methodName: print
     * @return: void
     * @author: ybwei
     * @date: 2022/8/6
     **/
    @Test
    public void print() {
        log.info("测试");
        try {
            int a=100/0;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
