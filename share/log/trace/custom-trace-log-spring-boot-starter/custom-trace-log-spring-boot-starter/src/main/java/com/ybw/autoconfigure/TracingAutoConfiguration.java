// src/main/java/com/example/tracing/autoconfigure/TracingAutoConfiguration.java
package com.ybw.autoconfigure;

import com.ybw.config.TraceIdProperties;
import com.ybw.interceptor.TraceIdInterceptor;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;

/**
 * 配置类，用于条件性地自动配置Spring MVC应用程序的追踪功能。
 *
 * @author ybw
 * @version V1.0
 * @className TracingAutoConfiguration
 * @date 2024/4/13
 **/
@Configuration
@ConditionalOnClass(WebMvcConfigurer.class)
@ConditionalOnProperty(name = "tracing.enabled", havingValue = "true", matchIfMissing = true)
@EnableConfigurationProperties(TraceIdProperties.class)
@AutoConfigureAfter(WebMvcAutoConfiguration.class)
// 组件扫描，扫描的包为"com.ybw"
@ComponentScan("com.ybw")
public class TracingAutoConfiguration implements WebMvcConfigurer {

    @Resource
    private TraceIdInterceptor traceIdInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(traceIdInterceptor);
    }
}