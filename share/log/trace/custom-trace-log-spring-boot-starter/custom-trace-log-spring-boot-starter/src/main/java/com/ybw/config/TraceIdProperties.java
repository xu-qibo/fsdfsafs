// src/main/java/com/example/tracing/config/TraceIdProperties.java
package com.ybw.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @className TraceIdProperties
 * @author ybw
 * @date 2024/4/13 
 * @version V1.0
 **/
@ConfigurationProperties(prefix = "tracing")
@Data
public class TraceIdProperties {
    /**
     * 请求头、日志名称
     *
     * @author: ybw
     * @date: 2024/4/13
     **/
    private String headerName = "trace-id";
    /**
     * 是否启用
     *
     * @author: ybw
     * @date: 2024/4/13
     **/
    private Boolean enabled = true;


}