package com.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//@SpringBootApplication(scanBasePackages = {"com.test","com.ybw"})
@SpringBootApplication
public class TraceDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(TraceDemoApplication.class, args);
    }

}
