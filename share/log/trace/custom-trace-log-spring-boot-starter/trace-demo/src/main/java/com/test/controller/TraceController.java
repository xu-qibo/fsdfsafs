package com.test.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 链路跟踪
 *
 * @author ybw
 * @version V1.0
 * @className TraceController
 * @date 2024/4/13
 **/
@RestController
@Slf4j
public class TraceController {

    /**
     * 链路跟踪
     *
     * @methodName: trace
     * @return: java.lang.String
     * @author: ybw
     * @date: 2024/4/13
     **/
    @GetMapping("/trace")
    public String trace() {
        log.info("aaa");
        return "trace";
    }
}
