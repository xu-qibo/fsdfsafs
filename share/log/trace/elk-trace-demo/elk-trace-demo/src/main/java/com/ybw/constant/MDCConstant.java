package com.ybw.constant;

/**
 * @program: logback-mdc-demo
 * @description:
 * @author: geoffrey
 * @create: 2022-05-09 15:26
 */
public class MDCConstant {
    /**
     * 日志id {@value}
     */
    /**
     * REQUEST_ID
     * @author: geoffrey
     * @date: 2022/5/9
     **/
    public static final String REQUEST_ID = "x-request-Id";
}
