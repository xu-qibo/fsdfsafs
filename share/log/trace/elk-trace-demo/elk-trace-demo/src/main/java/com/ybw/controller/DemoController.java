package com.ybw.controller;

import com.ybw.vo.base.ApiResult;
import com.ybw.vo.base.ApiResultGenerator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * 链路跟踪
 *
 * @author ybw
 * @version V1.0
 * @className DemoController
 * @date 2024/4/13
 **/
@RestController
@Slf4j
public class DemoController {

    @GetMapping("/hi")
    public ApiResult hi() {
        log.info("hi");
        return ApiResultGenerator.successResult(new Date());
    }

    @GetMapping("/hello")
    public Object hello() {
        log.info("hello");
        return "hello";
    }

}