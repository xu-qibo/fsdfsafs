package com.ybw.vo.base;

import lombok.Data;

import java.io.Serializable;

/**
 * @author weiyb
 * @ClassName: ApiResult
 * @Description:
 * @date 2017年11月16日 下午2:16:50
 */
@Data
public class ApiResult implements Serializable {

    private static final long serialVersionUID = -3307093826251631665L;
    /**
     * 消息提示
     *
     * @author weiyb
     */
    private String msg;
    /**
     * 状态码
     *
     * @author weiyb
     */
    private Integer code;
    /**
     * 返回结构
     *
     * @author weiyb
     */
    private Object data;

    /**
     * 禁止new创建对象
     */
    private ApiResult() {
        super();
    }

    /**
     * 统一创建ApiResultBean对象
     * 方便以后扩展
     *
     * @return
     * @author weiyb
     */
    public static ApiResult getInstance() {
        return new ApiResult();
    }

}
