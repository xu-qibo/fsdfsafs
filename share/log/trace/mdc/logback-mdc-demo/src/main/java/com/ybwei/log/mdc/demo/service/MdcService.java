package com.ybwei.log.mdc.demo.service;

public interface MdcService {
    /**
     * 异步方法
     *
     * @methodName: async
     * @return: void
     * @author: geoffrey
     * @date: 2022/5/9
     **/
    void async();

    /**
     * 异步方法(注解)
     *
     * @methodName: async2
     * @return: void
     * @author: geoffrey
     * @date: 2022/5/9
     **/
    void async2();
}
