package com.ybwei.log.mdc.demo.vo.req.yapi;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author ybw
 * @version V1.0
 * @className PreReqVO
 * @date 2022/3/30
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PreReqVO {
    /**
     * id
     *
     * @author: ybw
     * @date: 2022/3/30
     **/
    @NotNull
    private Long id;
    /**
     * no
     *
     * @author: ybw
     * @date: 2022/3/30
     **/
    @NotBlank
    private String no;
}
