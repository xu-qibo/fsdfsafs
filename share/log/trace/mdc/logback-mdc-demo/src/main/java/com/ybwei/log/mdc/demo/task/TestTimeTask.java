package com.ybwei.log.mdc.demo.task;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;

/**
 * 定时任务
 *
 * @author geoffrey
 * @version V1.0
 * @className TestTimeTask
 * @date 2022/5/9
 **/
@Slf4j
@Component
public class TestTimeTask {
    //基于注解@Scheduled默认为单线程，开启多个任务时，任务的执行时机会受上一个任务执行时间的影响。
    //使用的线程池是taskScheduler，线程ID为scheduling-x
    //添加@Async注解指定线程池，则可以多线程执行定时任务（原本是单线程的）。

    /**
     * 两次任务开始的时间间隔为2S
     * 不使用线程池，单线程间隔则为4S。单线程保证不了这个2S间隔，因为任务执行耗时超过了定时间隔，就会影响下一次任务的执行
     * 使用线程池，多线程执行，时间间隔为2S
     */
//    @Async(value = "scheduleThreadPool")
//    @Scheduled(fixedRate = 2000)
    public void fixedRate() throws InterruptedException {
        log.info("定时任务");
        TimeUnit.SECONDS.sleep(2);
    }
}
