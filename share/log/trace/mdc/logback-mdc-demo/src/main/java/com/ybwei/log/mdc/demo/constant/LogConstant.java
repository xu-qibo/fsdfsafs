package com.ybwei.log.mdc.demo.constant;



import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.List;

/**
 * @author weixiansheng
 * @version V1.0
 * @className LogConstant
 * @date 2023/7/10
 **/
public class LogConstant {

    /**
     * 忽略文本
     *
     * @author: weixiansheng
     * @date: 2023/7/10
     **/
    public static final List<String> IGNORE_STR_LIST = Arrays.asList("password");

    /**
     * 打印最大长度
     * 超过这个值，截断后，打印到日志文件
     *
     * @author: weixiansheng
     * @date: 2023/7/10
     **/
    public static final int LOG_STR_MAX_LENGTH = 100000;


    /**
     * 是否包含忽略文本
     *
     * @param requestParam
     * @methodName: isContainIgnoreStr
     * @return: boolean
     * @author: weixiansheng
     * @date: 2023/7/10
     **/
    public static boolean isContainIgnoreStr(String requestParam) {
        if (StringUtils.isBlank(requestParam)) {
            return false;
        }
        return IGNORE_STR_LIST.stream().anyMatch(ignoreStr->requestParam.contains(ignoreStr));
    }
}
