package com.ybwei.log.mdc.demo.service.impl;

import com.ybwei.log.mdc.demo.config.thread.MdcTaskExecutor;
import com.ybwei.log.mdc.demo.service.MdcService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.concurrent.Executor;

/**
 * @program: logback-mdc-demo
 * @description:
 * @author: geoffrey
 * @create: 2022-05-09 16:11
 */
@Service
@Slf4j
public class MdcServiceImpl implements MdcService {

    @Resource(name = "commonThreadPool")
    private Executor executor;

    @Override
    public void async() {
        executor.execute(() -> {
            log.info("异步处理");
        });
    }

    @Override
    @Async("commonThreadPool")
    public void async2() {
        log.info("异步处理2");
    }
}
