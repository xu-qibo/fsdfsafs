package com.ybwei.log.mdc.demo.vo.res.yapi;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * YAPI
 *
 * @author ybwei
 * @date 2022/1/18 10:51
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class YApiResVO {
    /**
     * id
     *
     * @author: ybwei
     * @date: 2022/1/18 10:51
     */
    private Long id;
    /**
     * 姓名
     *
     * @author: ybwei
     * @date: 2022/1/18 10:51
     */
    private String name;
    /**
     * 年龄
     *
     * @author: ybwei
     * @date: 2022/1/18 10:51
     */
    private Integer age;
}
