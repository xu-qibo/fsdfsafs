package com.ybwei.log.mdc.demo.util;

import java.util.UUID;

/**
 * @author ybwei
 * @date 2022/1/26 14:51
 **/
public class MyStringUtils {

    /**
     * 获取uuid
     *
     * @return java.lang.String
     * @throws
     * @methodName: generateUUID
     * @author ybwei
     * @date 2022/1/26 14:52
     */
    @SuppressWarnings("AlibabaLowerCamelCaseVariableNaming")
    public static final String generateUUID() {
        return UUID.randomUUID().toString();
    }

    /**
     * 获取uuid（无中线）
     *
     * @return java.lang.String
     * @throws
     * @methodName: generateUUIDNoCenterLine
     * @author ybwei
     * @date 2022/2/18 17:35
     */
    @SuppressWarnings("AlibabaLowerCamelCaseVariableNaming")
    public static final String generateUUIDNoCenterLine() {
        return generateUUID().replace("-", "");
    }
}
