package com.ybwei.log.mdc.demo.vo.base;

import com.ybwei.log.mdc.demo.constant.ReturnCode;

/**
 * 该类是用来创建成功、失败返回JSON的工具类
 *
 * @author weiyb
 * @ClassName: ApiResultGenerator
 * @Description:
 * @date 2017年11月16日 下午2:34:05
 */
public class ApiResultGenerator {

    /**
     * @Description 创建普通消息方法
     * @Author ybwei
     * @Date 2021/4/8 15:25
     * @Param [msg, code, result, throwable]
     * @Return com.ybwei.apirestfuldemo.vo.base.ApiResult
     * @Exception
     */
    public static ApiResult result(Integer code, String msg, Object data) {
        ApiResult apiResult = ApiResult.getInstance();
        apiResult.setCode(code);
        apiResult.setMsg(msg);
        apiResult.setData(data);
        return apiResult;
    }

    /**
     * 返回执行成功视图方法
     *
     * @param result
     * @return
     * @author weiyb
     */
    public static ApiResult successResult(Object data) {
        return result(ReturnCode.SUCCESS.getCode(), ReturnCode.SUCCESS.getMsg(), data);
    }

    /**
     * 返回执行失败视图方法
     *
     * @param msg
     * @param throwable
     * @return
     * @author weiyb
     */
    public static ApiResult errorResult(ReturnCode returnCode) {
        return result(returnCode.getCode(), returnCode.getMsg(), null);
    }

    /**
     * @Description 
     * @Author      ybwei
     * @Date 2021/4/9 15:45
     * @Param       [code, msg]
     * @Return      com.ybwei.apirestfuldemo.vo.base.ApiResult
     * @Exception   
     */
    public static ApiResult errorResult(int code, String msg) {
        return result(code, msg, null);
    }
}
