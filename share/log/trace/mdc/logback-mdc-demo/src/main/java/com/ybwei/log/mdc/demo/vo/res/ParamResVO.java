package com.ybwei.log.mdc.demo.vo.res;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @program: api-restful-demo
 * @description:
 * @author: geoffrey
 * @create: 2021-04-09 23:38
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ParamResVO {
    private Long id;
    private String result;
}
