package com.ybwei.log.mdc.demo.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author ybwei
 * @Description
 * @date 2021/4/8 18:06
 **/
@AllArgsConstructor
@Getter
public enum ReturnCode {
    /**
     * @Description 成功
     * @author: ybwei
     * @Date: 2021/4/8 18:06
     */
    SUCCESS(200, "成功"),
    /**
     * @Description 错误
     * @author: ybwei
     * @Date: 2021/4/8 18:12
     */
    ERROR(1001, "错误"),

    /**
     * @Description 参数失效
     * @author: ybwei
     * @Date: 2021/4/8 18:08
     */
    PARAM_INVALID(2001, "参数失效"),
    ;
    private int code;
    private String msg;
}
