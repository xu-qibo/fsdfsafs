package com.ybwei.log.mdc.demo.config.exception;

import com.ybwei.log.mdc.demo.constant.ReturnCode;
import com.ybwei.log.mdc.demo.exception.BizException;
import com.ybwei.log.mdc.demo.vo.base.ApiResult;
import com.ybwei.log.mdc.demo.vo.base.ApiResultGenerator;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * result统一异常处理 具体controller不需要单独处理异常了
 *
 * @author weiyb
 * @ClassName: RestExceptionHandler
 * @Description:
 * @date 2017年11月16日 下午2:12:52
 */
@ControllerAdvice(annotations = RestController.class)
public class RestExceptionHandler {

    /**
     * 默认统一异常处理方法
     *
     * @param e 默认Exception异常对象
     * @return
     * @author weiyb
     */
    @ExceptionHandler
    @ResponseBody
    public ApiResult runtimeExceptionHandler(Exception e) {
        return ApiResultGenerator.errorResult(ReturnCode.ERROR.getCode(), e.getMessage());
    }


    @ExceptionHandler
    @ResponseBody
    public ApiResult myExceptionHandler(BizException e) {
        return ApiResultGenerator.errorResult(e.getCode(), e.getMessage());
    }
}
