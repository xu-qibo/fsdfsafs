package com.ybwei.log.mdc.demo.config.thread;

import com.ybwei.log.mdc.demo.config.thread.MdcTaskExecutor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;

import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * @author ybwei
 * @Title: TaskPoolConfig.java
 * @ProjectName com.spring.pro.mybatis.page
 * @Description:
 * @date 2019年1月28日 下午8:23:06
 */
@SuppressWarnings("ALL")
@EnableAsync // 启动异步处理
@Configuration
@Slf4j
public class TaskPoolConfig {

    /**
     * cpu 核心数量
     *
     * @author: ybwei
     * @date: 2022/1/20 11:13
     */
    private final int cpuNum = Runtime.getRuntime().availableProcessors();

    /**
     * 异步任务线程池
     * 用于执行普通的异步请求，带有请求链路的MDC标志
     *
     * @methodName: taskExecutor
     * @return: com.ybwei.log.mdc.demo.config.thread.MdcTaskExecutor
     * @author: geoffrey
     * @date: 2022/5/9
     **/
    @Bean("commonThreadPool")
    public Executor commonThreadPool() {
        log.info("start init common thread pool");
        MdcTaskExecutor executor = new MdcTaskExecutor();
        // 核心线程数,线程池创建时候初始化的线程数
        executor.setCorePoolSize(cpuNum);
        // 最大线程数：线程池最大的线程数，只有在缓冲队列满了之后才会申请超过核心线程数的线程
        executor.setMaxPoolSize(cpuNum * 2);
        // 缓冲队列：用来缓冲执行任务的队列
        executor.setQueueCapacity(200);
        // 允许线程的空闲时间60秒：当超过了核心线程出之外的线程在空闲时间到达之后会被销毁
        executor.setKeepAliveSeconds(60);
        // 线程池名的前缀：设置好了之后可以方便我们定位处理任务所在的线程池
        executor.setThreadNamePrefix("common-thread-pool-");
        // 线程池对拒绝任务的处理策略：这里采用了CallerRunsPolicy策略，当线程池没有处理能力的时候，该策略会直接调用者运行
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        // 用来设置线程池关闭的时候等待所有任务都完成再继续销毁其他的Bean
        executor.setWaitForTasksToCompleteOnShutdown(true);
        // 该方法用来设置线程池中任务的等待时间，如果超过这个时候还没有销毁就强制销毁，以确保应用最后能够被关闭，而不是阻塞住。
        executor.setAwaitTerminationSeconds(60);
        //执行初始化
        executor.initialize();
        return executor;
    }

    /**
     * 定时任务线程池
     * 用于执行自启动的任务执行，父线程不带有MDC标志，不需要传递，直接设置新的MDC
     * 和上面的线程池没啥区别，只是名字不同
     *
     * @methodName: scheduleThreadPool
     * @return: java.util.concurrent.Executor
     * @author: geoffrey
     * @date: 2022/5/9
     **/
    @Bean("scheduleThreadPool")
    public Executor scheduleThreadPool() {
        log.info("start init schedule thread pool");
        MdcTaskExecutor executor = new MdcTaskExecutor();
        // 核心线程数,线程池创建时候初始化的线程数
        executor.setCorePoolSize(cpuNum);
        // 最大线程数：线程池最大的线程数，只有在缓冲队列满了之后才会申请超过核心线程数的线程
        executor.setMaxPoolSize(cpuNum * 2);
        // 缓冲队列：用来缓冲执行任务的队列
        executor.setQueueCapacity(200);
        // 允许线程的空闲时间60秒：当超过了核心线程出之外的线程在空闲时间到达之后会被销毁
        executor.setKeepAliveSeconds(60);
        // 线程池名的前缀：设置好了之后可以方便我们定位处理任务所在的线程池
        executor.setThreadNamePrefix("schedule-thread-pool-");
        // 线程池对拒绝任务的处理策略：这里采用了CallerRunsPolicy策略，当线程池没有处理能力的时候，该策略会直接调用者运行
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        // 用来设置线程池关闭的时候等待所有任务都完成再继续销毁其他的Bean
        executor.setWaitForTasksToCompleteOnShutdown(true);
        // 该方法用来设置线程池中任务的等待时间，如果超过这个时候还没有销毁就强制销毁，以确保应用最后能够被关闭，而不是阻塞住。
        executor.setAwaitTerminationSeconds(60);
        //执行初始化
        executor.initialize();
        return executor;
    }
}