package com.ybwei.log.mdc.demo.controller;

import com.alibaba.fastjson.JSON;
import com.ybwei.log.mdc.demo.service.MdcService;
import com.ybwei.log.mdc.demo.vo.base.ApiResult;
import com.ybwei.log.mdc.demo.vo.base.ApiResultGenerator;
import com.ybwei.log.mdc.demo.vo.req.ParamReqVO;
import com.ybwei.log.mdc.demo.vo.res.ParamResVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

/**
 * @program: api-restful-demo
 * @description: 参数测试
 * @author: geoffrey
 * @create: 2021-04-09 23:35
 */
@RestController
@Slf4j
@RequestMapping("/param")
public class MdcController {
    @Resource
    private MdcService mdcService;
    @Resource
    private RestTemplate restTemplate;
    /**
     * @MethodName: getParam
     * @Description:
     * @Param: [paramReqVO]
     * @Return: com.ybwei.apirestfuldemo.vo.res.ParamResVO
     * @Author: geoffrey
     * @Date: 2021/4/9
     **/
    @PostMapping("/getParam")
    public ApiResult getParam(@Validated @RequestBody ParamReqVO paramReqVO) {
        log.info("ParamController getParam:{}", JSON.toJSONString(paramReqVO));
        mdcService.async();
        mdcService.async2();
        ParamResVO paramResVO = new ParamResVO(paramReqVO.getId(), paramReqVO.getName() + paramReqVO.getNo());
        return ApiResultGenerator.successResult(paramResVO);
    }


    /**
     * @methodName: restTemplate
     * @param paramReqVO
     * @return: com.ybwei.log.mdc.demo.vo.base.ApiResult
     * @author: geoffrey
     * @date: 2022/5/10
     **/
    @PostMapping("/restTemplate")
    public ApiResult restTemplate(@Validated @RequestBody ParamReqVO paramReqVO) {
        String url="http://localhost:8080/param/getParam";
        log.info("ParamController getParam:{}", JSON.toJSONString(paramReqVO));
        return restTemplate.postForObject(url,paramReqVO,ApiResult.class);
    }
}
