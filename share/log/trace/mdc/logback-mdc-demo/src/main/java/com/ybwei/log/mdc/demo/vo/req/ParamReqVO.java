package com.ybwei.log.mdc.demo.vo.req;

import lombok.Data;

/**
 * @program: api-restful-demo
 * @description:
 * @author: geoffrey
 * @create: 2021-04-09 23:37
 */
@Data
public class ParamReqVO {
    private Long id;
    private String name;
    private Integer no;
}
