package com.ybwei.log.mdc.demo.vo.res.yapi;

import com.ybwei.log.mdc.demo.vo.req.yapi.PreReqVO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author ybw
 * @version V1.0
 * @className PreResVO
 * @date 2022/3/30
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PreResVO {
    /**
     * id
     *
     * @author: ybw
     * @date: 2022/3/30
     **/
    private Long id;
    /**
     * no
     *
     * @author: ybw
     * @date: 2022/3/30
     **/
    private String no;

    /**
     * @className PreResVO
     * @author ybw
     * @date 2022/3/30
     * @version V1.0
     **/
    public PreResVO(PreReqVO preReqVO) {
        this(preReqVO.getId() + 1, preReqVO.getNo() + "a");
    }
}
