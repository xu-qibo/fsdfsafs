package com.ybw.task;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * 定时任务
 *
 * @author geoffrey
 * @version V1.0
 * @className TestTimeTask
 * @date 2022/5/9
 **/
@Slf4j
@Component
public class TestTimeTask {

    /**
     * @methodName: fixedRate
     * @return: void
     * @author: weixiansheng
     * @date: 2023/11/20
     **/
    @Scheduled(fixedRate = 2000)
    public void fixedRate() throws InterruptedException {
        log.info("定时任务fixedRate");
        TimeUnit.SECONDS.sleep(2);
    }
}
