package com.ybw;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @className LogbackMdcTaskApplication
 * @author weixiansheng
 * @date 2023/11/20 
 * @version V1.0
 **/
@SpringBootApplication
@EnableScheduling
public class LogbackMdcTaskApplication {

    public static void main(String[] args) {
        SpringApplication.run(LogbackMdcTaskApplication.class, args);
    }

}
