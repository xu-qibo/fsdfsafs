package com.ybw.third.service;

/**
 * @author ybw
 * @version V1.0
 * @className ThirdService
 * @date 2022/10/21
 **/
public interface ThirdService {

    /**
     *
     * @methodName: call
     * @return: java.lang.String
     * @author: ybw
     * @date: 2022/10/21
     **/
    String call();
}
