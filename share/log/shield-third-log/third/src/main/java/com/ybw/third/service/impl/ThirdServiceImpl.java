package com.ybw.third.service.impl;

import com.ybw.third.service.ThirdService;
import lombok.extern.slf4j.Slf4j;

/**
 * @author ybw
 * @version V1.0
 * @className ThirdServiceImpl
 * @date 2022/10/21
 **/
@Slf4j
public class ThirdServiceImpl implements ThirdService {
    @Override
    public String call() {
        log.info("ThirdServiceImpl call");
        return "OK";
    }
}
