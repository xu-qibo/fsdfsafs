package com.ybw.caller;

import com.ybw.third.service.ThirdService;
import com.ybw.third.service.impl.ThirdServiceImpl;
import lombok.extern.slf4j.Slf4j;

/**
 * @author ybw
 * @version V1.0
 * @className CallerApi
 * @date 2022/10/21
 **/
@Slf4j
public class CallerApi {

    private ThirdService thirdService=new ThirdServiceImpl();

    /**
     *
     * @methodName: call
     * @return: void
     * @author: ybw
     * @date: 2022/10/21
     **/
    public void call(){
        log.info("CallerApi call");
        thirdService.call();
    }
}
